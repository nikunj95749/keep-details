import {AppRegistry} from 'react-native';
import {BaseSetting} from './app/config';
import App from './app/index.js';

import * as Sentry from '@sentry/react-native';

// if (!__DEV__) {
Sentry.init({
  dsn: 'https://750f1d3b078c4789b5e94ecf3c11b8a8@o1145853.ingest.sentry.io/6213960',
  // Set tracesSampleRate to 1.0 to capture 100% of transactions for performance monitoring.
  // We recommend adjusting this value in production.
  tracesSampleRate: 1.0,
});
// }

AppRegistry.registerComponent(BaseSetting.name, () => App);
