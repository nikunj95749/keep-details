import {TestIds} from '@react-native-firebase/admob';
import {Platform} from 'react-native';

export const CANCEL = 'Cancel';
export const TAKE_PHOTO = 'Take photo';
export const CHOOSE_FROM_GALLERY = 'Choose from gallery';

const dev = false;
export const interstitialAdUnitId =
  Platform.OS === 'ios'
    ? dev
      ? TestIds.INTERSTITIAL
      : 'ca-app-pub-5778749624122416/1998460846'
    : dev
    ? TestIds.INTERSTITIAL
    : 'ca-app-pub-5778749624122416/1028988599';

export const bannerAdUnitId =
  Platform.OS === 'ios'
    ? dev
      ? TestIds.BANNER
      : 'ca-app-pub-5778749624122416/7068660981'
    : dev
    ? TestIds.BANNER
    : 'ca-app-pub-5778749624122416/2443767725';
