/**
 * Images Defines
 * @author Passion UI <passionui.com>
 */
export const appImages = {
  details: require('@assets/appimages/details.png'),
  notes: require('@assets/appimages/notes.png'),
  settings: require('@assets/appimages/settings.png'),
  back: require('@assets/appimages/back.png'),
  card: require('@assets/appimages/card.png'),
  googlelogo: require('@assets/appimages/googlelogo.png'),
  applogo: require('@assets/appimages/logo.gif'),
  appIcon: require('@assets/appimages/appicon.png'),
  register: require('@assets/appimages/register.png'),
  forgotPassword: require('@assets/appimages/forgotPassword.png'),
  colorPicker: require('@assets/appimages/colorPicker.png'),
  family_icn: require('@assets/appimages/family_icn.png'),
  password: require('@assets/appimages/password.png'),
  emptyNotes: require('@assets/appimages/emptyNotes.png'),
  notesBackground: require('@assets/appimages/notesBackground.jpg'),
  cardBack: require('@assets/appimages/cardBack.png'),
  cardFront: require('@assets/appimages/cardFront.png'),
  lock: require('@assets/appimages/lock.png'),
  unlock: require('@assets/appimages/unlock.png'),
  close: require('@assets/appimages/Close.png'),
};
