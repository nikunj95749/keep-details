export const WHITE = '#FFFFFF';
export const BLACK = '#000000';

//PRIMARY
export const PRIMARY_CTA = '#E00087';
export const PRIMARY_PINK = '#FF0099';

// GRAYSCALE
export const LIGHT_GRAY_10 = '#F2F2F7';
export const LIGHT_GRAY_20 = '#d5d5db';

export const DARK_GRAY_40 = '#3a3a3c';
export const DARK_GRAY_80 = '#636366';

export const THEME_COLOR = '#3E4685';

// UTILITY
export const UTILITY_DESTRUCTIVE = '#FF0000';
export const UTILITY_SUCCESS = '#00820E';
