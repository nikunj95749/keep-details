import React, {useState} from 'react';
import {View, Modal, StyleSheet, Text, SafeAreaView} from 'react-native';
import PropTypes from 'prop-types';

import {THEME_COLOR, WHITE} from '../styles/colors';
import {TriangleColorPicker} from 'react-native-color-picker';
import {scaleFont} from '../styles';
var convert = require('color-convert');

const ColorPickerModal = ({
  visible,
  onChangeColor = () => {},
  defaultColor,
}) => {
  const [selectedColor, setSelectedColor] = useState(defaultColor);

  const _handleAddPress = () => {
    onChangeColor(selectedColor);
  };

  const _handleCancelPress = () => {
    onChangeColor(defaultColor);
  };

  if (!visible) {
    return null;
  }

  return (
    <Modal animationType="slide" transparent={true} visible={visible}>
      <View style={styles.containerStyle}>
        <View style={styles.datePickerStyle}>
          <SafeAreaView />
          <View
            style={{
              height: 40,
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 20,
            }}>
            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
              <Text
                onPress={_handleCancelPress}
                style={{
                  fontSize: scaleFont(16),
                  marginLeft: 15,
                  paddingHorizontal: 25,
                  backgroundColor: THEME_COLOR,
                  paddingVertical: 10,
                  borderRadius: 15,
                  overflow: 'hidden',
                  fontFamily: 'Poppins-Medium',
                  color: WHITE,
                }}>
                Cancel
              </Text>
            </View>

            <View
              style={{
                flex: 1,
                flexDirection: 'row-reverse',
                alignItems: 'center',
              }}>
              <Text
                onPress={_handleAddPress}
                style={{
                  fontSize: scaleFont(16),
                  marginRight: 15,
                  paddingHorizontal: 25,
                  backgroundColor: THEME_COLOR,
                  paddingVertical: 10,
                  borderRadius: 15,
                  overflow: 'hidden',
                  fontFamily: 'Poppins-Medium',
                  color: WHITE,
                }}>
                Add
              </Text>
            </View>
          </View>
          <TriangleColorPicker
            onColorSelected={(color) => {
              console.log(color);
            }}
            defaultColor={selectedColor}
            onColorChange={(color) => {
              setSelectedColor(
                `#${convert.hsv.hex([color.h, color.s * 100, color.v * 100])}`,
              );
            }}
            style={{flex: 1}}
          />
        </View>
      </View>
    </Modal>
  );
};
const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  datePickerStyle: {
    height: '100%',
    borderTopStartRadius: 30,
    borderTopEndRadius: 30,
    overflow: 'hidden',
  },
  datePickerStyleDark: {},
});

ColorPickerModal.propTypes = {
  visible: PropTypes.bool,
  onModalPress: PropTypes.func,
  onChange: PropTypes.func,
  setDatePicker: PropTypes.func,
};

export default ColorPickerModal;
