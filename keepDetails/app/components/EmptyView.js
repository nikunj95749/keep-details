import React from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';
import {scaleFont, THEME_COLOR} from '../styles';

export default function EmptyView(props) {
  return (
    <View
      {...props}
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <View style={{width: '80%', aspectRatio: 1, marginLeft: 10}}>
        <Image
          source={props?.titleImage}
          style={{height: '100%', width: '100%', resizeMode: 'contain'}}
        />
      </View>

      <View style={[{alignItems: 'center', width: '100%'}, props?.titleStyle]}>
        <Text
          style={{
            marginLeft: 10,
            fontFamily: 'Poppins-Regular',
            fontSize: scaleFont(20),
            color: THEME_COLOR,
          }}>
          {props?.title}
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({});
