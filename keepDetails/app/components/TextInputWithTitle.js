import React from 'react';
import {useRef} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  TextInput,
} from 'react-native';
import {THEME_COLOR, WHITE} from '../styles';

export default function TextInputWithTitle(props) {
  const input = useRef();
  return (
    <View
      style={{
        width: '90%',
        alignSelf: 'center',
        height: props?.aspectRatio ?? 70,
        marginTop: 15,
      }}>
      <View
        style={{
          width: '100%',
          justifyContent: 'center',
        }}>
        <Text
          style={{
            marginLeft: 10,
            fontFamily: 'Poppins-Regular',
            fontSize: 15,
          }}>
          {`${props?.title}`}
          <Text style={{color: 'red'}}>{props?.isRequired ? '*' : ''}</Text>
        </Text>
      </View>
      {/* <TouchableOpacity
        onPress={() => {
          input.current.focus();
        }}> */}
      <TouchableOpacity
        style={{
          borderColor: THEME_COLOR,
          borderWidth: 1.5,
          borderRadius: 25,
          paddingLeft: 10,
          backgroundColor: WHITE,
          justifyContent: 'center',
          height: props?.aspectRatio ? props?.aspectRatio - 20 : 50,
        }}
        activeOpacity={1}
        onPress={() => {
          input.current.focus();
        }}
        // pointerEvents="none"
      >
        <View
          style={{flex: 1, backgroundColor: 'rgba(0,0,0,0)'}}
          pointerEvents={props?.multiline ? null : 'none'}>
          <TextInput
            ref={input}
            {...props}
            style={{
              fontSize: 16,
              flex: 1,
            }}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            autoCapitalize="none"
            // value={props?.value}
            // onChangeText={_handleSetTitle}
          />
        </View>
      </TouchableOpacity>
      {/* </TouchableOpacity> */}
    </View>
  );
}

const styles = StyleSheet.create({});
