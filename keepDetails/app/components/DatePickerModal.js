import React from 'react';
import {
  View,
  TouchableWithoutFeedback,
  Modal,
  StyleSheet,
  Platform,
  useColorScheme,
} from 'react-native';
import PropTypes from 'prop-types';
import DateTimePicker from '@react-native-community/datetimepicker';

import {computeDateDifference} from '../helper/dateAndTime';
import {LIGHT_GRAY_10, DARK_GRAY_40} from '../styles/colors';

const DatePickerModal = ({
  visible,
  dateOfBirth,
  setDatePicker = () => {},
  onModalPress = () => {},
  onChange = () => {},
}) => {
  const colorScheme = useColorScheme();

  const _handleDateChange = (event, dob) => {
    setDatePicker();
    Platform.OS === 'android' && onModalPress();
    if (dob) {
      const age = computeDateDifference(dob);
      onChange(dob, age);
    }
  };

  if (!visible) {
    return null;
  }

  if (Platform.OS === 'android') {
    return (
      <DateTimePicker
        value={dateOfBirth || new Date()}
        mode={'date'}
        display="default"
        onChange={_handleDateChange}
      />
    );
  }

  return (
    <Modal animationType="slide" transparent={true} visible={visible}>
      <TouchableWithoutFeedback onPress={onModalPress}>
        <View style={styles.containerStyle}>
          <View
            style={
              colorScheme === 'light'
                ? styles.datePickerStyle
                : styles.datePickerStyleDark
            }>
            <DateTimePicker
              style={styles.iosPickerStyles}
              value={dateOfBirth || new Date()}
              mode={'date'}
              display="inline"
              onChange={_handleDateChange}
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );
};
const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  iosPickerStyles: {
    height: 420,
  },
  datePickerStyle: {
    backgroundColor: LIGHT_GRAY_10,
  },
  datePickerStyleDark: {
    backgroundColor: DARK_GRAY_40,
  },
});

DatePickerModal.propTypes = {
  dateOfBirth: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.instanceOf(Date),
  ]),
  visible: PropTypes.bool,
  onModalPress: PropTypes.func,
  onChange: PropTypes.func,
  setDatePicker: PropTypes.func,
};

export default DatePickerModal;
