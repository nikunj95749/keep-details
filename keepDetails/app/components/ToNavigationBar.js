import {Image, Text, TouchableOpacity, View} from 'react-native';
import Search from '../assets/appimages/Search.svg';
import React from 'react';
import {BLACK, LIGHT_GRAY_10, responsiveScale} from '../styles';
import {appImages} from '../config';
export const ToNavigationBar = ({
  onPressBack = () => {},
  title = '',
  onPressSearch = () => {},
}) => {
  return (
    <View
      style={{
        width: '100%',
        flexDirection: 'row',
        paddingLeft: 15,
        paddingBottom: 10,
        height: 40,
        borderBottomColor: LIGHT_GRAY_10,
        borderBottomWidth: 1.5,
      }}>
      <TouchableOpacity
        style={{
          height: '100%',
          aspectRatio: 1,
          backgroundColor: 'white',
          justifyContent: 'center',
          alignItems: 'center',
        }}
        onPress={onPressBack}>
        <Image
          resizeMode={'contain'}
          style={{height: 22, width: 25}}
          source={appImages.back}
        />
      </TouchableOpacity>
      <Text
        adjustsFontSizeToFit
        numberOfLines={1}
        style={{
          flex: 1,
          color: BLACK,
          fontFamily: 'Poppins-Medium',
          fontSize: responsiveScale(10),
          textAlign: 'center',
        //   marginRight: 50,
        }}>
        {title}
      </Text>
      <TouchableOpacity
        onPress={onPressSearch}
        style={{
          height: 35,
          width: 35,
          marginRight: 15,
          justifyContent: 'center',
        }}>
        <Search height={30} width={30} />
      </TouchableOpacity>
    </View>
  );
};
