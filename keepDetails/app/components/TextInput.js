import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {TextInput as Input} from 'react-native-paper';
import {DefaultTheme} from 'react-native-paper';
import { THEME_COLOR } from '../styles';

export default function TextInput({errorText, description, ...props}) {
  return (
    <View style={styles.container}>
      <Input
        style={styles.input}
        selectionColor={THEME_COLOR}
        underlineColor={THEME_COLOR}
        outlineColor={THEME_COLOR}
        activeOutlineColor={THEME_COLOR}
        mode="outlined"
        {...props}
      />
      {description && !errorText ? (
        <Text style={styles.description}>{description}</Text>
      ) : null}
      {errorText ? <Text style={styles.error}>{errorText}</Text> : null}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginVertical: 9,
  },
  input: {
    backgroundColor: DefaultTheme.surface,
    height:40
  },
  description: {
    fontSize: 13,
    color: '#414757',
    paddingTop: 8,
  },
  error: {
    fontSize: 13,
    color: '#f13a59',
    paddingTop: 8,
  },
});
