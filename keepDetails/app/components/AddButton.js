import React from 'react';
import {StyleSheet, TouchableOpacity, Text} from 'react-native';

import {DARK_GRAY_80, PRIMARY_PINK, WHITE} from '../styles';

export default function AddButton(props) {
  return (
    <TouchableOpacity
      {...props}
      activeOpacity={0.7}
      style={{
        width: 60,
        height: 60,
        borderRadius: 35,
        backgroundColor: WHITE,
        position: 'absolute',
        alignSelf: 'flex-end',
        right: 20,
        bottom: 20,
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: '#000',
        shadowOffset: {
          width: 1,
          height: 1,
        },
        shadowOpacity: 0.25,
        shadowRadius: 2,
        elevation: 5,
      }}>
      <Text style={{fontSize: 30}}>+</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  bottomButton: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 66,
    backgroundColor: PRIMARY_PINK,
  },
  bottomButtonText: {
    color: WHITE,
  },
  buttonDisabled: {
    backgroundColor: DARK_GRAY_80,
  },
});
