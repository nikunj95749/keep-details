import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
  Image,
  Text,
} from 'react-native';
import {padding, scaleSize, WHITE, THEME_COLOR} from '../styles';
import {appImages} from '../config';
// import Lock from '../assets/appimages/Lock.svg';
// import Unlock from '../assets/appimages/Unlock.svg';

export default function TopBar({
  onBack,
  headingText = '',
  onColorPicker,
  onPressTouchableButton,
  onLockShow = false,
  onUnLock = false,
  onPinShow = false,
  onUnPin = false,
  onPressLock,
  containerStyle = {},
  headerTextStyles = {},
  isTouchableButton = false,
  touchableButtonTitle = '',
}) {
  return (
    <SafeAreaView>
      <View style={{...styles.topBar, ...containerStyle}}>
        <View style={{flexDirection: 'row'}}>
          {onBack && (
            <TouchableOpacity style={styles.back} onPress={onBack}>
              <Image
                resizeMode={'contain'}
                style={{height: 25, width: 30, resizeMode: 'contain'}}
                source={appImages.back}
              />
            </TouchableOpacity>
          )}
        </View>
        {headingText !== '' && (
          <Text style={{...styles.headerText, ...headerTextStyles}}>
            {headingText}
          </Text>
        )}
        <View
          style={[!onColorPicker && styles.emptyView, {flexDirection: 'row'}]}>
          {onPinShow && (
            <TouchableOpacity style={styles.close} onPress={onPressLock}>
              {/* {onUnLock ? <Lock  height={25} width={30}/> : <Unlock />} */}
              <Image
                source={onUnPin ? appImages?.lock : appImages?.unlock}
                style={{height: 25, width: 30, resizeMode: 'contain'}}
              />
            </TouchableOpacity>
          )}
          {onLockShow && (
            <TouchableOpacity style={styles.close} onPress={onPressLock}>
              {/* {onUnLock ? <Lock  height={25} width={30}/> : <Unlock />} */}
              <Image
                source={onUnLock ? appImages?.lock : appImages?.unlock}
                style={{height: 25, width: 30, resizeMode: 'contain'}}
              />
            </TouchableOpacity>
          )}
          {onColorPicker && (
            <TouchableOpacity style={styles.close} onPress={onColorPicker}>
              <Image
                source={appImages.colorPicker}
                style={{height: 25, width: 30, resizeMode: 'contain'}}
              />
            </TouchableOpacity>
          )}
          {isTouchableButton && (
            <TouchableOpacity
              style={styles.saveAndUpdateButton}
              onPress={onPressTouchableButton}>
              <Text style={{color: WHITE, fontSize: 12}}>
                {touchableButtonTitle}
              </Text>
            </TouchableOpacity>
          )}
        </View>
      </View>
    </SafeAreaView>
  );
}

TopBar.propTypes = {
  onBack: PropTypes.func,
  onSearch: PropTypes.func,
  headingText: PropTypes.string,
  progressWidth: PropTypes.number,
  onColorPicker: PropTypes.func,
  openMenu: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
  containerStyle: PropTypes.object,
  chevronColor: PropTypes.string,
  headerTextStyles: PropTypes.object,
  optionsIconColor: PropTypes.string,
};

const styles = StyleSheet.create({
  topBar: {
    height: 49,
    position: 'relative',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 15,
    paddingHorizontal: scaleSize(12),
    paddingVertical: scaleSize(12),
    backgroundColor: WHITE,
  },
  headerText: {
    position: 'absolute',
    alignSelf: 'center',
    width: Dimensions.get('window').width * 0.5,
    textAlign: 'center',
    marginLeft:
      Dimensions.get('window').width * 0.5 -
      Dimensions.get('window').width * 0.25,
    bottom: 14,
  },
  back: {
    ...padding(12, 16, 12, scaleSize(4)),
  },
  close: {
    ...padding(8, 4, 8, 7),
  },
  search: {
    ...padding(12, 16, 12, scaleSize(4)),
  },
  menu: {
    ...padding(8, 0, 8, scaleSize(4)),
  },
  leftHeaderItemView: {
    flexDirection: 'row',
  },
  emptyView: {
    padding: 10,
  },
  saveAndUpdateButton: {
    backgroundColor: THEME_COLOR,
    alignSelf: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    alignItems: 'center',
    paddingHorizontal: 12,
    height: 30,
    marginLeft: 10,
    marginRight: 10,
  },
});
