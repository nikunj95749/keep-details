import React from 'react';
import {StyleSheet, TouchableOpacity, Text} from 'react-native';

import {DARK_GRAY_80, PRIMARY_PINK, WHITE} from '../styles';

export default function BottomButton(props) {
  return (
    <TouchableOpacity
      {...props}
      style={[
        styles.bottomButton,
        props.style,
        props.disabled && styles.buttonDisabled,
        props.overrideStyles,
      ]}
      disabled={props.disabled}
      activeOpacity={0.6}>
      <Text style={styles.bottomButtonText}>{props.children}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  bottomButton: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 66,
    backgroundColor: PRIMARY_PINK,
  },
  bottomButtonText: {
    color: WHITE,
  },
  buttonDisabled: {
    backgroundColor: DARK_GRAY_80,
  },
});
