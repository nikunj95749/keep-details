import React from 'react';
import {TouchableOpacity, Image, StyleSheet} from 'react-native';
import {appImages} from '../config';

export default function BackButton({goBack}) {
  return (
    <TouchableOpacity onPress={goBack} style={styles.container}>
      <Image style={styles.image} source={appImages.back} />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 10,
    left: 10,
  },
  image: {
    width: 24,
    height: 24,
  },
});
