import PropTypes from 'prop-types';
import React, {useCallback} from 'react';
import {
  StatusBar,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
} from 'react-native';
import Modal from 'react-native-modal';

const CustomActionsheet = ({
  isVisible = false,
  actionItems = [],
  OnBackdropPress,
  onCancel = () => {},
}) => {
  const actionSheetItems = actionItems;
  const handleCancel = useCallback(() => {
    onCancel();
  }, []);
  return (
    <Modal
      isVisible={isVisible}
      onBackdropPress={OnBackdropPress}
      onBackButtonPress={handleCancel}
      style={styles.modalMainStyle}>
      {isVisible && <StatusBar backgroundColor={'rgba(0,0,0,0.70)'} />}
      <View style={styles.modalContent}>
        {actionSheetItems.map((actionItem, index) => {
          return (
            <TouchableHighlight
              style={[
                styles.actionSheetView,
                index === actionSheetItems.length - 2 && {
                  borderBottomLeftRadius: 10,
                  borderBottomRightRadius: 10,
                  borderBottomWidth: 0,
                },
                index === actionSheetItems.length - 1 && {
                  borderBottomWidth: 0,
                  marginTop: 8,
                  borderRadius: 10,
                },
              ]}
              underlayColor={'rgba(255,255,255,0.5)'}
              key={index}
              onPress={() => {
                handleCancel();
                if (actionItem.onPress) {
                  actionItem.onPress();
                }
              }}>
              <Text allowFontScaling={false} style={[styles.actionSheetText]}>
                {actionItem.text}
              </Text>
            </TouchableHighlight>
          );
        })}
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContent: {
    marginLeft: 8,
    marginRight: 8,
    marginBottom: 20,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    overflow: 'hidden',
  },
  actionSheetText: {
    fontSize: 16,
    textAlign: 'center',
    color: 'rgba(0,44,239,0.8)',
  },
  actionSheetView: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 16,
    borderBottomWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.07)',
    backgroundColor: '#FFFFFF',
  },
  modalMainStyle: {
    margin: 0,
    justifyContent: 'flex-end',
  },
});

CustomActionsheet.propTypes = {
  actionItems: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      label: PropTypes.string,
      onPress: PropTypes.func,
    }),
  ).isRequired,
  onCancel: PropTypes.func,
  actionTextColor: PropTypes.string,
};

CustomActionsheet.defaultProps = {
  actionItems: [],
  onCancel: () => {},
  actionTextColor: null,
};

export default CustomActionsheet;
