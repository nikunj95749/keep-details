import React from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Image,
  Text,
} from 'react-native';
import {appImages} from '../../../config/images';
import {BLACK, responsiveScale, WHITE} from '../../../styles';
import {Calculator} from 'react-native-calculator';

function NormalCalculator({navigation}) {
  return (
    <View style={styles.mainView}>
      <SafeAreaView />
      <View
        style={{
          width: '100%',
          flexDirection: 'row',
          marginLeft: 15,
          height: 30,
        }}>
        <TouchableOpacity
          style={{
            height: '100%',
            aspectRatio: 1,
            backgroundColor: 'white',
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => {
            navigation.goBack();
          }}>
          <Image
            resizeMode={'contain'}
            style={{height: 22, width: 25}}
            source={appImages.back}
          />
        </TouchableOpacity>
        <Text adjustsFontSizeToFit numberOfLines={1} style={styles.headerTitle}>
          Normal Calculator
        </Text>
      </View>
      <View style={styles.flatListMainView}>
        <Calculator style={{flex: 1}} />
      </View>
      <SafeAreaView />
    </View>
  );
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: WHITE,
  },
  flatListMainView: {
    marginTop: 10,
    flex: 1,
  },
  creditOrDebitListItemMainView: {
    width: '92%',
    alignSelf: 'center',
    flexDirection: 'row',
    backgroundColor: WHITE,
    shadowColor: '#000',
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.25,
    shadowRadius: 2,
    elevation: 5,
    borderRadius: 10,
    marginTop: 15,
    marginBottom: 10,
  },
  titleView: {
    backgroundColor: WHITE,
    justifyContent: 'flex-end',
    paddingLeft: 20,
  },
  headerTitle: {
    flex: 1,
    color: BLACK,
    fontFamily: 'Poppins-Medium',
    fontSize: responsiveScale(10),
    textAlign: 'center',
    marginRight: 50,
  },
});

export default NormalCalculator;
