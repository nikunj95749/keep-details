import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  SafeAreaView,
  Text,
  TouchableOpacity,
} from 'react-native';
import {BLACK, THEME_COLOR, WHITE, WINDOW_WIDTH} from '../../styles';
import {
  InterstitialAd,
  AdEventType,
  BannerAd,
  BannerAdSize,
} from '@react-native-firebase/admob';
import {getValueFromLocal, setValueInLocal} from '../../helper/auth';

import CalculatorFocus from '../../assets/appimages/CalculatorFocus.svg';
import Identification_icn from '../../assets/appimages/Identification_icn.svg';
import EMICalculator from '../../assets/appimages/EMICalculator.svg';
import DabitCard from '../../assets/appimages/DabitCard.svg';
import {bannerAdUnitId, interstitialAdUnitId} from '../../config/constants';

const interstitial = InterstitialAd.createForAdRequest(interstitialAdUnitId, {
  requestNonPersonalizedAdsOnly: true,
});
function CalculatorListsScreen({navigation}) {
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    const eventListener = interstitial.onAdEvent((type) => {
      if (type === AdEventType.LOADED) {
        setLoaded(true);
        setTimeout(() => {
          interstitial.show();
        }, 2000);
      }
    });

    // Unsubscribe from events on unmount
    return () => {
      eventListener();
    };
  }, []);

  const initializeAdd = async () => {
    const notes = await getValueFromLocal('MyDetails');
    console.log('MyDetails=== ', JSON.parse(notes));
    if (notes === null) {
      setValueInLocal('MyDetails', JSON.stringify(0));
    } else if (JSON.parse(notes) >= 3) {
      interstitial.load();
      setValueInLocal('MyDetails', JSON.stringify(0));
    } else {
      setValueInLocal('MyDetails', JSON.stringify(JSON.parse(notes) + 1));
    }
  };

  const [arrsDetails, setArrsDetails] = useState([
    {
      name: 'Normal Calculator',
      image: <CalculatorFocus height={60} width={60} />,
      key: 'NormalCalculator',
    },
    // { name: 'Addresses', image: appImages.details ,key:'Addresses'},
    {
      name: 'EMI Calculator',
      image: <EMICalculator height={60} width={60} />,
      key: 'EMICalculator',
    },
    // {
    //   name: 'ADMOB',
    //   image: appImages.credi_icn,
    //   key: 'CreditOrDebitCard',
    // },
    // {
    //   name: 'Identifications',
    //   image: <Identification_icn />,
    //   key: 'Identifications',
    // },
    // {
    //   name: 'Passwords',
    //   image: <Passwords />,
    //   // image: appImages.password,
    //   key: 'PassWords',
    // },
  ]);

  const DetailListItem = ({item, index, navigation}) => {
    return (
      <TouchableOpacity
        activeOpacity={0.85}
        style={[
          styles.detaillistItemMainView,
          {
            flexDirection: index % 2 === 0 ? 'row' : 'row-reverse',
            width: WINDOW_WIDTH - 20,
            alignSelf: 'center',
          },
        ]}
        onPress={() => handleOnPressItem(item.key)}>
        <View style={styles.detaillistItemImageView}>{item.image}</View>
        <Text
          style={[
            styles.detailsListItemTypeTxt,
            {textAlign: index % 2 === 0 ? 'left' : 'right'},
          ]}>
          {item.name}
        </Text>
      </TouchableOpacity>
    );
  };

  const handleOnPressItem = (key) => {
    initializeAdd();
    navigation.navigate(key);
  };

  return (
    <View style={styles.mainView}>
      <SafeAreaView />

      <View style={styles.flatListMainView}>
        <FlatList
          renderse
          showsVerticalScrollIndicator={false}
          data={arrsDetails}
          renderItem={({item, index}) => (
            <DetailListItem item={item} index={index} navigation={navigation} />
          )}
          ListHeaderComponent={() => (
            <View style={styles.titleView}>
              <Text style={styles.headerTitle}>Calculators</Text>
            </View>
          )}
          keyExtractor={(item) => item.name}
        />
      </View>
      <BannerAd
        size={BannerAdSize.SMART_BANNER}
        requestOptions={{
          requestNonPersonalizedAdsOnly: true,
        }}
        unitId={bannerAdUnitId}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: WHITE,
    overflow: 'visible',
  },
  flatListMainView: {
    flex: 1,
  },
  detaillistItemMainView: {
    width: '100%',

    overflow: 'visible',
    aspectRatio: 3.5,
    backgroundColor: WHITE,
    marginVertical: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.25,
    shadowRadius: 2,
    elevation: 5,
    borderRadius: 10,
  },
  adMobView: {
    width: '100%',
    overflow: 'visible',
    backgroundColor: WHITE,
    marginVertical: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.25,
    shadowRadius: 2,
    elevation: 5,
    borderRadius: 10,
  },
  titleView: {
    width: '100%',
    backgroundColor: WHITE,
    marginVertical: 0,
    paddingLeft: 20,
  },
  detaillistItemImageMainView: {
    flex: 1,
  },
  detaillistItemImageView: {
    height: '100%',
    justifyContent: 'center',
    alignSelf: 'center',
    marginHorizontal: 20,
  },
  detaillistItemImage: {
    width: '100%',
    height: '100%',
  },
  detailsListItemTypeView: {
    width: '100%',
    aspectRatio: 6,
    backgroundColor: 'red',
    alignSelf: 'center',
  },
  detailsListItemTypeTxt: {
    fontSize: 18,
    alignSelf: 'center',
    flex: 1,
    color: THEME_COLOR,
  },
  headerTitle: {
    textAlign: 'left',
    color: BLACK,
    fontFamily: 'Poppins-Medium',
    fontSize: 25,
  },
});

export default CalculatorListsScreen;
