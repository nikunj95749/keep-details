import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Image,
  Text,
  TextInput,
  Switch,
} from 'react-native';
import {appImages} from '../../../config/images';
import {
  BLACK,
  responsiveScale,
  THEME_COLOR,
  WHITE,
  WINDOW_WIDTH,
} from '../../../styles';
import {Calculator} from 'react-native-calculator';
import TextInputWithTitle from '../../../components/TextInputWithTitle';

function NormalCalculator({navigation}) {
  const [emi, setEmi] = useState(0);

  const [principal, setPrincipal] = useState(0);

  const [interest, setInterest] = useState(0);

  const [tenure, setTenure] = useState(0);

  const calculateEMI = () => {
    let emii = 0;
    let r = interest;
    let p = principal;
    let n = tenure;
    if (r === 0 || p === 0 || n === 0) {
      setEmi(0);
    } else {
      r = interest / 12 / 100;
      emii = p * r * (Math.pow(1 + r, n) / (Math.pow(1 + r, n) - 1));
      setEmi(emii.toFixed(3));
    }
  };

  return (
    <View style={styles.mainView}>
      <SafeAreaView />
      <View
        style={{
          width: '100%',
          flexDirection: 'row',
          marginLeft: 15,
          height: 30,
        }}>
        <TouchableOpacity
          style={{
            height: '100%',
            aspectRatio: 1,
            backgroundColor: 'white',
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => {
            navigation.goBack();
          }}>
          <Image
            resizeMode={'contain'}
            style={{height: 22, width: 25}}
            source={appImages.back}
          />
        </TouchableOpacity>
        <Text adjustsFontSizeToFit numberOfLines={1} style={styles.headerTitle}>
          EMI Calculator
        </Text>
      </View>
      <TextInputWithTitle
        title={'Principal Amount:'}
        isRequired={true}
        value={principal}
        keyboardType={'number-pad'}
        placeholder="In Rupees"
        onChangeText={setPrincipal}
      />

      <TextInputWithTitle
        title={'Interest Rate:'}
        isRequired={true}
        value={interest}
        keyboardType={'number-pad'}
        placeholder="Percentage"
        onChangeText={setInterest}
      />

      <TextInputWithTitle
        title={'Tenure of Loan:'}
        isRequired={true}
        value={tenure}
        keyboardType={'number-pad'}
        placeholder="In Months"
        onChangeText={setTenure}
      />
      <View style={styles.flatListMainView}>
        <Text style={styles.emitext}>
          EMI to be paid: {'\u20B9'} {emi}
        </Text>
        <TouchableOpacity
          activeOpacity={0.95}
          style={styles.button}
          onPress={calculateEMI}>
          <Text style={styles.buttonText}>Calculate EMI</Text>
        </TouchableOpacity>
      </View>
      <SafeAreaView />
    </View>
  );
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: WHITE,
  },
  flatListMainView: {
    flex: 1,
    paddingHorizontal: 20,
  },
  creditOrDebitListItemMainView: {
    width: '92%',
    alignSelf: 'center',
    flexDirection: 'row',
    backgroundColor: WHITE,
    shadowColor: '#000',
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.25,
    shadowRadius: 2,
    elevation: 5,
    borderRadius: 10,
    marginTop: 15,
    marginBottom: 10,
  },
  titleView: {
    backgroundColor: WHITE,
    justifyContent: 'flex-end',
    paddingLeft: 20,
  },
  headerTitle: {
    flex: 1,
    color: BLACK,
    fontFamily: 'Poppins-Medium',
    fontSize: responsiveScale(10),
    textAlign: 'center',
    marginRight: 50,
  },
  input: {
    borderBottomWidth: 1,
    borderBottomColor: '#777',
    paddingLeft: 80,
    paddingRight: 80,
    paddingTop: 10,
    paddingBottom: 10,
    marginBottom: 15,
  },
  logo: {
    marginTop: 100,
    marginBottom: 50,
    width: 225,
    height: 122,
  },
  text: {
    marginTop: 15,
  },
  button: {
    marginTop: 90,
    color: '#fff',
    padding: 20,
    width: WINDOW_WIDTH,
    position: 'absolute',
    bottom: 0,
    backgroundColor: THEME_COLOR,
  },
  buttonText: {
    color: '#fff',
    alignSelf: 'center',
    fontSize: 20,
  },
  emitext: {
    color: THEME_COLOR,
    fontWeight: 'bold',
    fontSize: 20,
    marginTop: 20,
  },
});

export default NormalCalculator;
