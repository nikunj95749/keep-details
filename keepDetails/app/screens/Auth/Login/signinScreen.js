import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Image,
  SafeAreaView,
  TouchableOpacity,
  Text,
  StatusBar,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import {appImages} from '../../../config/images';

import Close from '../../../assets/appimages/Close.svg';

import {GoogleSignin} from '@react-native-community/google-signin';
import {WINDOW_HEIGHT} from '../../../styles/mixins';
import {THEME_COLOR, LIGHT_GRAY_20} from '../../../styles/colors';

import auth from '@react-native-firebase/auth';

import {
  appleAuth,
  AppleButton,
} from '@invertase/react-native-apple-authentication';
import {BLACK, PRIMARY_CTA} from '../../../styles/colors';
import {setAuthToken, setUserdetails} from '../../../helper/auth';
import {useDispatch} from 'react-redux';
import {setAuthTokenAction} from '../../../store/auth';
import {setUserDetailsAction} from '../../../store/userDetails';
import Button from '../../../components/Button';
import TextInput from '../../../components/TextInput';
import {colors} from 'react-native-elements';
import Toast from '../../../components/Toast';
import {firebase} from '@react-native-firebase/firestore';
import BackButton from '../../../components/BackButton';

function SigninScreen({navigation}) {
  const dispatch = useDispatch();
  const [email, setEmail] = useState({value: '', error: ''});
  const [password, setPassword] = useState({value: '', error: ''});
  const [name, setName] = useState({value: '', error: ''});
  const [loading, setLoading] = useState(false);
  const [isLogin, setIsLogin] = useState(true);
  const [loadingGoogle, setLoadingGoogle] = useState(false);
  const [error, setError] = useState();

  function emailValidator(emailId) {
    const re = /\S+@\S+\.\S+/;
    if (!emailId) {
      return "Email can't be empty.";
    }
    if (!re.test(emailId)) {
      return 'Ooops! We need a valid email address.';
    }
    return '';
  }

  function passwordValidator(passwordOfPerson) {
    if (!passwordOfPerson) {
      return "Password can't be empty.";
    }
    if (passwordOfPerson.length < 5) {
      return 'Password must be at least 5 characters long.';
    }
    return '';
  }

  function nameValidator(nameOfPerson) {
    if (!nameOfPerson) {
      return "Name can't be empty.";
    }
    return '';
  }

  async function onGoogleButtonPress() {
    // Get the users ID token
    try {
      setLoadingGoogle(true);

      const {user, idToken} = await GoogleSignin.signIn();

      const docs = await firebase
        .firestore()
        .collection('Users')
        .doc(user?.id)
        .get();
      if (docs.exists) {
        setUserdetails(JSON.stringify(docs.data()));
        dispatch(setUserDetailsAction(docs.data()));
      } else {
        let userDetails = {
          userId: user?.id,
          userFullName: user?.name,
          fontsize: 16,
          isDarkMode: true,
          themeColor: PRIMARY_CTA,
          titleDetailsFontColor: BLACK,
          titleFontColor: BLACK,
          isSetLock: false,
        };
        await firebase
          .firestore()
          .collection('Users')
          .doc(user?.id)
          .set(userDetails);
        dispatch(setUserDetailsAction(userDetails));
        setUserdetails(JSON.stringify(userDetails));
      }

      await setAuthToken(user?.id);
      dispatch(setAuthTokenAction(user?.id));

      // Create a Google credential with the token
      const googleCredential = auth.GoogleAuthProvider.credential(idToken);

      // Sign-in the user with the credential
      return auth().signInWithCredential(googleCredential);
    } catch (err) {
      console.log('err====> ', err);
    } finally {
      setLoadingGoogle(false);
    }
  }

  async function onAppleButtonPress() {
    // 1). start a apple sign-in request
    const appleAuthRequestResponse = await appleAuth.performRequest({
      requestedOperation: appleAuth.Operation.LOGIN,
      requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
    });

    // 2). if the request was successful, extract the token and nonce
    const {identityToken, nonce} = appleAuthRequestResponse;
    console.log('identityToken==== > ', identityToken, nonce);
    // can be null in some scenarios
    if (identityToken) {
      // 3). create a Firebase `AppleAuthProvider` credential
      const appleCredential = firebase.auth.AppleAuthProvider.credential(
        identityToken,
        nonce,
      );

      // 4). use the created `AppleAuthProvider` credential to start a Firebase auth request,
      //     in this example `signInWithCredential` is used, but you could also call `linkWithCredential`
      //     to link the account to an existing user
      const userCredential = await firebase
        .auth()
        .signInWithCredential(appleCredential);

      // user is now signed in, any Firebase `onAuthStateChanged` listeners you have will trigger
      console.warn(
        `Firebase authenticated via Apple, UID: ${userCredential.user.uid}`,
      );
    } else {
      // handle this - retry?
    }
  }

  const setUserDetails = () => {};

  const loginUser = async (emailId = '', pass = '') => {
    try {
      const user = await firebase
        .auth()
        .signInWithEmailAndPassword(emailId, pass);
      return {user};
    } catch (error) {
      return {
        error: error.message,
      };
    }
  };

  const onLoginPressed = async () => {
    try {
      const emailError = emailValidator(email.value);
      const passwordError = passwordValidator(password.value);
      if (emailError || passwordError) {
        setEmail({...email, error: emailError});
        setPassword({...password, error: passwordError});
        return;
      }
      setLoading(true);
      const response = await loginUser(email.value, password.value);
      console.log('response = ', response);
      if (response.error) {
        setError(response.error);
      } else {
        const uId = response?.user?.user?.uid;
        const docs = await firebase
          .firestore()
          .collection('Users')
          .doc(uId)
          .get();

        console.log('resopnse login === ', uId, docs);

        setUserdetails(JSON.stringify(docs.data()));
        dispatch(setUserDetailsAction(docs.data()));
        await setAuthToken(uId);
        dispatch(setAuthTokenAction(uId));
        navigation.goBack();
      }
    } catch (err) {
      setError(err);
      console.log('errorrrrrrr = ', err);
    } finally {
      setLoading(false);
    }
  };

  // eslint-disable-next-line react-hooks/rules-of-hooks
  // useEffect(() => {
  //   GoogleSignin.configure({
  //     scopes: ['email'],
  //     webClientId:
  //       '526858913563-hblvigjinn6hm0jomvjs9vo76svn7q1s.apps.googleusercontent.com',
  //     offlineAccess: true,
  //   });
  // }, []);

  const signUpUser = async (nameOfPerson, emailId, passwordOfPerson) => {
    try {
      const user = await auth().createUserWithEmailAndPassword(
        emailId,
        passwordOfPerson,
      );
      auth().currentUser.updateProfile({
        displayName: nameOfPerson,
      });
      return {user};
    } catch (err) {
      console.log('error=== ', err);
      return {
        error: err.message,
      };
    }
  };

  const onSignUpPressed = async () => {
    const nameError = nameValidator(name.value);
    const emailError = emailValidator(email.value);
    const passwordError = passwordValidator(password.value);
    if (emailError || passwordError || nameError) {
      setName({...name, error: nameError});
      setEmail({...email, error: emailError});
      setPassword({...password, error: passwordError});
      return;
    }
    setLoading(true);
    const response = await signUpUser(name.value, email.value, password.value);

    if (response.error) {
      setError(response.error);
    } else {
      console.log('response======= ', response, response?.user?.user?.uid);
      const uId = response?.user?.user?.uid;
      let userDetails = {
        userId: uId,
        userFullName: name.value,
        fontsize: 16,
        isDarkMode: true,
        themeColor: PRIMARY_CTA,
        titleDetailsFontColor: BLACK,
        titleFontColor: BLACK,
        isSetLock: false,
      };
      await firebase.firestore().collection('Users').doc(uId).set(userDetails);
      dispatch(setUserDetailsAction(userDetails));
      setUserdetails(JSON.stringify(userDetails));
      await setAuthToken(uId);
      dispatch(setAuthTokenAction(uId));
      navigation.pop(2);
    }
    setLoading(false);
  };

  return (
    <View style={styles.mainView}>
      <StatusBar backgroundColor={colors.white} barStyle="dark-content" />
      <View
        style={{
          height: '100%',
          width: '100%',
          backgroundColor: colors.white,
          flexDirection: 'column-reverse',
          position: 'absolute',
        }}>
        <KeyboardAwareScrollView
          style={styles.mainView}
          keyboardShouldPersistTaps="handled"
          extraScrollHeight={10}>
          <SafeAreaView style={{width: '100%', flex: 1}}>
            {/* <BackButton goBack={navigation.goBack} /> */}
            <TouchableOpacity
              onPress={navigation.goBack}
              style={{
                height: 25,
                width: 25,
                alignSelf: 'flex-end',
                marginRight: 20,
                marginTop: 10,
              }}>
              <Image source={appImages.close} style={{height:'100%',width:'100%'}}/>
            </TouchableOpacity>
            <View
              style={{
                width: '100%',
                marginTop: 50,
                marginBottom: 35,
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: '50%',
                  aspectRatio: 1,
                  borderRadius: 20,
                  overflow: 'hidden',
                }}>
                <Image
                  source={appImages.appIcon}
                  style={{width: '100%', height: '100%'}}
                />
              </View>
            </View>

            <View
              style={{
                width: '100%',
                flex: 1,
                paddingHorizontal: 30,
                height: 27,
                marginBottom: 25,
                alignItems: 'center',
              }}>
              <View
                style={{
                  height: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  onPress={() => {
                    setIsLogin(true);
                  }}
                  style={{
                    height: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderBottomWidth: 2,
                    borderBottomColor: isLogin ? THEME_COLOR : LIGHT_GRAY_20,
                  }}>
                  <Text style={{color: isLogin ? THEME_COLOR : LIGHT_GRAY_20}}>
                    LOGIN
                  </Text>
                </TouchableOpacity>
                <View
                  style={{
                    width: 2,
                    height: '50%',
                    backgroundColor: THEME_COLOR,
                    marginHorizontal: 20,
                  }}></View>
                <TouchableOpacity
                  onPress={() => {
                    setIsLogin(false);
                  }}
                  style={{
                    height: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderBottomWidth: 2,
                    borderBottomColor: !isLogin ? THEME_COLOR : LIGHT_GRAY_20,
                  }}>
                  <Text style={{color: !isLogin ? THEME_COLOR : LIGHT_GRAY_20}}>
                    SIGN UP
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={{width: '100%', flex: 1, paddingHorizontal: 30}}>
              {!isLogin ? (
                <TextInput
                  label="Name"
                  returnKeyType="next"
                  value={name.value}
                  onChangeText={(text) => setName({value: text, error: ''})}
                  error={!!name.error}
                  errorText={name.error}
                />
              ) : null}
              <TextInput
                label="Email"
                returnKeyType="next"
                value={email.value}
                onChangeText={(text) => setEmail({value: text, error: ''})}
                error={!!email.error}
                errorText={email.error}
                autoCapitalize="none"
                autoCompleteType="email"
                textContentType="emailAddress"
                keyboardType="email-address"
              />
              <TextInput
                label="Password"
                returnKeyType="done"
                value={password.value}
                onChangeText={(text) => setPassword({value: text, error: ''})}
                error={!!password.error}
                errorText={password.error}
                secureTextEntry
              />
              {isLogin ? (
                <View style={styles.forgotPassword}>
                  <TouchableOpacity
                    onPress={() => navigation.navigate('ResetPasswordScreen')}>
                    <Text style={styles.forgot}>Forgot your password?</Text>
                  </TouchableOpacity>
                </View>
              ) : null}
              <Button
                style={{marginTop: 24}}
                loading={loading}
                mode="contained"
                onPress={() =>
                  isLogin ? onLoginPressed() : onSignUpPressed()
                }>
                {isLogin ? 'Login' : 'Sign up'}
              </Button>
              {/* <Button
                style={{marginTop: 20}}
                mode="contained"
                onPress={() => navigation.navigate('RegisterScreen')}>
                Sign up
              </Button> */}
              {/* <TouchableOpacity
                hitSlop={{top: 5, bottom: 11}}
                onPress={() => navigation.navigate('RegisterScreen')}
                style={[styles.row, {marginTop: 5}]}>
                <Text>Don’t have an account? </Text>
                <View>
                  <Text style={styles.link}>Sign up</Text>
                </View>
              </TouchableOpacity> */}

              <View
                style={{
                  width: '100%',
                  aspectRatio: 2.5,
                  marginBottom: 15,
                  alignItems: 'center',
                  marginTop: 25,
                }}>
                {/* <TouchableOpacity
                  onPress={onGoogleButtonPress}
                  style={{
                    width: '100%',
                    aspectRatio: 6,
                    backgroundColor: 'red',
                    flexDirection: 'row',
                    borderRadius: 5,
                    alignItems: 'center',
                  }}>
                  <View
                    style={{
                      height: '60%',
                      aspectRatio: 1,
                      marginLeft: 15,
                      justifyContent: 'center',
                    }}>
                    {loadingGoogle === true ? (
                      <ActivityIndicator color="white" />
                    ) : (
                      <Image
                        source={appImages.googlelogo}
                        style={{height: '100%', width: '100%'}}
                      />
                    )}
                  </View>
                  <Text
                    style={{
                      fontSize: 17,
                      color: colors.white,
                      fontWeight: 'bold',
                    }}>
                    {'   '}
                    Sign in with Google+
                  </Text>
                </TouchableOpacity> */}
                {/* {appleAuth.isSupported && (
                  <AppleButton
                    cornerRadius={5}
                    style={{
                      width: '85%',
                      aspectRatio: 6,
                      borderRadius: 5,
                      marginTop: -20,
                    }}
                    buttonStyle={AppleButton.Style.WHITE}
                    buttonType={AppleButton.Type.SIGN_IN}
                    onPress={() => onAppleButtonPress()}
                  />
                )} */}
              </View>
            </View>
          </SafeAreaView>
        </KeyboardAwareScrollView>
      </View>
      <Toast message={error} onDismiss={() => setError('')} />
    </View>
  );
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: colors.white,
  },
  flatListMainView: {
    marginTop: 10,
    marginHorizontal: 10,
    flex: 1,
  },
  backgroundVideo: {
    height: WINDOW_HEIGHT,
  },
  forgotPassword: {
    width: '100%',
    alignItems: 'flex-end',
  },
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  forgot: {
    fontSize: 13,
    color: '#414757',
  },
  link: {
    fontWeight: 'bold',
    color: '#121330',
  },
});

export default SigninScreen;
