import React, {useState} from 'react';
import {View, StyleSheet, TouchableOpacity, Text} from 'react-native';

import Button from '../../../components/Button';
import TextInput from '../../../components/TextInput';
import Toast from '../../../components/Toast';
import {theme} from '../../../config/theme';
import {BLACK, PRIMARY_CTA} from '../../../styles';
import {useDispatch} from 'react-redux';
import {setUserDetailsAction} from '../../../store/userDetails';
import {setAuthToken, setUserdetails} from '../../../helper/auth';
import {setAuthTokenAction} from '../../../store/auth';
import {colors} from 'react-native-elements';
import Logo from '../../../components/Logo';
import {appImages} from '../../../config';
import BackButton from '../../../components/BackButton';
import {firebase} from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';

export default function RegisterScreen({navigation}) {
  const [name, setName] = useState({value: '', error: ''});
  const [email, setEmail] = useState({value: '', error: ''});
  const [password, setPassword] = useState({value: '', error: ''});
  const [loading, setLoading] = useState();
  const [error, setError] = useState();

  const dispatch = useDispatch();

  function emailValidator(emailId) {
    const re = /\S+@\S+\.\S+/;
    if (!emailId) {
      return "Email can't be empty.";
    }
    if (!re.test(emailId)) {
      return 'Ooops! We need a valid email address.';
    }
    return '';
  }

  function passwordValidator(passwordOfUser) {
    if (!passwordOfUser) {
      return "Password can't be empty.";
    }
    if (passwordOfUser.length < 5) {
      return 'Password must be at least 5 characters long.';
    }
    return '';
  }

  function nameValidator(nameOfPerson) {
    if (!nameOfPerson) {
      return "Name can't be empty.";
    }
    return '';
  }

  const signUpUser = async (nameOfPerson, emailId, passwordOfPerson) => {
    try {
      const user = await auth().createUserWithEmailAndPassword(
        emailId,
        passwordOfPerson,
      );
      auth().currentUser.updateProfile({
        displayName: nameOfPerson,
      });
      return {user};
    } catch (err) {
      console.log('error=== ', err);
      return {
        error: err.message,
      };
    }
  };

  const onSignUpPressed = async () => {
    const nameError = nameValidator(name.value);
    const emailError = emailValidator(email.value);
    const passwordError = passwordValidator(password.value);
    if (emailError || passwordError || nameError) {
      setName({...name, error: nameError});
      setEmail({...email, error: emailError});
      setPassword({...password, error: passwordError});
      return;
    }
    setLoading(true);
    const response = await signUpUser(name.value, email.value, password.value);

    if (response.error) {
      setError(response.error);
    } else {
      console.log('response======= ', response, response?.user?.user?.uid);
      const uId = response?.user?.user?.uid;
      let userDetails = {
        userId: uId,
        userFullName: name.value,
        fontsize: 16,
        isDarkMode: true,
        themeColor: PRIMARY_CTA,
        titleDetailsFontColor: BLACK,
        titleFontColor: BLACK,
        isSetLock: false,
      };
      await firebase.firestore().collection('Users').doc(uId).set(userDetails);
      dispatch(setUserDetailsAction(userDetails));
      setUserdetails(JSON.stringify(userDetails));
      await setAuthToken(uId);
      dispatch(setAuthTokenAction(uId));
      navigation.pop(2);
    }
    setLoading(false);
  };

  // {"user": {"additionalUserInfo": {"isNewUser": true, "profile": null, "providerId": "password", "username": null}, "user": [Object]}} {"additionalUserInfo": {"isNewUser": true, "profile": null, "providerId": "password", "username": null}, "user": {"displayName": null, "email": "nikunjaaa1@gmail.com", "emailVerified": false, "isAnonymous": false, "metadata": [Object], "phoneNumber": null, "photoURL": null, "providerData": [Array], "providerId": "firebase", "refreshToken": "AFxQ4_oy2nGNsqitnST-9gQypqno13V6mURRe5sIE8FSORvSxY27bENDABq7eBUUh3xRXCZp-746-a7Sr6ffZZ6bLmimUw01NaGJOf3jIW0H4P4rc5RHSRVN7J3hVEchdaWpg_Z-MOkuZ0oB0Pyefo5vzZiMB5G5RbOrlxR_IQFm2tA69pAM-GffsO90YcFMEv_cB-NLWyADH7vyUnYknBGGQBSoDD4IwQ", "tenantId": null, "uid": "AYWvM9AhcaUNlBgiDYLYv1C3B112"}}

  return (
    <View style={styles.mainView}>
      <BackButton goBack={navigation.goBack} />
      <Logo style={{width: 120, height: 120}} logo={appImages.register} />
      <TextInput
        label="Name"
        returnKeyType="next"
        value={name.value}
        onChangeText={(text) => setName({value: text, error: ''})}
        error={!!name.error}
        errorText={name.error}
      />
      <TextInput
        label="Email"
        returnKeyType="next"
        value={email.value}
        onChangeText={(text) => setEmail({value: text, error: ''})}
        error={!!email.error}
        errorText={email.error}
        autoCapitalize="none"
        autoCompleteType="email"
        textContentType="emailAddress"
        keyboardType="email-address"
      />
      <TextInput
        label="Password"
        returnKeyType="done"
        value={password.value}
        onChangeText={(text) => setPassword({value: text, error: ''})}
        error={!!password.error}
        errorText={password.error}
        secureTextEntry
      />
      <Button
        loading={loading}
        mode="contained"
        onPress={onSignUpPressed}
        style={{marginTop: 24}}>
        Sign Up
      </Button>
      <View style={styles.row}>
        <Text>Already have an account? </Text>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Text style={styles.link}>Login</Text>
        </TouchableOpacity>
      </View>
      <Toast message={error} onDismiss={() => setError('')} />
    </View>
  );
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 30,
  },
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  link: {
    fontWeight: 'bold',
    color: theme.colors.primary,
  },
});
