import React, {useState} from 'react';
import BackButton from '../../../components/BackButton';
import Logo from '../../../components/Logo';
import Header from '../../../components/Header';
import TextInput from '../../../components/TextInput';
import Button from '../../../components/Button';
import Toast from '../../../components/Toast';
import {appImages} from '../../../config';
import {StyleSheet, View} from 'react-native';
import {colors} from 'react-native-elements';
import auth from '@react-native-firebase/auth';

export default function ResetPasswordScreen({navigation}) {
  const [email, setEmail] = useState({value: '', error: ''});
  const [loading, setLoading] = useState(false);
  const [toast, setToast] = useState({value: '', type: ''});

  function emailValidator(email) {
    const re = /\S+@\S+\.\S+/;
    if (!email) return "Email can't be empty.";
    if (!re.test(email)) return 'Ooops! We need a valid email address.';
    return '';
  }

  const sendEmailWithPassword = async (email) => {
    try {
      await auth().sendPasswordResetEmail(email);
      return {};
    } catch (error) {
      return {
        error: error.message,
      };
    }
  };

  const sendResetPasswordEmail = async () => {
    const emailError = emailValidator(email.value);
    if (emailError) {
      setEmail({...email, error: emailError});
      return;
    }
    setLoading(true);
    const response = await sendEmailWithPassword(email.value);
    if (response.error) {
      setToast({type: 'error', message: response.error});
    } else {
      setToast({
        type: 'success',
        message: 'Email with password has been sent.',
      });
    }
    setLoading(false);
  };

  return (
    <View style={styles.mainView}>
      <BackButton goBack={navigation.goBack} />
      <Logo logo={appImages.forgotPassword} />
      <Header>Restore Password</Header>
      <TextInput
        label="E-mail address"
        returnKeyType="done"
        value={email.value}
        onChangeText={(text) => setEmail({value: text, error: ''})}
        error={!!email.error}
        errorText={email.error}
        autoCapitalize="none"
        autoCompleteType="email"
        textContentType="emailAddress"
        keyboardType="email-address"
        description="You will receive email with password reset link."
      />
      <Button
        loading={loading}
        mode="contained"
        onPress={sendResetPasswordEmail}
        style={{marginTop: 16}}>
        Send Instructions
      </Button>
      <Toast {...toast} onDismiss={() => setToast({value: '', type: ''})} />
    </View>
  );
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 30,
  },
});
