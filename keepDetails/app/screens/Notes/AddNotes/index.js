import {isEmpty} from 'lodash';
import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  Keyboard,
  AppState,
  TouchableWithoutFeedback,
  StatusBar,
  ActivityIndicator,
} from 'react-native';
import storage from '@react-native-firebase/storage';

import {useSelector} from 'react-redux';
import ColorPickerModal from '../../../components/ColorPickerModal';
import TopBar from '../../../components/TopBar';
import QuillEditor, {QuillToolbar} from 'react-native-cn-quill';
import {
  addDetails,
  updateDetails,
} from '../../../firebaseServices/Details/details';
import {firebase} from '@react-native-firebase/firestore';
import ImagePicker from 'react-native-image-crop-picker';
import CustomActionsheet from '../../../components/CustomActionsheet';
import {
  CANCEL,
  CHOOSE_FROM_GALLERY,
  TAKE_PHOTO,
} from '../../../config/constants';

function AddNotes({navigation, route}) {
  const [selectedItem, setSelectedItem] = useState(
    route?.params?.selectedItem ?? {},
  );
  const _editor = useRef();
  const userDetails = useSelector((state) => state.userDetails ?? {});
  const authToken = useSelector((state) => state.auth?.authToken ?? '');

  const [txtNote, setTxtNote] = useState(selectedItem?.note ?? '');

  const [isLock, setIsLock] = useState(!!selectedItem?.isLock ?? '');

  const [isPinned, setIsPinned] = useState(!!selectedItem?.isPinned ?? '');

  const [actionsheetVisible, setActionsheetVisible] = useState(false);

  const [isImageLoading, setIsImageLoading] = useState(false);

  const [noteColor, setNoteColor] = useState(selectedItem?.color ?? '#cfbe9b');
  const [isColorPickerOpen, setIsColorPickerOpen] = useState(false);

  const actionSheetItemsForPostPrivacy = [
    {
      text: CHOOSE_FROM_GALLERY,
      onPress: () => {
        choosePhotoFromGallery();
      },
    },
    {
      text: TAKE_PHOTO,
      onPress: () => {
        takePhotoFromCamera();
      },
    },
    {
      text: CANCEL,
      onPress: () => setActionsheetVisible(false),
    },
  ];

  const choosePhotoFromGallery = () => {
    try {
      setActionsheetVisible(false);
      setTimeout(() => {
        ImagePicker.openPicker({
          width: 300,
          height: 400,
          cropping: true,
          compressImageQuality: 0.8,
        })
          .then(async (response) => {
            console.log('response==== ', response?.path);
            uploadImage(response?.path);
          })
          .catch((err) => {
            console.log('[AddNotes] choosePhotoFromGallery', err);
          });
      }, 500);
    } catch (error) {}
  };

  const uploadImage = async (imgPath) => {
    setIsImageLoading(true);

    try {
      const autoId = firebase.firestore().collection('Users').doc().id;
      const imagePath = `Images/useridIsHere//${'Notes'}/${autoId}`;

      await storage().ref(imagePath).putFile(imgPath);
      let imageRef = await storage()
        .ref('/' + imagePath)
        .getDownloadURL();
      _editor.current?.insertEmbed(0, 'image', imageRef);
    } catch (error) {
      console.log('[AddNotes] uploadImage ', error);
    } finally {
      setIsImageLoading(false);
    }
  };

  const takePhotoFromCamera = async () => {
    setActionsheetVisible(false);

    setTimeout(() => {
      ImagePicker.openCamera({
        cancelable: true,
        compressImageQuality: 0.8,
      })
        .then(async (response) => {
          console.log('response==== ', response);
          uploadImage(response?.path);
        })
        .catch((error) => {
          console.log('error in Camera ===>', error);
        })
        .finally(() => {});
    }, 600);
  };

  const _handleSubmitButton = async () => {
    if (!authToken) {
      navigation.navigate('Signin');
      return;
    }

    let data = {
      note: txtNote,
      color: noteColor,
      isLock: isLock,
    };

    console.log('data====== ', data, selectedItem);

    if (isEmpty(selectedItem)) {
      if (txtNote) {
        setSelectedItem(data);
        // data['createdAt'] = firebase?.firestore?.FieldValue.serverTimestamp();
        await addDetails(
          'notes',
          {
            ...data,
            createdDate: new Date(),
          },
          authToken,
        );

        return;
      }
    }
    if (
      txtNote !== selectedItem?.note ||
      isLock !== selectedItem?.isLock ||
      noteColor !== selectedItem?.color
    ) {
      await updateDetails('notes', {...data, id: selectedItem?.id}, authToken);
    }
  };

  const _handleBackPress = () => {
    // _handleSubmitButton();

    navigation.goBack();
  };

  const _handleColorPickerPress = () => {
    setIsColorPickerOpen(true);
  };

  const _handleChangeColorPress = (color) => {
    setNoteColor(color);
    setIsColorPickerOpen(false);
  };

  const appState = useRef(AppState.currentState);
  const [appStateVisible, setAppStateVisible] = useState(appState.current);

  useEffect(() => {
    AppState.addEventListener('change', _handleAppStateChange);

    return () => {
      AppState.removeEventListener('change', _handleAppStateChange);
    };
  }, []);

  const _handleAppStateChange = (nextAppState) => {
    appState.current = nextAppState;
    setAppStateVisible(appState.current);
  };

  useEffect(() => {
    if (appStateVisible === 'background') {
      _handleSubmitButton();
    }
  }, [appStateVisible]);

  const customHandler = (name, value) => {
    if (name === 'image') {
      setActionsheetVisible(true);
      // _editor.current?.insertEmbed(0, 'image', 'https://picsum.photos/200/300');
    } else if (name === 'clock') {
      _editor.current?.insertText(0, `Today is ${this.getCurrentDate()}`, {
        bold: true,
        color: 'red',
      });
    } else {
      console.log(`${name} clicked with value: ${value}`);
    }
  };

  const handleTextChange = (data) => {
    if (data.source === 'api') {
      console.log('An API call triggered this change.');
    } else if (data.source === 'user') {
      console.log('55678768978908790 ', data);
    }
  };

  return (
    <View
      style={{
        width: '100%',
        flex: 1,
      }}>
      <ColorPickerModal
        defaultColor={noteColor}
        visible={isColorPickerOpen}
        onChangeColor={_handleChangeColorPress}
      />
      <StatusBar
        animated={true}
        backgroundColor={noteColor}
        barStyle={'dark-content'}
        showHideTransition={'fade'}
        hidden={false}
      />
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View
          style={{
            width: '100%',
            flex: 1,
          }}>
          <View
            style={{
              width: '100%',
              flex: 1,
              backgroundColor: noteColor,
            }}></View>
          <View
            style={{
              width: '100%',
              height: '100%',
              position: 'absolute',
            }}>
            <SafeAreaView />
            <TopBar
              onBack={_handleBackPress}
              onColorPicker={_handleColorPickerPress}
              onLockShow={true}
              onUnLock={isLock}
              onPressLock={() => {
                if (!userDetails?.password) {
                  navigation.navigate('PinLock');
                }
                setIsLock(!isLock);
              }}
              containerStyle={{backgroundColor: 'rgba(0,0,0,0)'}}
              isTouchableButton={
                txtNote !== selectedItem?.note ||
                isLock !== selectedItem?.isLock ||
                noteColor !== selectedItem?.color
              }
              onPressTouchableButton={_handleSubmitButton}
              touchableButtonTitle={isEmpty(selectedItem) ? 'Save' : 'Update'}
            />

            {isImageLoading ? <ActivityIndicator /> : null}

            <View style={{width: '100%', flex: 1}}>
              <QuillEditor
                style={[
                  {
                    root: (provided) => ({
                      ...provided,
                    }),
                    backgroundColor: noteColor,
                  },
                ]}
                import3rdParties="cdn"
                ref={_editor}
                nestedScrollEnabled
                theme={{background: noteColor}}
                quill={{
                  // not required just for to show how to pass this props
                  placeholder: 'Enter text',
                  modules: {
                    toolbar: false, // this is default value
                  },
                }}
                webview={{nestedScrollEnabled: true}}
                onTextChange={handleTextChange}
                onHtmlChange={({html}) => setTxtNote(html)}
                initialHtml={txtNote}
              />
              {/* </ScrollView> */}
            </View>

            <QuillToolbar
              styles={{
                toolbar: {
                  provider: (provided) => ({
                    ...provided,
                    borderTopWidth: 0,
                  }),
                  root: (provided) => ({
                    ...provided,
                    backgroundColor: noteColor,
                  }),
                },
              }}
              editor={_editor}
              options="full"
              theme="light"
              custom={{
                handler: customHandler,
                actions: ['image', 'clock'],
                // icons: {
                //   clock: clockIcon,
                // },
              }}
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
      <CustomActionsheet
        isVisible={actionsheetVisible}
        actionItems={actionSheetItemsForPostPrivacy}
        OnBackdropPress={() => {
          setActionsheetVisible(false);
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
  },
  editor: {},
});

export default AddNotes;
