import React, {useState, useEffect, useRef, useCallback} from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  SafeAreaView,
  TouchableOpacity,
  ActionSheetIOS,
  Text,
} from 'react-native';
import moment from 'moment';
import Picker from 'react-native-picker-select';
import ActionSheet from 'react-native-actionsheet';

import {PRIMARY_CTA} from '../../../styles';
import TopBar from '../../../components/TopBar';

function AddReminder({navigation}) {
  const [selectDate, setSelectDate] = useState(moment().format('DD/MM/YYYY'));
  const [selectTime, setSelectTime] = useState(moment().format('hh : mm A'));
  const [repeatSelectionType, setSepeatSelectionType] = useState('never');

  const actionSheetRef = useRef(null);

  const _handleActionSheet = useCallback(
    (buttonIndex) => {
      switch (buttonIndex) {
        case 1:
          setSepeatSelectionType('Never');
          break;
        case 2:
          setSepeatSelectionType('Hourly');
          break;
        case 3:
          setSepeatSelectionType('Daily');
          break;
        case 4:
          setSepeatSelectionType('Weekly');
          break;
        case 5:
          setSepeatSelectionType('Monthly');
          break;
      }
    },
    [navigation],
  );

  const _handleMenuPress = useCallback(() => {
    if (Platform.OS === 'android') {
      actionSheetRef.current.show();
      return;
    }
    ActionSheetIOS.showActionSheetWithOptions(
      {
        options: ['Done', 'Never', 'Hourly', 'Daily', 'Weekly', 'Monthly'],
        cancelButtonIndex: 0,
        tintColor: PRIMARY_CTA,
        message: 'Reeat Actions',
      },
      _handleActionSheet,
    );
  }, [_handleActionSheet]);

  const _handleBackPress = () => {
    navigation.goBack();
  };

  useEffect(() => {}, []);
  return (
    <>
      <ActionSheet
        ref={actionSheetRef}
        title={'Reeat Actions'}
        options={['Done', 'Never', 'Hourly', 'Daily', 'Weekly', 'Monthly']}
        cancelButtonIndex={0}
        tintColor={PRIMARY_CTA}
        onPress={_handleActionSheet}
      />
      <TopBar onBack={_handleBackPress} headingText={'Add Notes'} />

      <View style={styles.mainView}>
        <View
          style={{
            width: '95%',
            height: 60,
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: 'red',
          }}>
          <Text style={{marginLeft: 10}}>Date</Text>
          <Text style={{textAlign: 'right', flex: 1, marginRight: 10}}>
            {selectDate}
          </Text>
        </View>

        <View
          style={{
            width: '95%',
            height: 60,
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: 'red',
            marginTop: 10,
          }}>
          <Text style={{marginLeft: 10}}>Time</Text>
          <Text style={{textAlign: 'right', flex: 1, marginRight: 10}}>
            {selectTime}
          </Text>
        </View>

        <View
          style={{
            width: '95%',
            height: 60,
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: 'red',
            marginTop: 10,
          }}>
          <Text style={{marginLeft: 10}}>Repeat</Text>
          <View
            style={{
              alignItems: 'flex-end',
              flex: 1,
              marginRight: 10,
            }}>
            <Text onPress={_handleMenuPress} style={{textAlign: 'right'}}>
              {' '}
              {repeatSelectionType}
            </Text>
          </View>
        </View>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  mainView: {
    width: '100%',
    flex: 1,
    alignItems: 'center',
  },
  flatListMainView: {
    marginTop: 10,
    marginHorizontal: 10,
    flex: 1,
  },
  creditOrDebitListItemMainView: {
    width: '100%',
    aspectRatio: 3.5,
    flexDirection: 'row',
    backgroundColor: 'pink',
  },
});
export default AddReminder;
