import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Text,
  Alert,
  Image,
} from 'react-native';
import {useSelector} from 'react-redux';
import {appImages} from '../../config';
import AddButton from '../../components/AddButton';
import {useNavigation} from '@react-navigation/core';
import {BLACK, responsiveScale, scaleSize, WHITE} from '../../styles';
import EmptyView from '../../components/EmptyView';
import moment from 'moment';
import {getValueFromLocal, setValueInLocal} from '../../helper/auth';
import {SwipeListView} from 'react-native-swipe-list-view';
import {deleteDetails} from '../../firebaseServices/Details/details';
import {ActivityIndicator} from 'react-native-paper';
import WebView from 'react-native-webview';
import Delete from '../../assets/appimages/Delete.svg';
import Search from '../../assets/appimages/Search.svg';
import {
  AdEventType,
  InterstitialAd,
  BannerAd,
  BannerAdSize,
} from '@react-native-firebase/admob';
import {bannerAdUnitId, interstitialAdUnitId} from '../../config/constants';
import {firebase} from '@react-native-firebase/firestore';

const interstitial = InterstitialAd.createForAdRequest(interstitialAdUnitId, {
  requestNonPersonalizedAdsOnly: true,
});

function Notes() {
  const [arrDetails, setArrDetails] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const authToken = useSelector((state) => state.auth?.authToken ?? '');
  const userDetails = useSelector((state) => state.userDetails ?? {});
  const navigation = useNavigation();

  useEffect(() => {
    const eventListener = interstitial.onAdEvent((type) => {
      if (type === AdEventType.LOADED) {
        setTimeout(() => {
          interstitial.show();
        }, 2000);
      }
    });

    // Unsubscribe from events on unmount
    return () => {
      eventListener();
    };
  }, []);

  const initializeAdd = async () => {
    const notes = await getValueFromLocal('notes');
    console.log('notes=== ', JSON.parse(notes));
    if (notes === null) {
      setValueInLocal('notes', JSON.stringify(0));
    } else if (JSON.parse(notes) >= 3) {
      interstitial.load();
      setValueInLocal('notes', JSON.stringify(0));
    } else {
      setValueInLocal('notes', JSON.stringify(JSON.parse(notes) + 1));
    }
  };

  useEffect(() => {
    const getIdentificationsData = () => {
      setIsLoading(true);
      firebase
        .firestore()
        .collection('Users')
        .doc(authToken)
        .collection('notes')
        .orderBy('createdDate', 'desc')
        .onSnapshot((snapshot) => {
          const data = snapshot.docs.map((doc, index) => {
            return {
              id: doc.id,
              ...doc.data(),
            };
          });
          setIsLoading(false);
          setArrDetails(data);
        });
    };
    // AdMobInterstitial.setAdUnitID(appData.fullScreenAdId);
    // AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);
    if (!authToken) {
      setArrDetails([]);
      return getIdentificationsData();
    }
    getIdentificationsData();
  }, [authToken]);

  const _handleAddNotesItemPress = (item) => {
    // console.log('userDetails?.recoverEmailId  === ', !!userDetails?.recoverEmailId , item?.isLock);
    initializeAdd();
    navigation.navigate('AddNotes', {
      selectedItem: item,
    });
    if (!!userDetails?.recoverEmailId && item?.isLock) {
      navigation.navigate('LockScreen');
    }
  };

  const NotesListItem = ({item}) => {
    return (
      <TouchableOpacity
        key={item?.id}
        activeOpacity={0.98}
        style={styles.creditOrDebitListItemMainView}
        onPress={() => _handleAddNotesItemPress(item)}>
        <View
          style={{
            width: '100%',
            backgroundColor: item?.color,
            borderRadius: 15,
            overflow: 'hidden',
            flex: 1,
          }}>
          <Image
            source={item?.isLock ? appImages.lock : appImages.unlock}
            style={{
              marginTop: 10,
              marginLeft: 10,
              height: 15,
              width: 15,
              resizeMode: 'contain',
            }}
          />
          <Text
            style={{
              marginLeft: 10,
              fontFamily: 'Poppins-Medium',
              fontSize: responsiveScale(7),
            }}>
            {`Date: ${
              moment(item?.createdDate?.toDate())?.format(
                'dddd, MMMM Do YYYY hh:mm:ss a',
              ) ?? ''
            }`}
          </Text>
          <View style={{marginHorizontal: 10, height: 100}}>
            <WebView
              injectedJavaScript={`const meta = document.createElement('meta'); meta.setAttribute('content', 'width=device-width+50, initial-scale=0.5, maximum-scale=0.5, user-scalable=0'); meta.setAttribute('name', 'viewport'); document.getElementsByTagName('head')[0].appendChild(meta); `}
              scalesPageToFit={false}
              source={{html: item.note}}
              showsHorizontalScrollIndicator={false}
              showsVerticalScrollIndicator={false}
              style={{
                backgroundColor: 'rgba(0,0,0,0)',
              }}
            />
            <View
              style={{
                width: '100%',
                height: 100,
                position: 'absolute',
                backgroundColor: 'rgba(0,0,0,0)',
              }}
            />
          </View>
          <Text
            style={{
              alignSelf: 'flex-end',
              backgroundColor: 'red',
            }}>
            {item.date}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  const SwipeViewListItem = ({item, index, navigation}) => {
    console.log('item==== ', item);
    return (
      <View
        key={item?.id}
        activeOpacity={0.85}
        style={[styles.creditOrDebitListItemMainView]}>
        <View
          style={{
            width: '100%',
            height: 90,
            borderRadius: 15,
            overflow: 'hidden',
            alignItems: 'flex-end',
          }}>
          <TouchableOpacity
            onPress={() => {
              Alert.alert('', 'Do you really want to delete this item?', [
                {
                  text: 'Cancel',
                  onPress: () => console.log('Cancel Pressed'),
                  style: 'cancel',
                },
                {
                  text: 'OK',
                  onPress: () => deleteDetails('notes', item?.item, authToken),
                },
              ]);
            }}
            style={{
              height: 90,
              width: 90,
              backgroundColor: 'red',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View style={{height: 50, width: 50}}>
              <Delete />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  return (
    <View style={styles.mainView}>
      <SafeAreaView />
      <View style={{marginHorizontal: 10}}>
        <View style={styles.titleView}>
          <Text style={styles.headerTitle}>My Notes</Text>
          <View
            style={{
              height: '100%',
              flex: 1,
              flexDirection: 'row-reverse',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              onPress={() => {
                if (!authToken) {
                  navigation.navigate('Signin');
                  return;
                }
                navigation.navigate('SearchNotesScreen', {
                  defaultDetails: arrDetails,
                });
              }}
              style={{
                height: '80%',
                aspectRatio: 1,
              }}>
              <Search height={'100%'} width={'100%'} />
            </TouchableOpacity>
          </View>
        </View>
        <BannerAd
          size={BannerAdSize.SMART_BANNER}
          requestOptions={{
            requestNonPersonalizedAdsOnly: true,
          }}
          unitId={bannerAdUnitId}
        />
      </View>
      {arrDetails?.length > 0 ? (
        <View style={styles.flatListMainView}>
          <SwipeListView
            data={arrDetails}
            rightOpenValue={-80}
            renderItem={({item, index}) => (
              <NotesListItem
                item={item}
                index={index}
                navigation={navigation}
              />
            )}
            renderHiddenItem={(data, rowMap) => (
              <SwipeViewListItem item={data} navigation={navigation} />
            )}
          />
        </View>
      ) : (
        <>
          {!isLoading ? (
            <EmptyView
              title={'Empty Notes!'}
              titleImage={appImages.emptyNotes}
              titleStyle={{marginTop: scaleSize(-55)}}
            />
          ) : (
            <ActivityIndicator size="large" color={'black'} />
          )}
        </>
      )}

      <AddButton
        onPress={() => {
          initializeAdd();
          navigation.navigate('AddNotes');
        }}
      />
      <SafeAreaView />
    </View>
  );
}

const styles = StyleSheet.create({
  mainView: {
    width: '100%',
    flex: 1,
    backgroundColor: WHITE,
  },
  flatListMainView: {
    marginTop: 10,
    marginHorizontal: 10,
    flex: 1,
    marginBottom: 10,
    overflow: 'visible',
  },
  creditOrDebitListItemMainView: {
    width: '100%',
    marginTop: 10,
  },
  adMobView: {
    width: '100%',
    overflow: 'visible',
    marginVertical: 10,
    borderRadius: 10,
  },
  titleView: {
    width: '100%',
    backgroundColor: WHITE,
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerTitle: {
    textAlign: 'left',
    color: BLACK,
    fontFamily: 'Poppins-Medium',
    fontSize: 25,
  },
});
export default Notes;
