import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Text,
  Alert,
  Image,
} from 'react-native';
import {useSelector} from 'react-redux';
import {appImages} from '../../config';
import {useNavigation} from '@react-navigation/core';
import {BLACK, responsiveScale, scaleSize, WHITE} from '../../styles';
import EmptyView from '../../components/EmptyView';
import moment from 'moment';
import {getValueFromLocal, setValueInLocal} from '../../helper/auth';
import {SwipeListView} from 'react-native-swipe-list-view';
import {deleteDetails} from '../../firebaseServices/Details/details';
import WebView from 'react-native-webview';
import Delete from '../../assets/appimages/Delete.svg';
import {AdEventType, InterstitialAd} from '@react-native-firebase/admob';
import {interstitialAdUnitId} from '../../config/constants';
import {Searchbar} from 'react-native-paper';

const interstitial = InterstitialAd.createForAdRequest(interstitialAdUnitId, {
  requestNonPersonalizedAdsOnly: true,
});

function SearchNotesScreen({route}) {
  const [arrsDetails, setArrDetails] = useState(
    route?.params?.defaultDetails || [],
  );
  const [txtSearchBar, setTxtSearchBar] = useState('');
  const [isLoading, setIsLoading] = useState(true);

  const authToken = useSelector((state) => state.auth?.authToken ?? '');
  const userDetails = useSelector((state) => state.userDetails ?? {});
  const navigation = useNavigation();

  useEffect(() => {
    const eventListener = interstitial.onAdEvent((type) => {
      if (type === AdEventType.LOADED) {
        setTimeout(() => {
          interstitial.show();
        }, 2000);
      }
    });

    // Unsubscribe from events on unmount
    return () => {
      eventListener();
    };
  }, []);

  useEffect(() => {
    console.log('txtSearchBar==== ', txtSearchBar);
  }, [txtSearchBar]);

  const initializeAdd = async () => {
    const notes = await getValueFromLocal('notes');
    console.log('notes=== ', JSON.parse(notes));
    if (notes === null) {
      setValueInLocal('notes', JSON.stringify(0));
    } else if (JSON.parse(notes) >= 3) {
      interstitial.load();
      setValueInLocal('notes', JSON.stringify(0));
    } else {
      setValueInLocal('notes', JSON.stringify(JSON.parse(notes) + 1));
    }
  };

  const _handleAddNotesItemPress = (item) => {
    // console.log('userDetails?.recoverEmailId  === ', !!userDetails?.recoverEmailId , item?.isLock);
    initializeAdd();
    navigation.navigate('AddNotes', {
      selectedItem: item,
    });
    if (!!userDetails?.recoverEmailId && item?.isLock) {
      navigation.navigate('LockScreen');
    }
  };

  const NotesListItem = ({item}) => {
    return (
      <TouchableOpacity
        key={item?.id}
        activeOpacity={0.98}
        style={styles.creditOrDebitListItemMainView}
        onPress={() => _handleAddNotesItemPress(item)}>
        <View
          style={{
            width: '100%',
            backgroundColor: item?.color,
            borderRadius: 15,
            overflow: 'hidden',
            flex: 1,
          }}>
          <Image
            source={item?.isLock ? appImages.lock : appImages.unlock}
            style={{
              marginTop: 10,
              marginLeft: 10,
              height: 15,
              width: 15,
              resizeMode: 'contain',
            }}
          />
          <Text
            style={{
              marginLeft: 10,
              fontFamily: 'Poppins-Medium',
              fontSize: responsiveScale(7),
            }}>
            {`Date: ${
              moment(item?.createdDate?.toDate())?.format(
                'dddd, MMMM Do YYYY hh:mm:ss a',
              ) ?? ''
            }`}
          </Text>
          <View style={{marginHorizontal: 10, height: 100}}>
            <WebView
              injectedJavaScript={`const meta = document.createElement('meta'); meta.setAttribute('content', 'width=device-width, initial-scale=0.5, maximum-scale=0.5, user-scalable=0'); meta.setAttribute('name', 'viewport'); document.getElementsByTagName('head')[0].appendChild(meta); `}
              scalesPageToFit={false}
              source={{html: item.note}}
              showsHorizontalScrollIndicator={false}
              showsVerticalScrollIndicator={false}
              style={{
                backgroundColor: 'rgba(0,0,0,0)',
                opacity: 0.99,
              }}
            />
            <View
              style={{
                width: '100%',
                height: 100,
                position: 'absolute',
                backgroundColor: 'rgba(0,0,0,0)',
              }}
            />
          </View>
          <Text
            style={{
              alignSelf: 'flex-end',
              backgroundColor: 'red',
            }}>
            {item.date}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  const SwipeViewListItem = ({item, index, navigation}) => {
    return (
      <View
        key={item?.id}
        activeOpacity={0.85}
        style={[styles.creditOrDebitListItemMainView]}>
        <View
          style={{
            width: '100%',
            height: 90,
            borderRadius: 15,
            overflow: 'hidden',
            alignItems: 'flex-end',
          }}>
          <TouchableOpacity
            onPress={() => {
              Alert.alert('', 'Do you really want to delete this item?', [
                {
                  text: 'Cancel',
                  onPress: () => console.log('Cancel Pressed'),
                  style: 'cancel',
                },
                {
                  text: 'OK',
                  onPress: () => deleteDetails('notes', item?.item, authToken),
                },
              ]);
            }}
            style={{
              height: 90,
              width: 90,
              backgroundColor: 'red',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View style={{height: 50, width: 50}}>
              <Delete />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  return (
    <View style={styles.mainView}>
      <SafeAreaView />
      <View style={styles.titleView}>
        <TouchableOpacity
          onPress={navigation.goBack}
          style={{
            height: 35,
            width: 35,
            marginLeft: 15,
            marginRight: 10,
            justifyContent: 'center',
          }}>
          <Image
            resizeMode={'contain'}
            style={{height: 25, width: 30, resizeMode: 'contain'}}
            source={appImages.back}
          />
        </TouchableOpacity>
        <View
          style={{
            height: '100%',
            flex: 1,
            paddingRight: 10,
            justifyContent: 'center',
          }}>
          <Searchbar
            placeholder="Search"
            onChangeText={(value) => {
              if (value === '') {
                setArrDetails(route?.params?.defaultDetails);
              } else {
                const arrFilteredData = route?.params?.defaultDetails?.filter(
                  (item) =>
                    item.note?.toLowerCase()?.includes(value.toLowerCase()),
                );
                setArrDetails(arrFilteredData);
              }
            }}
            // onIconPress={() => {
            //   getIdentificationsData();
            // }}
          />
        </View>
      </View>
      <View style={{width: '100%', flex: 1}}>
        {arrsDetails?.length > 0 ? (
          <View style={styles.flatListMainView}>
            <SwipeListView
              data={arrsDetails}
              rightOpenValue={-80}
              renderItem={({item, index}) => (
                <NotesListItem
                  key={item.id}
                  item={item}
                  index={index}
                  navigation={navigation}
                />
              )}
              renderHiddenItem={(data, rowMap) => (
                <SwipeViewListItem item={data} navigation={navigation} />
              )}
            />
          </View>
        ) : (
          <>
            <EmptyView
              title={'Empty Notes!'}
              titleImage={appImages.emptyNotes}
              titleStyle={{marginTop: scaleSize(-55)}}
            />
          </>
        )}
      </View>

      <SafeAreaView />
    </View>
  );
}

const styles = StyleSheet.create({
  mainView: {
    width: '100%',
    flex: 1,
    backgroundColor: WHITE,
  },
  flatListMainView: {
    marginTop: 10,
    marginHorizontal: 10,
    flex: 1,
    marginBottom: 10,
    overflow: 'visible',
  },
  creditOrDebitListItemMainView: {
    width: '100%',
    marginTop: 10,
  },
  adMobView: {
    width: '100%',
    overflow: 'visible',
    marginVertical: 10,
    borderRadius: 10,
  },
  titleView: {
    width: '100%',
    height: 60,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerTitle: {
    textAlign: 'left',
    color: BLACK,
    fontFamily: 'Poppins-Medium',
    fontSize: 25,
  },
});
export default SearchNotesScreen;
