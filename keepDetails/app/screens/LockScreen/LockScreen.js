import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  Text,
  SafeAreaView,
  BackHandler,
  TouchableOpacity,
} from 'react-native';
import {useSelector} from 'react-redux';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {scaleFont, THEME_COLOR, WHITE, WINDOW_WIDTH} from '../../styles';

import {Root, Popup} from 'popup-ui';
import TextInputWithTitle from '../../components/TextInputWithTitle';
import LockShow from '../../assets/appimages/LockShow.svg';

function LockScreen({navigation}) {
  const userDetails = useSelector((state) => state.userDetails ?? {});

  const [txtPassword, setTxtPassword] = useState();

  const handleTxtPassword = (text) => {
    setTxtPassword(text);
  };

  const handleForgotPasswordSubmit = async () => {
    navigation.navigate('ForgotPassword');
  };

  const handleSubmit = async () => {
    if (txtPassword !== userDetails?.password ?? '') {
      Popup.show({
        type: 'Warning',
        title: 'Password Incorrect!',
        textBody: 'Please enter a valid password.',
        buttonText: 'Ok',
        callback: () => Popup.hide(),
      });
      return;
    }
    navigation.goBack();
  };

  React.useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackBtnPressed);
    return () => {
      BackHandler.removeEventListener(
        'hardwareBackPress',
        handleBackBtnPressed,
      );
    };
  }, []);

  const handleBackBtnPressed = () => {
    // navigation.goBack(null);
    return true;
  };

  return (
    <View style={{flex: 1, backgroundColor: WHITE}}>
      <Root style={{flex: 1}}>
        <SafeAreaView />
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps="handled"
          style={{flex: 1}}>
          <View style={styles.mainView}>
            <LockShow width={WINDOW_WIDTH * 0.4} height={WINDOW_WIDTH * 0.4} />
            <Text
              style={{
                fontSize: 30,
                marginVertical: 25,
                textAlign: 'center',
                fontFamily: 'Poppins-Regular',
                fontSize: scaleFont(16),
              }}>
              {' '}
              Please enter your selected passcode for Unlock the App!
            </Text>

            <TextInputWithTitle
              title={'Enter your password'}
              autoCapitalize="none"
              value={txtPassword}
              onChangeText={handleTxtPassword}
              textAlign={'center'}
            />
          </View>
          <TouchableOpacity
            style={{
              width: WINDOW_WIDTH - 60,
              backgroundColor: THEME_COLOR,
              alignSelf: 'center',
              marginVertical: 20,
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 4,
              padding: 10,
            }}
            onPress={handleSubmit}>
            <Text
              style={{
                color: WHITE,
                fontFamily: 'Poppins-Regular',
                fontSize: 16,
              }}>
              UNLOCK
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              width: WINDOW_WIDTH - 60,
              backgroundColor: THEME_COLOR,
              alignSelf: 'center',
              marginVertical: 20,
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 4,
              padding: 10,
            }}
            onPress={handleForgotPasswordSubmit}>
            <Text
              style={{
                color: WHITE,
                fontFamily: 'Poppins-Regular',
                fontSize: 16,
              }}>
              FORGOT PASSCODE?
            </Text>
          </TouchableOpacity>
        </KeyboardAwareScrollView>
      </Root>
    </View>
  );
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 25,
  },
});

export default LockScreen;
