import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import {useSelector} from 'react-redux';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import TopBar from '../../components/TopBar';
import {THEME_COLOR, WHITE, WINDOW_WIDTH} from '../../styles';

import {Root, Popup} from 'popup-ui';
import LockShow from '../../assets/appimages/LockShow.svg';
import {firebase} from '@react-native-firebase/firestore';

function ForgotPassword({navigation}) {
  const userDetails = useSelector((state) => state.userDetails ?? {});

  const handleSubmit = async () => {
    firebase
      .firestore()
      .collection('mail')
      .add({
        to: userDetails?.recoverEmailId,
        message: {
          subject: 'Hello from KeepDetails!',
          html: `Your forgot password is '${userDetails?.password}'.`,
        },
      });

    Popup.show({
      type: 'Success',
      title: 'Please check your email!',
      textBody:
        'If you not get the emailid then please check in you spam list.',
      buttonText: 'Ok',
      callback: () => Popup.hide(),
    });
  };

  const _handleBackPress = () => {
    navigation.goBack();
  };

  return (
    <View style={{flex: 1, backgroundColor: WHITE}}>
      <Root style={{flex: 1}}>
        <SafeAreaView />
        <TopBar onBack={_handleBackPress} />
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps="handled"
          style={{flex: 1}}>
          <View style={styles.mainView}>
            <LockShow width={WINDOW_WIDTH * 0.4} height={WINDOW_WIDTH * 0.4} />
            <Text
              style={{
                marginTop: 25,
                textAlign: 'center',
                fontFamily: 'Poppins-Regular',
                fontSize: 16,
              }}>
              Your recovery emailid is
            </Text>
            <Text
              style={{
                marginTop: 10,
                textAlign: 'center',
                fontFamily: 'Poppins-Regular',
                fontSize: 17,
              }}>
              {userDetails?.recoverEmailId}
            </Text>

            <Text
              style={{
                marginTop: 25,
                textAlign: 'center',
                fontFamily: 'Poppins-Regular',
                fontSize: 16,
              }}>
              We all forget sometimes...
            </Text>

            <Text
              style={{
                marginTop: 25,
                textAlign: 'center',
                fontFamily: 'Poppins-Regular',
                fontSize: 16,
              }}>
              Click "Send code!" to have the code sent to your email.
            </Text>
            <TouchableOpacity
              style={{
                backgroundColor: THEME_COLOR,
                alignSelf: 'center',
                marginVertical: 20,
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 4,
                padding: 10,
                width: WINDOW_WIDTH - 60,
              }}
              onPress={handleSubmit}>
              <Text
                style={{
                  color: WHITE,
                  fontFamily: 'Poppins-Regular',
                  fontSize: 16,
                }}>
                SEND PASSCODE!
              </Text>
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
      </Root>
    </View>
  );
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    alignItems: 'center',
  },
});

export default ForgotPassword;
