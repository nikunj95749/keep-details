import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  Text,
  SafeAreaView,
  TouchableOpacity,
  Image,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {appImages} from '../../../config';
import {BLACK, THEME_COLOR, WHITE, WINDOW_WIDTH} from '../../../styles';
import {useDispatch, useSelector} from 'react-redux';
import {setPassword, setRecoverEmailId} from '../../../store/userDetails';
import {Root, Popup} from 'popup-ui';
import AwesomeAlert from 'react-native-awesome-alerts';
import TextInputWithTitle from '../../../components/TextInputWithTitle';
import LockShow from '../../../assets/appimages/LockShow.svg';
import {firebase} from '@react-native-firebase/firestore';

function PinLockScreen({navigation}) {
  const userDetails = useSelector((state) => state.userDetails ?? {});

  const dispatch = useDispatch();

  const _handleBackPress = () => {
    navigation.goBack();
  };

  const [txtRecoveryEmail, setTxtRecoveryEmail] = useState(
    userDetails?.recoverEmailId ?? '',
  );
  const [txtConfirmRecoveryEmail, setTxtConfirmRecoveryEmail] = useState(
    userDetails?.recoverEmailId ?? '',
  );

  const [txtPassword, setTxtPassword] = useState(userDetails?.password ?? '');
  const [txtConfirmPassword, setConfirmTxtPassword] = useState(
    userDetails?.password ?? '',
  );

  const [isShowAlert, setIsShowAlert] = useState(false);
  const [isShowLoading, setIsShowLoading] = useState(false);
  const [alertTitle, setAlertTitle] = useState('');
  const [alertDescription, setAlertDescription] = useState('');
  const [alertConfirmText, setAlertConfirmText] = useState('Ok');
  const [alertCancelText, setAlertCancelText] = useState('Cancel');

  const handleTxtRecoveryEmail = (text) => {
    setTxtRecoveryEmail(text);
  };

  const handleTxtConfirmRecoveryEmail = (text) => {
    setTxtConfirmRecoveryEmail(text);
  };

  const handleTxtPassword = (text) => {
    setTxtPassword(text);
  };

  const handleTxtConfirmPassword = (text) => {
    setConfirmTxtPassword(text);
  };

  const handleDeactiveLock = () => {
    setAlertTitle('PassCode Deactivate?');
    setAlertDescription('Are you sure you want to deactivate passcode?');
    setAlertConfirmText('Yes');

    setAlertCancelText('No');
    setIsShowAlert(true);
  };

  const handleAlertConfirmButtonPress = async () => {
    switch (alertTitle) {
      case 'PassCode Deactivate?':
        console.log('aaaaaaaaaa ');
        dispatch(setRecoverEmailId(''));
        dispatch(setPassword(''));
        setTxtRecoveryEmail('');
        setTxtConfirmRecoveryEmail('');
        setTxtPassword('');
        setConfirmTxtPassword('');

        Popup.show({
          type: 'Success',
          title: 'PassCode Deactivated!',
          textBody: 'Your passCode has been Deactivated!',
          buttonText: 'Ok',
          callback: () => Popup.hide(),
        });
        await firebase
          .firestore()
          .collection('Users')
          .doc(userDetails?.userId)
          .update({
            recoverEmailId: '',
            password: '',
          });

        break;

      default:
        break;
    }
  };

  const handleSubmit = async () => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
    if (reg.test(txtRecoveryEmail) === false) {
      Popup.show({
        type: 'Warning',
        title: 'SetUp Incomplete',
        textBody: 'Please add valid Email Address.',
        buttonText: 'Ok',
        callback: () => Popup.hide(),
      });
      return;
    } else if (reg.test(txtConfirmRecoveryEmail) === false) {
      Popup.show({
        type: 'Warning',
        title: 'SetUp Incomplete',
        textBody: 'Please add valid Email Address.',
        buttonText: 'Ok',
        callback: () => Popup.hide(),
      });
      return;
    } else if (txtPassword?.length < 8) {
      Popup.show({
        type: 'Warning',
        title: 'SetUp Incomplete',
        textBody: 'Please add minimun 8 later password.',
        buttonText: 'Ok',
        callback: () => Popup.hide(),
      });
      return;
    } else if (txtConfirmPassword?.length < 8) {
      Popup.show({
        type: 'Warning',
        title: 'SetUp Incomplete',
        textBody: 'Please add minimun 8 later password.',
        buttonText: 'Ok',
        callback: () => Popup.hide(),
      });
      return;
    } else if (txtRecoveryEmail !== txtConfirmRecoveryEmail) {
      Popup.show({
        type: 'Warning',
        title: 'SetUp Incomplete',
        textBody: 'Please enter the same emailid in both textfield.',
        buttonText: 'Ok',
        callback: () => Popup.hide(),
      });
      return;
    } else if (txtPassword !== txtConfirmPassword) {
      Popup.show({
        type: 'Warning',
        title: 'SetUp Incomplete',
        textBody: 'Please enter the same password in both textfield.',
        buttonText: 'Ok',
        callback: () => Popup.hide(),
      });
      return;
    }
    try {
      dispatch(setRecoverEmailId(txtRecoveryEmail));
      dispatch(setPassword(txtPassword));

      Popup.show({
        type: 'Success',
        title: 'SetUp Completed',
        textBody: 'Congrats! Your password setup successfully done.',
        buttonText: 'Ok',
        callback: () => Popup.hide(),
      });
      await firebase
        .firestore()
        .collection('Users')
        .doc(userDetails?.userId)
        .update({
          recoverEmailId: txtRecoveryEmail,
          password: txtPassword,
        });
    } catch (error) {}

    // firestore()
    // .collection("mail")
    // .add({
    //   to: "nikunjpatel0071@gmail.com",
    //   message: {
    //     subject: "Hello from Firebase!",
    //     html: "This is the section of the email body.",
    //   },
    // })
    // .then(() => console.log("Queued email for delivery!"));
  };

  return (
    <View style={{flex: 1, backgroundColor: WHITE}}>
      <Root style={{flex: 1}}>
        <SafeAreaView />
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            marginLeft: 15,
            height: 30,
          }}>
          <TouchableOpacity
            style={{
              height: '100%',
              aspectRatio: 1,
              backgroundColor: 'white',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={_handleBackPress}>
            <Image
              resizeMode={'contain'}
              style={{height: 22, width: 25}}
              source={appImages.back}
            />
          </TouchableOpacity>
          <Text adjustsFontSizeToFit style={styles.headerTitle}>
            {' '}
            Pin Lock
          </Text>
        </View>
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps="handled"
          style={{flex: 1}}>
          <View style={styles.mainView}>
            <LockShow width={WINDOW_WIDTH * 0.4} height={WINDOW_WIDTH * 0.4} />

            <TextInputWithTitle
              title={'Enter Password'}
              autoCapitalize="none"
              value={txtPassword}
              onChangeText={handleTxtPassword}
            />

            <TextInputWithTitle
              title={'Enter Confirm Password'}
              autoCapitalize="none"
              value={txtConfirmPassword}
              onChangeText={handleTxtConfirmPassword}
            />

            <TextInputWithTitle
              title={'Recovery email'}
              value={txtRecoveryEmail}
              onChangeText={handleTxtRecoveryEmail}
            />

            <TextInputWithTitle
              title={'Confirm recovery email'}
              value={txtConfirmRecoveryEmail}
              onChangeText={handleTxtConfirmRecoveryEmail}
            />

            <TouchableOpacity
              style={{
                width: WINDOW_WIDTH - 40,
                height: 45,
                backgroundColor: THEME_COLOR,
                alignSelf: 'center',
                marginTop: 50,
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 4,
              }}
              onPress={handleSubmit}>
              <Text
                style={{
                  color: WHITE,
                  fontFamily: 'Poppins-Regular',
                  fontSize: 16,
                }}>
                {userDetails?.recoverEmailId ? 'Update' : 'Save'}
              </Text>
            </TouchableOpacity>
            {userDetails?.recoverEmailId ? (
              <TouchableOpacity
                style={{
                  width: WINDOW_WIDTH - 40,
                  height: 45,
                  backgroundColor: THEME_COLOR,
                  alignSelf: 'center',
                  marginTop: 10,
                  marginBottom: 20,
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 4,
                }}
                onPress={handleDeactiveLock}>
                <Text
                  style={{
                    color: WHITE,
                    fontFamily: 'Poppins-Regular',
                    fontSize: 16,
                  }}>
                  Deactive Lock
                </Text>
              </TouchableOpacity>
            ) : null}
          </View>
        </KeyboardAwareScrollView>

        <AwesomeAlert
          show={isShowAlert}
          title={alertTitle}
          message={alertDescription}
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          showConfirmButton={true}
          cancelText={alertCancelText}
          confirmText={alertConfirmText}
          confirmButtonColor="blue"
          onCancelPressed={() => {
            setIsShowAlert(false);
          }}
          onConfirmPressed={() => {
            handleAlertConfirmButtonPress();
            setIsShowAlert(false);
          }}
        />
        <AwesomeAlert
          show={isShowLoading}
          showProgress={true}
          title="Loading..."
        />
      </Root>
    </View>
  );
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 25,
  },
  headerTitle: {
    fontSize: 18,
    flex: 1,
    color: BLACK,
    fontFamily: 'Poppins-Medium',
    textAlign: 'center',
    marginRight: 50,
  },
});

export default PinLockScreen;
