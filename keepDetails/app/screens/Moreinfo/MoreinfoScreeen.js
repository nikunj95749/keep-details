import * as React from 'react';
import {
  View,
  StyleSheet,
  Text,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  Image,
  Platform,
  Linking,
  Alert,
} from 'react-native';
import {appImages} from '../../config';
import {
  BLACK,
  LIGHT_GRAY_10,
  responsiveScale,
  scaleFont,
  WHITE,
} from '../../styles';
import MoreInfoItem from './Components/MoreInfoItem';
import Share from 'react-native-share';
import useInAppBrowser from '../../hooks/useInAppBrowser';
import {clearAsyncStorage} from '../../helper/auth';
import {useDispatch, useSelector} from 'react-redux';
import {removeAuthTokenAction} from '../../store/auth';
import qs from 'qs';

const GOOGLE_PACKAGE_NAME = 'com.keepDetails';

const APPLE_STORE_ID = '1545580135';

async function sendEmail(to, subject, body, options = {}) {
  const {cc, bcc} = options;

  let url = `mailto:${to}`;

  // Create email link query
  const query = qs.stringify({
    subject: subject,
    body: body,
    cc: cc,
    bcc: bcc,
  });

  if (query.length) {
    url += `?${query}`;
  }

  // // check if we can use this link
  // const canOpen = await Linking.canOpenURL(url);

  // if (!canOpen) {
  //   throw new Error('Provided URL can not be handled');
  // }

  return Linking.openURL(url);
}

function Moreinfo({navigation}) {
  const openURLInApp = useInAppBrowser();

  const authToken = useSelector((state) => state.auth?.authToken ?? '');

  const dispatch = useDispatch();

  const _handlePressedPinLock = () => {
    if (!authToken) {
      navigation.navigate('Signin');
      return;
    }
    navigation.navigate('PinLock');
  };

  const onPressRateUsOrFeedBack = () => {
    openURL(getAppLink());
  };

  const onPressLogout = () => {
    Alert.alert('Logout', 'Are you sure you want to logout?', [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
      },
      {
        text: 'Logout',
        style: 'destructive',
        onPress: async () => {
          try {
            clearAsyncStorage();
            dispatch(removeAuthTokenAction());
          } catch (err) {}
        },
      },
    ]);
  };

  const getAppLink = () => {
    if (Platform.OS !== 'ios') {
      //To open the Google Play Store
      return `https://play.google.com/store/apps/details?id=com.keepdetails`;
    } else {
      //To open the Apple App Store
      return `https://apps.apple.com/app/id${APPLE_STORE_ID}`;
    }
  };

  const inviteYourFriend = async () => {
    try {
      await Share.open({
        message: `Join me on keep details ${getAppLink()}`,
      });
    } catch (error) {
      console.log('Error =>', error);
    }
  };

  const aboutUsHandler = async () => {
    openURLInApp(
      'https://sites.google.com/view/keepdetails-aboutus/home',
      'aboutUsHandler',
    );
  };

  const privacyPolicyHandler = async () => {
    openURLInApp(
      'https://sites.google.com/view/keepdetails-privacypolicy/home',
      'aboutUsHandler',
    );
  };

  const onPressHelp = async () => {
    sendEmail('keepdetails0071@gmail.com', 'need help?', '').then(() => {
      Alert.alert(
        '',
        'Your message was successfully sent!',
        [
          {
            text: 'Ok',
            onPress: () => console.log('OK: Email Error Response'),
          },
        ],
        {cancelable: true},
      );
      console.log('Your message was successfully sent!');
    });
  };

  const openURL = (url = '') => {
    Linking.openURL(url);
  };

  return (
    <View style={{flex: 1, backgroundColor: WHITE}}>
      <SafeAreaView />
      <ScrollView style={{flex: 1}}>
        <View style={styles.mainView}>
          <View style={styles.titleView}>
            <Text style={styles.headerTitle}>Settings</Text>
          </View>
          <MoreInfoItem title={'Pin Locks'} onPress={_handlePressedPinLock} />

          {/* <MoreInfoItem
            title={'Theme Color'}
            titleImage={appImages.pin_Lock}
            onPress={_handlePressedPinLock}
          />

          <MoreInfoItem
            title={'Reminder'}
            titleImage={appImages.pin_Lock}
            onPress={_handlePressedPinLock}
          /> */}

          <MoreInfoItem
            title={'Invite Your Friends'}
            onPress={inviteYourFriend}
          />
          <MoreInfoItem title={'Feedback'} onPress={onPressRateUsOrFeedBack} />

          <MoreInfoItem title={'Help'} onPress={onPressHelp} />

          <MoreInfoItem title={'About'} onPress={aboutUsHandler} />
          <MoreInfoItem
            title={'Privacy Policy'}
            onPress={privacyPolicyHandler}
          />

          <MoreInfoItem title={'Rate Us'} onPress={onPressRateUsOrFeedBack} />
          {authToken ? (
            <MoreInfoItem title={'Logout'} onPress={onPressLogout} />
          ) : null}
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    alignItems: 'center',
  },
  titleView: {
    width: '100%',
    backgroundColor: WHITE,
    marginVertical: 0,
    justifyContent: 'flex-end',
    paddingLeft: 20,
  },
  headerTitle: {
    textAlign: 'left',
    color: BLACK,
    fontFamily: 'Poppins-Medium',
    fontSize: Platform.OS === 'ios' ? responsiveScale(20) : responsiveScale(16),
  },
});

export default Moreinfo;
