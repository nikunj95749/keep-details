import React from 'react';
import {StyleSheet, TouchableOpacity, View, Text} from 'react-native';
import {LIGHT_GRAY_10, scaleFont} from '../../../styles';
import Pin_Lock from '../../../assets/appimages/Pin_Lock.svg';
import InviteFriends from '../../../assets/appimages/InviteFriends.svg';
import FeedBack from '../../../assets/appimages/FeedBack.svg';
import Help from '../../../assets/appimages/Help.svg';
import About from '../../../assets/appimages/About.svg';
import PrivacyPolicy from '../../../assets/appimages/PrivacyPolicy.svg';
import RateUs from '../../../assets/appimages/RateUs.svg';
import LogOut from '../../../assets/appimages/LogOut.svg';

export default function MoreInfoItem(props) {
  return (
    <TouchableOpacity
      {...props}
      style={{
        width: '95%',
        aspectRatio: 7,
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 8,
        backgroundColor: LIGHT_GRAY_10,
        marginTop: 15,
      }}>
      <View style={{height: 25, width: 25, marginLeft: 10}}>
        {/* {props?.titleImage}
         */}
        {props?.title === 'Pin Locks' && (
          <Pin_Lock height={'100%'} width={'100%'} />
        )}
        {props?.title === 'Invite Your Friends' && (
          <InviteFriends height={'100%'} width={'100%'} />
        )}
        {props?.title === 'Feedback' && (
          <FeedBack height={'100%'} width={'100%'} />
        )}
        {props?.title === 'Help' && <Help height={'100%'} width={'100%'} />}

        {props?.title === 'About' && <About height={'100%'} width={'100%'} />}
        {props?.title === 'Privacy Policy' && (
          <PrivacyPolicy height={'100%'} width={'100%'} />
        )}
        {props?.title === 'Rate Us' && (
          <RateUs height={'100%'} width={'100%'} />
        )}
        {props?.title === 'Logout' && <LogOut height={'100%'} width={'100%'} />}
      </View>

      <View style={{justifyContent: 'center', height: '100%', width: '100%'}}>
        <Text
          style={{
            marginLeft: 10,
            fontFamily: 'Poppins-Regular',
            fontSize: 15,
          }}>
          {props?.title}
        </Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({});
