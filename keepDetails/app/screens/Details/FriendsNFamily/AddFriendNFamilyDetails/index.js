import React, {useState, useCallback, useMemo} from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  Text,
  Image,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {appImages} from '../../../../config';
import ImagePicker from 'react-native-image-crop-picker';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {formatDate} from '../../../../helper/dateAndTime';
import DatePickerModal from '../../../../components/DatePickerModal';
import {
  addDetails,
  updateDetails,
} from '../../../../firebaseServices/Details/details';
import {isEmpty} from 'lodash';
import {useSelector} from 'react-redux';
import {Root, Popup} from 'popup-ui';
import {BLACK, responsiveScale, THEME_COLOR, WHITE} from '../../../../styles';
import TextInputWithTitle from '../../../../components/TextInputWithTitle';
import CustomActionsheet from '../../../../components/CustomActionsheet';
import {
  CANCEL,
  CHOOSE_FROM_GALLERY,
  TAKE_PHOTO,
} from '../../../../config/constants';
import User from '../../../../assets/appimages/User.svg';

function AddFriendNFamilyDetails({navigation, route}) {
  const selectedItem = useMemo(
    () => route?.params?.selectedItem ?? {},
    [navigation],
  );

  const selectedDate = useMemo(() => {
    return selectedItem?.dob ? selectedItem?.dob.toDate() : '';
  }, [navigation]);

  const authToken = useSelector((state) => state.auth?.authToken ?? '');

  const [imgSubject, setImgSubject] = useState(selectedItem?.imageUrl ?? '');
  const [title, setTitle] = useState(selectedItem?.title ?? '');
  const [txtFullName, setTxtFullName] = useState(selectedItem?.full_name ?? '');
  const [dateOfBirth, setDateOfBirth] = useState(selectedDate);
  const [gender, setGender] = useState(selectedItem?.gender ?? '');
  const [txtNationality, setTxtNationality] = useState(
    selectedItem?.natinality ?? '',
  );
  const [txtOccupation, setTxtOccupation] = useState(
    selectedItem?.occupation ?? '',
  );
  const [txtNote, setTxtNote] = useState(selectedItem?.note ?? '');
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const actionSheetItemsForPostPrivacy = [
    {
      text: CHOOSE_FROM_GALLERY,
      onPress: () => {
        choosePhotoFromGallery();
      },
    },
    {
      text: TAKE_PHOTO,
      onPress: () => {
        takePhotoFromCamera();
      },
    },
    {
      text: CANCEL,
      onPress: () => setActionsheetVisible(false),
    },
  ];

  const [actionsheetVisible, setActionsheetVisible] = useState(false);

  const choosePhotoFromGallery = () => {
    try {
      setActionsheetVisible(false);
      setTimeout(() => {
        ImagePicker.openPicker({
          width: 300,
          height: 400,
          cropping: true,
          compressImageQuality: 0.8,
        })
          .then(async (response) => {
            console.log('response==== ', response?.path);
            setImgSubject(response?.path);
          })
          .catch((err) => {});
      }, 500);
    } catch (error) {}
  };

  const takePhotoFromCamera = async () => {
    setActionsheetVisible(false);

    setTimeout(() => {
      ImagePicker.openCamera({
        cancelable: true,
        compressImageQuality: 0.8,
      })
        .then(async (response) => {
          console.log('response==== ', response);
          setImgSubject(response?.path);
        })
        .catch((error) => {
          console.log('error in Camera ===>', error);
        })
        .finally(() => {});
    }, 600);
  };

  const _handleDateChange = useCallback((value) => {
    setDateOfBirth(value);
  }, []);

  const _handleSetTitle = (text) => {
    setTitle(text);
  };

  const _handleFullName = (text) => {
    setTxtFullName(text);
  };

  const _handleNationalityName = (text) => {
    setTxtNationality(text);
  };

  const _handleOccupationName = (text) => {
    setTxtOccupation(text);
  };

  const _handleNotes = (text) => {
    setTxtNote(text);
  };

  const _handleSubmitButton = async () => {
    if (!title) {
      Popup.show({
        type: 'Warning',
        title: 'Empty required field!',
        textBody: 'Please enter title.',
        buttonText: 'Ok',
        callback: () => Popup.hide(),
      });
      return;
    } else if (!txtFullName) {
      Popup.show({
        type: 'Warning',
        title: 'Empty required field!',
        textBody: 'Please enter full name.',
        buttonText: 'Ok',
        callback: () => Popup.hide(),
      });
      return;
    } else if (!gender) {
      Popup.show({
        type: 'Warning',
        title: 'Empty required field!',
        textBody: 'Please enter gender.',
        buttonText: 'Ok',
        callback: () => Popup.hide(),
      });
      return;
    }
    setTimeout(() => {
      navigation.goBack();
    }, 400);
    const data = {
      title: title,
      full_name: txtFullName,
      dob: dateOfBirth,
      gender: gender,
      natinality: txtNationality,
      occupation: txtOccupation,
      note: txtNote,
    };
    setIsLoading(true);
    if (isEmpty(selectedItem)) {
      data['createdAt'] = new Date();
      await addDetails('friendsNfamily', data, authToken, imgSubject);
      setIsLoading(false);
      return;
    }

    if (selectedItem?.imageUrl !== imgSubject) {
      await updateDetails(
        'friendsNfamily',
        {...data, id: selectedItem?.id},
        authToken,
        imgSubject,
      );
      setIsLoading(false);
      return;
    }

    await updateDetails(
      'friendsNfamily',
      {...data, id: selectedItem?.id},
      authToken,
    );
    setIsLoading(false);
  };

  return (
    <Root>
      <View style={styles.mainView}>
        <SafeAreaView />
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            marginLeft: 15,
            height: 30,
          }}>
          <TouchableOpacity
            style={{
              height: '100%',
              aspectRatio: 1,
              backgroundColor: 'white',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() => {
              navigation.goBack();
            }}>
            <Image
              resizeMode={'contain'}
              style={{height: 22, width: 25}}
              source={appImages.back}
            />
          </TouchableOpacity>
          <Text
            adjustsFontSizeToFit
            numberOfLines={1}
            style={styles.headerTitle}>
            {' '}
            Add Friends & Family{' '}
          </Text>
        </View>

        <KeyboardAwareScrollView
          keyboardShouldPersistTaps="handled"
          extraScrollHeight={10}>
          <View style={{width: '100%', flex: 1}}>
            <TouchableOpacity
              style={{
                marginTop: 20,
                width: '35%',
                aspectRatio: 1,
                alignSelf: 'center',
                borderRadius: 10,
                overflow: 'hidden',
              }}
              onPress={() => {
                setActionsheetVisible(true);
              }}>
              {!imgSubject ? (
                <User />
              ) : (
                <Image
                  style={{height: '100%', width: '100%', resizeMode: 'cover'}}
                  source={{
                    uri: imgSubject,
                  }}
                />
              )}
            </TouchableOpacity>

            <TextInputWithTitle
              title={'Title'}
              isRequired={true}
              value={title}
              onChangeText={_handleSetTitle}
            />

            <TextInputWithTitle
              title={'Full Name'}
              isRequired={true}
              value={txtFullName}
              onChangeText={_handleFullName}
            />

            <View
              style={{
                width: '90%',
                alignSelf: 'center',
                height: 70,
                marginTop: 15,
              }}>
              <View
                style={{
                  width: '100%',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    marginLeft: 10,
                    fontFamily: 'Poppins-Regular',
                    fontSize: 15,
                  }}>
                  DOB (dd/mm/yyyy)
                </Text>
              </View>
              <TouchableOpacity
                style={{
                  borderColor: THEME_COLOR,
                  borderWidth: 1.5,
                  borderRadius: 25,
                  backgroundColor: WHITE,
                  justifyContent: 'center',
                  paddingHorizontal: 10,
                  height: 50,
                }}
                onPress={() => {
                  setIsModalVisible(true);
                }}>
                <Text style={{marginLeft: 5, fontWeight: '400'}}>
                  {dateOfBirth ? formatDate(dateOfBirth, 'MMMM do yyyy') : ''}
                </Text>
              </TouchableOpacity>
            </View>

            <TextInputWithTitle
              title={'Gender'}
              isRequired={true}
              value={gender}
              onChangeText={(txt) => {
                setGender(txt);
              }}
            />

            <TextInputWithTitle
              title={'Nationality'}
              value={txtNationality}
              onChangeText={_handleNationalityName}
            />

            <TextInputWithTitle
              title={'Occupation'}
              value={txtOccupation}
              onChangeText={_handleOccupationName}
            />

            <TextInputWithTitle
              title={'Note'}
              underlineColorAndroid="transparent"
              autoCapitalize="none"
              multiline={true}
              value={txtNote}
              onChangeText={_handleNotes}
              aspectRatio={120}
            />
            <TouchableOpacity
              disabled={isLoading}
              style={{
                backgroundColor: THEME_COLOR,
                alignSelf: 'center',
                marginVertical: 20,
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 4,
                height: 50,
                width: isEmpty(selectedItem) ? 100 : 120,
              }}
              onPress={_handleSubmitButton}>
              {isLoading ? (
                <ActivityIndicator color={'white'} />
              ) : (
                <Text
                  style={{
                    color: WHITE,
                    fontFamily: 'Poppins-Regular',
                    fontSize: 16,
                  }}>
                  {isEmpty(selectedItem) ? 'Save' : 'Update'}
                </Text>
              )}
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>

        <DatePickerModal
          dateOfBirth={dateOfBirth}
          visible={isModalVisible}
          onModalPress={() => {
            setIsModalVisible(false);
          }}
          onChange={_handleDateChange}
        />
      </View>
      <CustomActionsheet
        isVisible={actionsheetVisible}
        actionItems={actionSheetItemsForPostPrivacy}
        OnBackdropPress={() => {
          setActionsheetVisible(false);
        }}
      />
    </Root>
  );
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: 'white',
  },
  headerTitle: {
    flex: 1,
    color: BLACK,
    fontFamily: 'Poppins-Medium',
    fontSize: responsiveScale(10),
    textAlign: 'center',
    marginRight: 50,
  },
});

export default AddFriendNFamilyDetails;
