import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Text,
  Alert,
  Image,
} from 'react-native';
import {useSelector} from 'react-redux';
import {appImages} from '../../../config';
import {useNavigation} from '@react-navigation/core';
import {
  BLACK,
  responsiveScale,
  scaleFont,
  scaleSize,
  WHITE,
} from '../../../styles';
import EmptyView from '../../../components/EmptyView';
import moment from 'moment';
import {getValueFromLocal, setValueInLocal} from '../../../helper/auth';
import {SwipeListView} from 'react-native-swipe-list-view';
import {deleteDetails} from '../../../firebaseServices/Details/details';
import User from '../../../assets/appimages/User.svg';
import Delete from '../../../assets/appimages/Delete.svg';
import {AdEventType, InterstitialAd} from '@react-native-firebase/admob';
import {interstitialAdUnitId} from '../../../config/constants';
import {Searchbar} from 'react-native-paper';

const interstitial = InterstitialAd.createForAdRequest(interstitialAdUnitId, {
  requestNonPersonalizedAdsOnly: true,
});

function SearchFriendDetailsScreen({route}) {
  const [arrsDetails, setArrDetails] = useState(route?.params?.defaultDetails || []);

  const authToken = useSelector((state) => state.auth?.authToken ?? '');
  const navigation = useNavigation();

  useEffect(() => {
    const eventListener = interstitial.onAdEvent((type) => {
      if (type === AdEventType.LOADED) {
        setTimeout(() => {
          interstitial.show();
        }, 2000);
      }
    });

    // Unsubscribe from events on unmount
    return () => {
      eventListener();
    };
  }, []);

  const initializeAdd = async () => {
    const notes = await getValueFromLocal('notes');
    if (notes === null) {
      setValueInLocal('SearchFriendDetailsScreen', JSON.stringify(0));
    } else if (JSON.parse(notes) >= 3) {
      interstitial.load();
      setValueInLocal('SearchFriendDetailsScreen', JSON.stringify(0));
    } else {
      setValueInLocal(
        'SearchFriendDetailsScreen',
        JSON.stringify(JSON.parse(notes) + 1),
      );
    }
  };


  const _handleFriendsNFamilyItemTap = (item) => {
    initializeAdd();
    navigation.navigate('AddFriendNFamilyDetails', {
      selectedItem: item,
    });
  };

  const FriendsNFamilyListItem = ({item, index, navigation}) => {
    var timestemp = new Date(item?.createdAt?.toDate());

    return (
      <View>
        <TouchableOpacity
          activeOpacity={0.85}
          style={styles.FriendsNFamilyListItemMainView}
          onPress={() => _handleFriendsNFamilyItemTap(item)}>
          <View
            style={{
              marginLeft: 10,
              height: 75,
              aspectRatio: 1,
              marginTop: 10,
              borderRadius: 10,
              overflow: 'hidden',
            }}>
            {!item?.imageUrl ? (
              <User />
            ) : (
              <Image
                style={{height: '100%', width: '100%', resizeMode: 'cover'}}
                source={{
                  uri: item?.imageUrl,
                }}
              />
            )}
          </View>
          <View
            style={{
              marginLeft: 10,
              flex: 1,
              marginRight: 10,
              marginVertical: 10,
              alignSelf: 'center',
            }}>
            <Text
              style={{fontFamily: 'Poppins-Medium', fontSize: scaleFont(16)}}>
              {item.title} {'\n'}
              {item.full_name}
            </Text>
            <Text
              style={{
                marginRight: 10,
                fontFamily: 'Poppins-Medium',
                fontSize: scaleFont(16),
              }}>
              Gender: {item.gender}
            </Text>
            <Text
              style={{
                fontFamily: 'Poppins-Medium',
              }}>
              {`${moment(timestemp)?.format('ddd, MMMM Do YYYY hh:mm:ss a')}`}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  const FriendsNFamilyListHiddenItem = ({item}) => {
    return (
      <View>
        <TouchableOpacity
          activeOpacity={0.85}
          style={styles.FriendsNFamilyListItemMainView}>
          <View
            style={{
              width: '100%',
              height: '100%',
              borderRadius: 10,
              overflow: 'hidden',
              alignItems: 'flex-end',
            }}>
            <TouchableOpacity
              onPress={() => {
                Alert.alert('', 'Do you really want to delete this item?', [
                  {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                  },
                  {
                    text: 'OK',
                    onPress: () =>
                      deleteDetails('friendsNfamily', item?.item, authToken),
                  },
                ]);
              }}
              style={{
                height: 110,
                width: 90,
                backgroundColor: 'red',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <View style={{height: 50, width: 50}}>
                <Delete />
              </View>
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={styles.mainView}>
      <SafeAreaView />
      <View style={styles.titleView}>
        <TouchableOpacity
          onPress={navigation.goBack}
          style={{
            height: 35,
            width: 35,
            marginLeft: 15,
            marginRight: 10,
            justifyContent: 'center',
          }}>
          <Image
            resizeMode={'contain'}
            style={{height: 25, width: 30, resizeMode: 'contain'}}
            source={appImages.back}
          />
        </TouchableOpacity>
        <View
          style={{
            height: '100%',
            flex: 1,
            paddingRight: 10,
            justifyContent: 'center',
          }}>
          <Searchbar
            autoCapitalize="none"
            placeholder="Search Friends & Family"
            onChangeText={(value) => {
              if (value === '') {
                setArrDetails(route?.params?.defaultDetails);
              } else {
                const arrFilteredData = route?.params?.defaultDetails?.filter(
                  (item) =>
                    item.title?.toLowerCase()?.includes(value.toLowerCase()) ||
                    item.full_name
                      ?.toLowerCase()
                      ?.includes(value.toLowerCase()) ||
                    item.note?.toLowerCase()?.includes(value.toLowerCase()) ||
                    item.gender?.toLowerCase()?.includes(value.toLowerCase()),
                );
                setArrDetails(arrFilteredData);
              }
            }}
          />
        </View>
      </View>
      <View style={{width: '100%', flex: 1}}>
        {arrsDetails?.length > 0 ? (
          <View style={styles.flatListMainView}>
            <SwipeListView
              data={arrsDetails}
              renderItem={({item, index}) => (
                <FriendsNFamilyListItem
                  item={item}
                  index={index}
                  navigation={navigation}
                />
              )}
              renderHiddenItem={(data, rowMap) => (
                <FriendsNFamilyListHiddenItem
                  item={data}
                  navigation={navigation}
                />
              )}
              rightOpenValue={-75}
              keyExtractor={(item) => item.id}
            />
          </View>
        ) : (
          <>
            <EmptyView
              title={'Empty Lists!'}
              titleImage={appImages.emptyNotes}
              titleStyle={{marginTop: scaleSize(-55)}}
            />
          </>
        )}
      </View>

      <SafeAreaView />
    </View>
  );
}

const styles = StyleSheet.create({
  mainView: {
    width: '100%',
    flex: 1,
    backgroundColor: WHITE,
  },
  flatListMainView: {
    marginTop: 10,
    flex: 1,
  },
  creditOrDebitListItemMainView: {
    width: '100%',
    marginTop: 10,
  },
  adMobView: {
    width: '100%',
    overflow: 'visible',
    marginVertical: 10,
    borderRadius: 10,
  },
  titleView: {
    width: '100%',
    height: 60,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerTitle: {
    textAlign: 'left',
    color: BLACK,
    fontFamily: 'Poppins-Medium',
    fontSize: 25,
  },
  FriendsNFamilyListItemMainView: {
    width: '92%',
    alignSelf: 'center',
    flexDirection: 'row',
    backgroundColor: WHITE,
    shadowColor: '#000',
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.25,
    shadowRadius: 2,
    elevation: 5,
    borderRadius: 10,
    marginTop: 15,
    marginBottom: 10,
  },
});
export default SearchFriendDetailsScreen;
