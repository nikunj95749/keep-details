import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Image,
  Text,
  Alert,
  ActivityIndicator,
} from 'react-native';
import {appImages} from '../../../config/images';
import {useSelector} from 'react-redux';
import AddButton from '../../../components/AddButton';
import {
  BLACK,
  responsiveScale,
  scaleFont,
  scaleSize,
  WHITE,
} from '../../../styles';
import moment from 'moment';
import {SwipeListView} from 'react-native-swipe-list-view';
import {deleteDetails} from '../../../firebaseServices/Details/details';
import User from '../../../assets/appimages/User.svg';
import Delete from '../../../assets/appimages/Delete.svg';
import Search from '../../../assets/appimages/Search.svg';
import {firebase} from '@react-native-firebase/firestore';
import EmptyView from '../../../components/EmptyView';
import { ToNavigationBar } from '../../../components/ToNavigationBar';

function FriendsNFamily({navigation}) {
  const [arrsDetails, setArrDetails] = useState([]);
  const authToken = useSelector((state) => state.auth?.authToken ?? '');
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const getFrndsNFamilyData = async () => {
      try {
        console.log('1234');
        firebase
          .firestore()
          .collection('Users')
          .doc(authToken)
          .collection('friendsNfamily')
          .onSnapshot((snapshot) => {
            const data = snapshot.docs.map((doc) => ({
              id: doc.id,
              ...doc.data(),
            }));
            setArrDetails(data);
            setIsLoading(false);
            console.log('5678');
          });
      } catch (error) {}
    };
    getFrndsNFamilyData();
  }, []);

  const _handleFriendsNFamilyItemTap = (item) => {
    console.log(item);
    navigation.navigate('AddFriendNFamilyDetails', {
      selectedItem: item,
    });
  };

  const FriendsNFamilyListItem = ({item, index, navigation}) => {
    var timestemp = new Date(item?.createdAt?.toDate());

    return (
      <View>
        <TouchableOpacity
          activeOpacity={0.85}
          style={styles.FriendsNFamilyListItemMainView}
          onPress={() => _handleFriendsNFamilyItemTap(item)}>
          <View
            style={{
              marginLeft: 10,
              height: 75,
              aspectRatio: 1,
              marginTop: 10,
              borderRadius: 10,
              overflow: 'hidden',
            }}>
            {!item?.imageUrl ? (
              <User />
            ) : (
              <Image
                style={{height: '100%', width: '100%', resizeMode: 'cover'}}
                source={{
                  uri: item?.imageUrl,
                }}
              />
            )}
          </View>
          <View
            style={{
              marginLeft: 10,
              flex: 1,
              marginRight: 10,
              marginVertical: 10,
              alignSelf: 'center',
            }}>
            <Text
              style={{fontFamily: 'Poppins-Medium', fontSize: scaleFont(16)}}>
              {item.title} {'\n'}
              {item.full_name}
            </Text>
            <Text
              style={{
                marginRight: 10,
                fontFamily: 'Poppins-Medium',
                fontSize: scaleFont(16),
              }}>
              Gender: {item.gender}
            </Text>
            <Text
              style={{
                fontFamily: 'Poppins-Medium',
              }}>
              {`${moment(timestemp)?.format('ddd, MMMM Do YYYY hh:mm:ss a')}`}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  const FriendsNFamilyListHiddenItem = ({item, index, navigation}) => {
    return (
      <View>
        <TouchableOpacity
          activeOpacity={0.85}
          style={styles.FriendsNFamilyListItemMainView}>
          <View
            style={{
              width: '100%',
              height: '100%',
              borderRadius: 10,
              overflow: 'hidden',
              alignItems: 'flex-end',
            }}>
            <TouchableOpacity
              onPress={() => {
                Alert.alert('', 'Do you really want to delete this item?', [
                  {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                  },
                  {
                    text: 'OK',
                    onPress: () =>
                      deleteDetails('friendsNfamily', item?.item, authToken),
                  },
                ]);
              }}
              style={{
                height: 110,
                width: 90,
                backgroundColor: 'red',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <View style={{height: 50, width: 50}}>
                <Delete />
              </View>
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={styles.mainView}>
      <SafeAreaView />
      <ToNavigationBar
        onPressBack={() => {
          navigation.goBack();
        }}
        title={'Friends & Family'}
        onPressSearch={() => {
          navigation.navigate('SearchFriendDetailsScreen', {
            defaultDetails: arrsDetails,
          });
        }}
      />
      <View style={styles.flatListMainView}>
        {arrsDetails?.length > 0 ? (
          <SwipeListView
            data={arrsDetails}
            renderItem={({item, index}) => (
              <FriendsNFamilyListItem
                item={item}
                index={index}
                navigation={navigation}
              />
            )}
            renderHiddenItem={(data, rowMap) => (
              <FriendsNFamilyListHiddenItem
                item={data}
                navigation={navigation}
              />
            )}
            rightOpenValue={-75}
            keyExtractor={(item) => item.id}
          />
        ) : (
          <>
            {!isLoading ? (
              <EmptyView
                title={'Empty Notes!'}
                titleImage={appImages.emptyNotes}
                titleStyle={{marginTop: scaleSize(-55)}}
              />
            ) : (
              <ActivityIndicator size="large" color={'black'} />
            )}
          </>
        )}
        {/* <FlatList
          ListHeaderComponent={() => (
            <View style={styles.titleView}>
              <Text style={styles.headerTitle}>Friends & Family</Text>
            </View>
          )}
          showsVerticalScrollIndicator={false}
          data={arrsDetails}
          renderItem={({item, index}) => (
            <FriendsNFamilyListItem
              item={item}
              index={index}
              navigation={navigation}
            />
          )}
          keyExtractor={(item) => item.name}
        /> */}
      </View>
      <AddButton
        onPress={() => {
          navigation.navigate('AddFriendNFamilyDetails');
        }}
      />
      <SafeAreaView />
    </View>
  );
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: WHITE,
  },
  flatListMainView: {
    marginTop: 10,
    flex: 1,
  },
  FriendsNFamilyListItemMainView: {
    width: '92%',
    alignSelf: 'center',
    flexDirection: 'row',
    backgroundColor: WHITE,
    shadowColor: '#000',
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.25,
    shadowRadius: 2,
    elevation: 5,
    borderRadius: 10,
    marginTop: 15,
    marginBottom: 10,
  },
  titleView: {
    width: '100%',
    backgroundColor: WHITE,
    marginVertical: 10,
    justifyContent: 'flex-end',
    paddingLeft: 20,
  },
  headerTitle: {
    flex: 1,
    color: BLACK,
    fontFamily: 'Poppins-Medium',
    fontSize: responsiveScale(10),
    textAlign: 'center',
    marginRight: 50,
  },
});

export default FriendsNFamily;
