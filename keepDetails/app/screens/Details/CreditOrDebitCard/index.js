import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Image,
  Text,
  Alert,
  ActivityIndicator,
} from 'react-native';
import {appImages} from '../../../config/images';
import {useSelector} from 'react-redux';
import {BLACK, responsiveScale, scaleFont, scaleSize, WHITE} from '../../../styles';
import AddButton from '../../../components/AddButton';
import moment from 'moment';
import {SwipeListView} from 'react-native-swipe-list-view';
import {deleteDetails} from '../../../firebaseServices/Details/details';
import Delete from '../../../assets/appimages/Delete.svg';
import Search from '../../../assets/appimages/Search.svg';
import {firebase} from '@react-native-firebase/firestore';
import EmptyView from '../../../components/EmptyView';
import { ToNavigationBar } from '../../../components/ToNavigationBar';

function CreditOrDebitCard({navigation}) {
  const [arrsDetails, setarrsDetails] = useState([]);
  const authToken = useSelector((state) => state.auth?.authToken ?? '');
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const getFrndsNFamilyData = async () => {
      firebase
        .firestore()
        .collection('Users')
        .doc(authToken)
        .collection('creditOrDebitCard')
        .onSnapshot((snapshot) => {
          const data = snapshot.docs.map((doc) => ({
            id: doc.id,
            ...doc.data(),
          }));
          setarrsDetails(data);
          setIsLoading(false);
        });
    };
    getFrndsNFamilyData();
  }, []);

  const _handleCreditCardItemTap = (item) => {
    navigation.navigate('AddCreditOrDebitCard', {
      selectedItem: item,
    });
  };

  const CreditOrDebitListItem = ({item, index, navigation}) => {
    var mainStr = item?.card_number;
    let vis = mainStr?.slice(-4);
    let countNum = '';

    for (var i = mainStr.length - 4; i > 0; i--) {
      countNum += '*';
    }

    var timestemp = new Date(item?.createdAt?.toDate());

    return (
      <View>
        <TouchableOpacity
          activeOpacity={0.85}
          style={styles.creditOrDebitListItemMainView}
          onPress={() => _handleCreditCardItemTap(item)}>
          <View
            style={{
              marginLeft: 10,
              flex: 1,
              marginVertical: 10,
              marginRight: 10,
              alignSelf: 'center',
            }}>
            <Text
              style={{
                marginRight: 10,
                fontFamily: 'Poppins-Medium',
                fontSize: scaleFont(16),
              }}>
              {' '}
              Bank: {item.bank_name}
            </Text>
            <Text
              style={{
                marginRight: 10,
                fontFamily: 'Poppins-Medium',
                fontSize: scaleFont(16),
              }}>
              {' '}
              Name: {item.card_holder_name}
            </Text>
            <Text
              style={{
                marginRight: 10,
                fontFamily: 'Poppins-Medium',
                fontSize: scaleFont(16),
              }}>
              {' '}
              No.: {countNum + vis}
            </Text>
            <Text
              style={{
                fontFamily: 'Poppins-Medium',
              }}>
              {' '}
              {`${moment(timestemp)?.format('ddd, MMMM Do YYYY hh:mm:ss a')}`}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  const CreditOrDebitListHiddenItem = ({item, index, navigation}) => {
    return (
      <View>
        <TouchableOpacity
          activeOpacity={0.85}
          style={styles.creditOrDebitListItemMainView}
          ListHeaderComponent={() => (
            <View style={styles.titleView}>
              <Text style={styles.headerTitle}>Cards</Text>
            </View>
          )}>
          <View
            style={{
              width: '100%',
              flex: 1,
              borderRadius: 10,
              overflow: 'hidden',
              alignItems: 'flex-end',
            }}>
            <TouchableOpacity
              onPress={() => {
                Alert.alert('', 'Do you really want to delete this item?', [
                  {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                  },
                  {
                    text: 'OK',
                    onPress: () =>
                      deleteDetails('creditOrDebitCard', item?.item, authToken),
                  },
                ]);
              }}
              style={{
                height: 120,
                width: 90,
                backgroundColor: 'red',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <View style={{height: 50, width: 50}}>
                <Delete />
              </View>
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={styles.mainView}>
      <SafeAreaView />
      <ToNavigationBar
        onPressBack={() => {
          navigation.goBack();
        }}
        title={'Cards'}
        onPressSearch={() => {
          navigation.navigate('SearchCreditOrDebitCardScreen', {
            defaultDetails: arrsDetails,
          });
        }}
      />
      <View style={styles.flatListMainView}>
        {arrsDetails?.length > 0 ? (
          <SwipeListView
            data={arrsDetails}
            renderItem={({item, index}) => (
              <CreditOrDebitListItem
                item={item}
                index={index}
                navigation={navigation}
              />
            )}
            renderHiddenItem={(data, rowMap) => (
              <CreditOrDebitListHiddenItem
                item={data}
                navigation={navigation}
              />
            )}
            rightOpenValue={-75}
            keyExtractor={(item) => item.id}
          />
        ) : (
          <>
            {!isLoading ? (
              <EmptyView
                title={'Empty Notes!'}
                titleImage={appImages.emptyNotes}
                titleStyle={{marginTop: scaleSize(-55)}}
              />
            ) : (
              <ActivityIndicator size="large" color={'black'} />
            )}
          </>
        )}
      </View>
      <AddButton
        onPress={() => {
          navigation.navigate('AddCreditOrDebitCard');
        }}
      />
      <SafeAreaView />
    </View>
  );
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: WHITE,
  },
  flatListMainView: {
    marginTop: 10,
    flex: 1,
  },
  creditOrDebitListItemMainView: {
    width: '92%',
    alignSelf: 'center',
    flexDirection: 'row',
    backgroundColor: WHITE,
    shadowColor: '#000',
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.25,
    shadowRadius: 2,
    elevation: 5,
    borderRadius: 10,
    marginTop: 15,
    marginBottom: 10,
  },
  titleView: {
    backgroundColor: WHITE,
    justifyContent: 'flex-end',
    paddingLeft: 20,
  },
  headerTitle: {
    flex: 1,
    color: BLACK,
    fontFamily: 'Poppins-Medium',
    fontSize: responsiveScale(10),
    textAlign: 'center',
    marginRight: 50,
  },
});

export default CreditOrDebitCard;
