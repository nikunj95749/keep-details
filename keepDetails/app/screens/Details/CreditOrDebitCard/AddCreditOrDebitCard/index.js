import React, {useState, useMemo} from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  Text,
  Image,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {appImages} from '../../../../config';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  addDetails,
  updateDetails,
} from '../../../../firebaseServices/Details/details';
import {isEmpty} from 'lodash';
import {useSelector} from 'react-redux';
import {Root, Popup} from 'popup-ui';
import {BLACK, THEME_COLOR, WHITE} from '../../../../styles';
import TextInputWithTitle from '../../../../components/TextInputWithTitle';

function AddCreditOrDebitCard({navigation, route}) {
  const selectedItem = useMemo(
    () => route?.params?.selectedItem ?? {},
    [navigation],
  );

  const authToken = useSelector((state) => state.auth?.authToken ?? '');

  const [txtType, setTxtType] = useState(selectedItem?.card_type ?? '');
  const [txtBankName, setTxtBankName] = useState(selectedItem?.bank_name ?? '');
  const [txtCardHolderName, setTxtCardHolderName] = useState(
    selectedItem?.card_holder_name ?? '',
  );
  const [txtCardNumber, setTxtCardNumber] = useState(
    selectedItem?.card_number ?? '',
  );
  const [txtExpDate, setTxtExpDate] = useState(selectedItem?.expDate ?? '');
  const [txtCvv, setTxtCvv] = useState(selectedItem?.card_cvv ?? '');
  const [txtNote, setTxtNote] = useState(selectedItem?.note ?? '');
  const [lastCardNumberLength, setLastCardNumberLength] = useState(0);
  const [lastExpDate, setLastExpDate] = useState(0);
  const [isLoading, setIsLoading] = useState(false);

  const _handleSubmitButton = async () => {
    if (!txtType) {
      Popup.show({
        type: 'Warning',
        title: 'Empty required field!',
        textBody: 'Please enter card Type.',
        buttonText: 'Ok',
        callback: () => Popup.hide(),
      });
      return;
    } else if (!txtBankName) {
      Popup.show({
        type: 'Warning',
        title: 'Empty required field!',
        textBody: 'Please enter Bank name.',
        buttonText: 'Ok',
        callback: () => Popup.hide(),
      });
      return;
    }

    const data = {
      card_type: txtType,
      bank_name: txtBankName,
      card_holder_name: txtCardHolderName,
      card_number: txtCardNumber,
      expDate: txtExpDate,
      card_cvv: txtCvv,
      note: txtNote,
    };
    setIsLoading(true);
    if (isEmpty(selectedItem)) {
      data['createdAt'] = new Date();
      await addDetails('creditOrDebitCard', data, authToken);
      setIsLoading(false);
      setTimeout(() => {
        navigation.goBack();
      }, 400);
      return;
    }

    await updateDetails(
      'creditOrDebitCard',
      {...data, id: selectedItem?.id},
      authToken,
    );
    setIsLoading(false);
    setTimeout(() => {
      navigation.goBack();
    }, 400);
  };

  const handleNotes = (text) => {
    setTxtNote(text);
  };

  const handleType = (text) => {
    setTxtType(text);
  };

  const handleCvv = (text) => {
    setTxtCvv(text);
  };

  const handleExpDate = (text) => {
    const arrText = Object.assign([], text);
    var finaltext = '';

    if (arrText.length <= 5) {
      if (arrText.length === 2) {
        finaltext = arrText[0] + arrText[1] + '/';
        if (lastExpDate === 3) {
          finaltext = arrText[0] + arrText[1];
        }
        setTxtExpDate(finaltext);
      } else {
        setTxtExpDate(text);
      }
    }
    setLastExpDate(arrText.length);
  };

  const handleBankName = (text) => {
    setTxtBankName(text);
  };

  const handleCardHolderName = (text) => {
    setTxtCardHolderName(text);
  };

  const handleCardNumber = (text) => {
    const arrText = Object.assign([], text);
    var finaltext = '';

    if (arrText.length <= 19) {
      if (arrText.length === 4) {
        finaltext = arrText[0] + arrText[1] + arrText[2] + arrText[3] + ' ';
        if (lastCardNumberLength === 5) {
          finaltext = arrText[0] + arrText[1] + arrText[2] + arrText[3];
        }
        setTxtCardNumber(finaltext);
      } else if (arrText.length === 9) {
        finaltext =
          arrText[0] +
          arrText[1] +
          arrText[2] +
          arrText[3] +
          arrText[4] +
          arrText[5] +
          arrText[6] +
          arrText[7] +
          arrText[8] +
          ' ';
        if (lastCardNumberLength === 10) {
          finaltext =
            arrText[0] +
            arrText[1] +
            arrText[2] +
            arrText[3] +
            arrText[4] +
            arrText[5] +
            arrText[6] +
            arrText[7] +
            arrText[8];
        }
        setTxtCardNumber(finaltext);
      } else if (arrText.length === 14) {
        finaltext =
          arrText[0] +
          arrText[1] +
          arrText[2] +
          arrText[3] +
          arrText[4] +
          arrText[5] +
          arrText[6] +
          arrText[7] +
          arrText[8] +
          arrText[9] +
          arrText[10] +
          arrText[11] +
          arrText[12] +
          arrText[13] +
          ' ';
        if (lastCardNumberLength === 15) {
          finaltext =
            arrText[0] +
            arrText[1] +
            arrText[2] +
            arrText[3] +
            arrText[4] +
            arrText[5] +
            arrText[6] +
            arrText[7] +
            arrText[8] +
            arrText[9] +
            arrText[10] +
            arrText[11] +
            arrText[12] +
            arrText[13];
        }
        setTxtCardNumber(finaltext);
      } else {
        setTxtCardNumber(text);
      }
    }
    setLastCardNumberLength(arrText.length);
  };

  return (
    <Root>
      <View style={styles.mainView}>
        <SafeAreaView />

        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            marginLeft: 15,
            height: 30,
          }}>
          <TouchableOpacity
            style={{
              height: '100%',
              aspectRatio: 1,
              backgroundColor: 'white',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() => {
              navigation.goBack();
            }}>
            <Image
              resizeMode={'contain'}
              style={{height: 22, width: 25}}
              source={appImages.back}
            />
          </TouchableOpacity>
          <Text style={styles.headerTitle}>Add Credit & Debit Details</Text>
        </View>
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps="handled"
          extraScrollHeight={10}>
          <View style={{width: '100%', flex: 1}}>
            <View style={styles.creditCardMainView}>
              <View style={styles.creditCardView}>
                <Image source={appImages.card} style={styles.creditCardImage} />
                <Text style={styles.creditCardNumberText}>
                  {txtCardNumber === '' ? '---- ---- ---- ----' : txtCardNumber}
                </Text>
                <View style={styles.creditCardBottomSubView}>
                  <View style={styles.creditCardNameView}>
                    <Text
                      numberOfLines={2}
                      adjustsFontSizeToFit
                      style={styles.creditCardNameText}>
                      {txtCardHolderName === ''
                        ? '--------'
                        : txtCardHolderName}
                    </Text>
                  </View>
                  <View style={styles.creditCardExDateNCvcView}>
                    <View style={styles.creditCardExDateView}>
                      <Text
                        numberOfLines={1}
                        adjustsFontSizeToFit
                        style={styles.creditCardExDateText}>
                        EX Date : {txtExpDate === '' ? '--/--' : txtExpDate}
                      </Text>
                    </View>
                    <View style={styles.creditCardCvcView}>
                      <Text
                        numberOfLines={1}
                        adjustsFontSizeToFit
                        style={styles.creditCardCvcText}>
                        CVC : {txtCvv === '' ? '***' : txtCvv}{' '}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>

            <TextInputWithTitle
              title={'Type'}
              isRequired={true}
              value={txtType}
              onChangeText={handleType}
            />

            <TextInputWithTitle
              title={'Bank name'}
              isRequired={true}
              value={txtBankName}
              onChangeText={handleBankName}
            />

            <TextInputWithTitle
              title={'Card holder name'}
              value={txtCardHolderName}
              onChangeText={handleCardHolderName}
            />

            <TextInputWithTitle
              title={'Card number'}
              value={txtCardNumber}
              onChangeText={handleCardNumber}
            />

            <TextInputWithTitle
              title={'Exp Date'}
              value={txtExpDate}
              onChangeText={handleExpDate}
            />

            <TextInputWithTitle
              title={'CVC'}
              value={txtCvv}
              onChangeText={handleCvv}
            />

            <TextInputWithTitle
              title={'Note'}
              underlineColorAndroid="transparent"
              autoCapitalize="none"
              multiline={true}
              value={txtNote}
              onChangeText={handleNotes}
              aspectRatio={120}
            />

            <TouchableOpacity
              disabled={isLoading}
              style={{
                backgroundColor: THEME_COLOR,
                alignSelf: 'center',
                marginVertical: 20,
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 4,
                width: isEmpty(selectedItem) ? 100 : 120,
                height: 50,
              }}
              onPress={_handleSubmitButton}>
              {isLoading ? (
                <ActivityIndicator color={'white'} />
              ) : (
                <Text
                  style={{
                    color: WHITE,
                    fontFamily: 'Poppins-Regular',
                    fontSize: 16,
                  }}>
                  {isEmpty(selectedItem) ? 'Save' : 'Update'}
                </Text>
              )}
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
      </View>
    </Root>
  );
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: 'white',
  },
  creditCardMainView: {
    justifyContent: 'center',
    aspectRatio: 1.55,
  },
  creditCardView: {
    alignItems: 'center',
    aspectRatio: 1.75,
  },
  creditCardImage: {
    height: '100%',
    width: '100%',
    resizeMode: 'contain',
    position: 'absolute',
  },
  creditCardNumberText: {
    color: 'white',
    marginTop: '30%',
    textAlign: 'center',
    height: '10%',
    width: '80%',
  },
  creditCardBottomSubView: {
    height: '25%',
    marginLeft: '5%',
    width: '74%',
    flexDirection: 'row',
  },
  creditCardNameView: {
    alignItems: 'center',
    height: '100%',
    width: '62%',
    flexDirection: 'row',
  },
  creditCardNameText: {
    flex: 1,
    color: 'white',
    fontWeight: '700',
  },
  creditCardExDateNCvcView: {
    flexDirection: 'column',
    height: '100%',
    width: '38%',
  },
  creditCardExDateView: {
    alignItems: 'center',
    height: '50%',
    width: '100%',
    flexDirection: 'row',
  },
  creditCardExDateText: {
    fontSize: 10,
    flex: 1,
    color: 'white',
    fontWeight: '700',
  },
  creditCardCvcView: {
    alignItems: 'center',
    height: '50%',
    width: '100%',
    flexDirection: 'row',
  },
  creditCardCvcText: {
    fontSize: 10,
    flex: 1,
    color: 'white',
    fontWeight: '700',
  },
  creditCardNPaypalBtnView: {
    marginHorizontal: 25,
    aspectRatio: 8,
    flexDirection: 'row',
  },
  creditCardBtnMainView: {
    justifyContent: 'center',
    alignSelf: 'flex-start',
    height: '100%',
    width: '45%',
  },
  creditCardBtnView: {
    flexDirection: 'row',
    height: '90%',
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 10,
  },
  creditCardCheckBtnView: {
    justifyContent: 'center',
    height: '100%',
    aspectRatio: 1,
    borderRadius: 5,
  },
  creditCardCheckImage: {
    resizeMode: 'contain',
    height: '60%',
    aspectRatio: 1,
    alignSelf: 'center',
  },
  headerTitle: {
    fontSize: 18,
    flex: 1,
    color: BLACK,
    fontFamily: 'Poppins-Medium',
    textAlign: 'center',
    marginRight: 50,
  },
});

export default AddCreditOrDebitCard;
