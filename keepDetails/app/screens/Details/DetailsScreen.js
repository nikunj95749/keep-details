import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  SafeAreaView,
  Text,
  TouchableOpacity,
} from 'react-native';
import {BLACK, THEME_COLOR, WHITE, WINDOW_WIDTH} from '../../styles';
import {
  InterstitialAd,
  AdEventType,
  BannerAd,
  BannerAdSize,
} from '@react-native-firebase/admob';
import {getValueFromLocal, setValueInLocal} from '../../helper/auth';

import FamilyIcon from '../../assets/appimages/FamilyIcon.svg';
import Identification_icn from '../../assets/appimages/Identification_icn.svg';
import Passwords from '../../assets/appimages/Passwords.svg';
import DabitCard from '../../assets/appimages/DabitCard.svg';
import {bannerAdUnitId, interstitialAdUnitId} from '../../config/constants';
import { useSelector } from 'react-redux';

const interstitial = InterstitialAd.createForAdRequest(interstitialAdUnitId, {
  requestNonPersonalizedAdsOnly: true,
});
function Details({navigation}) {
  const [loaded, setLoaded] = useState(false);
  const authToken = useSelector((state) => state.auth?.authToken ?? '');


  useEffect(() => {
    const eventListener = interstitial.onAdEvent((type) => {
      if (type === AdEventType.LOADED) {
        setLoaded(true);
        setTimeout(() => {
          interstitial.show();
        }, 2000);
      }
    });

    // Unsubscribe from events on unmount
    return () => {
      eventListener();
    };
  }, []);

  const initializeAdd = async () => {
    const notes = await getValueFromLocal('MyDetails');
    console.log('MyDetails=== ', JSON.parse(notes));
    if (notes === null) {
      setValueInLocal('MyDetails', JSON.stringify(0));
    } else if (JSON.parse(notes) >= 3) {
      interstitial.load();
      setValueInLocal('MyDetails', JSON.stringify(0));
    } else {
      setValueInLocal('MyDetails', JSON.stringify(JSON.parse(notes) + 1));
    }
  };

  const [arrsDetails, setArrsDetails] = useState([
    {
      name: 'Family & Friends',
      image: <FamilyIcon />,
      key: 'FriendsNFamily',
    },
    // { name: 'Addresses', image: appImages.details ,key:'Addresses'},
    {
      name: 'Credit/Debit Cards',
      image: <DabitCard />,
      key: 'CreditOrDebitCard',
    },
    // {
    //   name: 'ADMOB',
    //   image: appImages.credi_icn,
    //   key: 'CreditOrDebitCard',
    // },
    {
      name: 'Identifications',
      image: <Identification_icn />,
      key: 'Identifications',
    },
    {
      name: 'Passwords',
      image: <Passwords />,
      // image: appImages.password,
      key: 'PassWords',
    },
  ]);

  const DetailListItem = ({item, index, navigation}) => {
    // return item?.name === 'ADMOB' ? (
    //   <NativeAdView
    //     ref={nativeAdViewRef}
    //     style={[
    //       styles.adMobView,
    //       {
    //         flexDirection: 'row',
    //         width: WINDOW_WIDTH - 20,
    //         alignSelf: 'center',
    //       },
    //     ]}
    //     adUnitID="ca-app-pub-3940256099942544/2247696110">
    //     <AdBadge
    //       style={{
    //         width: 20,
    //         height: 20,
    //         borderWidth: 1,
    //         borderRadius: 2,
    //         borderColor: THEME_COLOR,
    //       }}
    //       textStyle={{
    //         fontSize: 8,
    //         color: THEME_COLOR,
    //         marginLeft: 2,
    //         marginTop: 4,
    //       }}
    //     />
    //     <HeadlineView
    //       style={{
    //         // fontWeight: 'bold',
    //         fontSize: 13,
    //         marginLeft: 25,
    //         fontFamily: 'Poppins-SemiBold',
    //         color: THEME_COLOR,
    //       }}
    //     />
    //     <View
    //       style={{
    //         width: WINDOW_WIDTH - 20,
    //         flex: 1,
    //         flexDirection: 'row',
    //         marginTop: 5,
    //       }}>
    //       <ImageView
    //         style={{
    //           height: 50,
    //           aspectRatio: 1,
    //           marginLeft: 10,
    //           borderRadius: 5,
    //           overflow: 'hidden',
    //           borderColor: THEME_COLOR,
    //           borderWidth: 0.5,
    //           marginBottom: 10,
    //         }}
    //       />
    //       <View style={{flex: 1}}>
    //         <TaglineView
    //           numberOfLines={2}
    //           style={{
    //             fontSize: responsiveScale(7),
    //             marginLeft: 10,
    //             fontFamily: 'Poppins-Medium',
    //             color: THEME_COLOR,
    //             marginRight: 10,
    //           }}
    //         />
    //         <StarRatingView
    //           style={{
    //             marginTop: 5,
    //             marginLeft: 10,
    //           }}
    //         />
    //         {/* <AdvertiserView
    //           style={{
    //             fontSize: responsiveScale(13),
    //             marginLeft: 10,
    //             fontFamily: 'Poppins-Medium',
    //             color: THEME_COLOR,
    //           }}
    //         /> */}
    //       </View>
    //     </View>
    //   </NativeAdView>
    // ) : (
    return (
      <TouchableOpacity
        activeOpacity={0.85}
        style={[
          styles.detaillistItemMainView,
          {
            flexDirection: index % 2 === 0 ? 'row' : 'row-reverse',
            width: WINDOW_WIDTH - 20,
            alignSelf: 'center',
          },
        ]}
        onPress={() => handleOnPressItem(item.key)}>
        <View style={styles.detaillistItemImageView}>
          {/* <Image style={styles.detaillistItemImage} source={item.image} /> */}
          {/* <FamilyIcon /> */}
          {item.image}
        </View>
        <Text
          style={[
            styles.detailsListItemTypeTxt,
            {textAlign: index % 2 === 0 ? 'left' : 'right'},
          ]}>
          {item.name}
        </Text>
      </TouchableOpacity>
    );
    // );
  };

  const handleOnPressItem = (key) => {
    if (!authToken) {
      navigation.navigate('Signin');
      return;
    }
    initializeAdd();
    navigation.navigate(key);
  };

  return (
    <View style={styles.mainView}>
      <SafeAreaView />

      <View style={styles.flatListMainView}>
        <FlatList
          renderse
          showsVerticalScrollIndicator={false}
          data={arrsDetails}
          renderItem={({item, index}) => (
            <DetailListItem item={item} index={index} navigation={navigation} />
          )}
          ListHeaderComponent={() => (
            <View style={styles.titleView}>
              <Text style={styles.headerTitle}>My Details</Text>
            </View>
          )}
          keyExtractor={(item) => item.name}
        />
      </View>
      <BannerAd
        size={BannerAdSize.SMART_BANNER}
        requestOptions={{
          requestNonPersonalizedAdsOnly: true,
        }}
        unitId={bannerAdUnitId}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: WHITE,
    overflow: 'visible',
  },
  flatListMainView: {
    flex: 1,
  },
  detaillistItemMainView: {
    width: '100%',

    overflow: 'visible',
    aspectRatio: 3.5,
    backgroundColor: WHITE,
    marginVertical: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.25,
    shadowRadius: 2,
    elevation: 5,
    borderRadius: 10,
  },
  adMobView: {
    width: '100%',
    overflow: 'visible',
    backgroundColor: WHITE,
    marginVertical: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.25,
    shadowRadius: 2,
    elevation: 5,
    borderRadius: 10,
  },
  titleView: {
    width: '100%',
    backgroundColor: WHITE,
    marginVertical: 0,
    paddingLeft: 20,
  },
  detaillistItemImageMainView: {
    flex: 1,
  },
  detaillistItemImageView: {
    height: '60%',
    aspectRatio: 1,
    alignSelf: 'center',
    marginHorizontal: 20,
  },
  detaillistItemImage: {
    width: '100%',
    height: '100%',
  },
  detailsListItemTypeView: {
    width: '100%',
    aspectRatio: 6,
    backgroundColor: 'red',
    alignSelf: 'center',
  },
  detailsListItemTypeTxt: {
    fontSize: 18,
    alignSelf: 'center',
    flex: 1,
    color: THEME_COLOR,
  },
  headerTitle: {
    textAlign: 'left',
    color: BLACK,
    fontFamily: 'Poppins-Medium',
    fontSize: 25,
  },
});

export default Details;
