import React, {useState, useMemo} from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  Text,
  ActivityIndicator,
  Image,
  TouchableOpacity,
} from 'react-native';

import ImagePicker from 'react-native-image-crop-picker';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  addDetails,
  updateDetails,
} from '../../../../firebaseServices/Details/details';
import {appImages} from '../../../../config';
import {useSelector} from 'react-redux';
import {isEmpty} from 'lodash';
import {Root, Popup} from 'popup-ui';
import {
  BLACK,
  responsiveScale,
  scaleFont,
  THEME_COLOR,
  WHITE,
} from '../../../../styles';
import TextInputWithTitle from '../../../../components/TextInputWithTitle';
import {
  CANCEL,
  CHOOSE_FROM_GALLERY,
  TAKE_PHOTO,
} from '../../../../config/constants';
import CustomActionsheet from '../../../../components/CustomActionsheet';
import PinLock from '../../../../assets/appimages/PinLock.svg';

function AddNotes({navigation, route}) {
  const selectedItem = useMemo(() => route?.params?.selectedItem ?? {}, []);
  const authToken = useSelector((state) => state.auth?.authToken ?? '');

  const [imgSubject, setImgSubject] = useState(selectedItem?.imageUrl ?? '');
  const [txtType, setTxtType] = useState(selectedItem?.type ?? '');
  const [txtUserName, setTxtUserName] = useState(selectedItem?.id_name ?? '');
  const [txtPassword, setTxtPassword] = useState(selectedItem?.password ?? '');
  const [txtNote, setTxtNote] = useState(selectedItem?.note ?? '');

  const [isLoading, setIsLoading] = useState(false);

  const actionSheetItemsForPostPrivacy = [
    {
      text: CHOOSE_FROM_GALLERY,
      onPress: () => {
        choosePhotoFromGallery();
      },
    },
    {
      text: TAKE_PHOTO,
      onPress: () => {
        takePhotoFromCamera();
      },
    },
    {
      text: CANCEL,
      onPress: () => setActionsheetVisible(false),
    },
  ];

  const [actionsheetVisible, setActionsheetVisible] = useState(false);

  const choosePhotoFromGallery = () => {
    try {
      setActionsheetVisible(false);
      setTimeout(() => {
        ImagePicker.openPicker({
          width: 300,
          height: 400,
          cropping: true,
          compressImageQuality: 0.8,
        })
          .then(async (response) => {
            console.log('response==== ', response?.path);
            setImgSubject(response?.path);
          })
          .catch((err) => {});
      }, 500);
    } catch (error) {}
  };

  const takePhotoFromCamera = async () => {
    setActionsheetVisible(false);

    setTimeout(() => {
      ImagePicker.openCamera({
        includeBase64: true,
        cancelable: true,
        compressImageQuality: 0.8,
      })
        .then(async (response) => {
          setImgSubject(response?.path);
        })
        .catch((error) => {
          console.log('error in Camera ===>', error);
        });
    }, 600);
  };

  const handleType = (text) => {
    setTxtType(text);
  };

  const handleUserName = (text) => {
    setTxtUserName(text);
  };

  const handlePassword = (text) => {
    setTxtPassword(text);
  };

  const handleNotes = (text) => {
    setTxtNote(text);
  };

  const _handleSubmitButton = async () => {
    if (!txtUserName) {
      Popup.show({
        type: 'Warning',
        title: 'Empty required field!',
        textBody: 'Please enter Username.',
        buttonText: 'Ok',
        callback: () => Popup.hide(),
      });
      return;
    }

    setIsLoading(true);
    const data = {
      type: txtType,
      id_name: txtUserName,
      password: txtPassword,
      note: txtNote,
    };
    if (isEmpty(selectedItem)) {
      data['createdAt'] = new Date();
      await addDetails('passwords', data, authToken, imgSubject);
      setIsLoading(false);
      navigation.goBack();
      return;
    }

    if (selectedItem?.imageUrl !== imgSubject) {
      await updateDetails(
        'passwords',
        {...data, id: selectedItem?.id},
        authToken,
        imgSubject,
      );
      setIsLoading(false);
      navigation.goBack();
      return;
    }

    await updateDetails(
      'passwords',
      {...data, id: selectedItem?.id},
      authToken,
    );
    setIsLoading(false);
    navigation.goBack();
  };

  return (
    <Root>
      <View style={styles.mainView}>
        <SafeAreaView />

        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            marginLeft: 15,
            height: 30,
          }}>
          <TouchableOpacity
            style={{
              height: '100%',
              aspectRatio: 1,
              backgroundColor: 'white',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() => {
              navigation.goBack();
            }}>
            <Image
              resizeMode={'contain'}
              style={{height: 22, width: 25}}
              source={appImages.back}
            />
          </TouchableOpacity>
          <Text adjustsFontSizeToFit style={styles.headerTitle}>
            {' '}
            Add Email/Username Details
          </Text>
        </View>
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps="handled"
          extraScrollHeight={10}>
          <View style={{width: '100%', flex: 1}}>
            <TouchableOpacity
              style={{
                marginTop: 10,
                width: '25%',
                aspectRatio: 1,
                borderStyle: 'dashed',
                alignSelf: 'center',
                padding: 10,
                borderWidth: 4,
                borderColor: THEME_COLOR,
                borderRadius: 62,
                overflow: 'hidden',
              }}
              onPress={() => {
                setActionsheetVisible(true);
              }}>
              {!imgSubject ? (
                <PinLock />
              ) : (
                <Image
                  style={{
                    height: '100%',
                    width: '100%',
                    resizeMode: 'cover',
                  }}
                  source={{
                    uri: imgSubject,
                  }}
                />
              )}
            </TouchableOpacity>

            <TextInputWithTitle
              title={'Type:'}
              value={txtType}
              onChangeText={handleType}
            />
            <TextInputWithTitle
              title={'UserName/Email Id:'}
              isRequired={true}
              value={txtUserName}
              onChangeText={handleUserName}
            />

            <TextInputWithTitle
              title={'Password:'}
              secureTextEntry
              autoCapitalize="none"
              value={txtPassword}
              onChangeText={handlePassword}
            />

            <TextInputWithTitle
              title={'Note:'}
              underlineColorAndroid="transparent"
              autoCapitalize="none"
              multiline={true}
              value={txtNote}
              onChangeText={handleNotes}
              aspectRatio={120}
            />
            <TouchableOpacity
              disabled={isLoading}
              style={{
                width: isEmpty(selectedItem) ? 100 : 120,
                height: 50,
                backgroundColor: THEME_COLOR,
                alignSelf: 'center',
                marginVertical: 20,
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 4,
              }}
              onPress={_handleSubmitButton}>
              {isLoading ? (
                <ActivityIndicator color={'white'} />
              ) : (
                <Text
                  style={{
                    color: WHITE,
                    fontFamily: 'Poppins-Regular',
                    fontSize: 16,
                  }}>
                  {isEmpty(selectedItem) ? 'Save' : 'Update'}
                </Text>
              )}
              
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
      </View>
      <CustomActionsheet
        isVisible={actionsheetVisible}
        actionItems={actionSheetItemsForPostPrivacy}
        OnBackdropPress={() => {
          setActionsheetVisible(false);
        }}
      />
    </Root>
  );
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: 'white',
  },
  headerTitle: {
    flex: 1,
    color: BLACK,
    fontFamily: 'Poppins-Medium',
    fontSize: responsiveScale(10),
    textAlign: 'center',
    marginRight: 50,
  },
});

export default AddNotes;
