import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Image,
  SafeAreaView,
  TouchableOpacity,
  Text,
  Alert,
  ActivityIndicator,
} from 'react-native';

import {appImages} from '../../../config/images';
import {useSelector} from 'react-redux';
import {
  BLACK,
  LIGHT_GRAY_10,
  responsiveScale,
  scaleFont,
  scaleSize,
  WHITE,
} from '../../../styles';
import AddButton from '../../../components/AddButton';
import moment from 'moment';
import {SwipeListView} from 'react-native-swipe-list-view';
import {deleteDetails} from '../../../firebaseServices/Details/details';
import PinLock from '../../../assets/appimages/PinLock.svg';
import Delete from '../../../assets/appimages/Delete.svg';

import {firebase} from '@react-native-firebase/storage';
import EmptyView from '../../../components/EmptyView';
import { ToNavigationBar } from '../../../components/ToNavigationBar';

function PassWords({navigation}) {
  const [arrsDetails, setArrDetails] = useState([]);
  const authToken = useSelector((state) => state.auth?.authToken ?? '');
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const getIdentificationsData = () => {
      firebase
        .firestore()
        .collection('Users')
        .doc(authToken)
        .collection('passwords')
        .onSnapshot((snapshot) => {
          const data = snapshot.docs.map((doc) => ({
            id: doc.id,
            ...doc.data(),
          }));
          setArrDetails(data);
          setIsLoading(false);
        });
    };
    getIdentificationsData();
  }, []);

  const _handleAddPasswordItemPress = (item) => {
    navigation.navigate('AddPassword', {
      selectedItem: item,
    });
  };

  const FriendsNFamilyListItem = ({item, index, navigation}) => {
    var timestemp = new Date(item?.createdAt?.toDate());
    return (
      <View>
        <TouchableOpacity
          activeOpacity={0.85}
          style={styles.friendsNFamilyListItemMainView}
          onPress={() => _handleAddPasswordItemPress(item)}>
          <View
            style={{
              marginLeft: 10,
              height: 65,
              aspectRatio: 1,
              marginTop: 10,
              borderRadius: 10,
              overflow: 'hidden',
            }}>
            {!item?.imageUrl ? (
              <PinLock />
            ) : (
              <Image
                style={{height: '100%', width: '100%', resizeMode: 'cover'}}
                source={{
                  uri: item?.imageUrl,
                }}
              />
            )}
          </View>
          <View
            style={{
              marginLeft: 10,
              marginVertical: 8,
              flex: 1,
              marginRight: 10,
            }}>
            <Text
              style={{fontFamily: 'Poppins-Medium', fontSize: scaleFont(16)}}>
              Type:{item.type}
            </Text>
            <Text
              style={{fontFamily: 'Poppins-Medium', fontSize: scaleFont(16)}}>
              UserName: {item.id_name}
            </Text>
            <Text
              style={{
                fontFamily: 'Poppins-Medium',
              }}>
              {`${moment(timestemp)?.format('ddd, MMMM Do YYYY hh:mm:ss a')}`}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  const FriendsNFamilyListHiddenItem = ({item, index, navigation}) => {
    return (
      <View>
        <TouchableOpacity
          activeOpacity={0.85}
          style={styles.friendsNFamilyListItemMainView}
          ListHeaderComponent={() => (
            <View style={styles.titleView}>
              <Text style={styles.headerTitle}>Cards</Text>
            </View>
          )}>
          <View
            style={{
              width: '100%',
              flex: 1,
              borderRadius: 10,
              overflow: 'hidden',
              alignItems: 'flex-end',
            }}>
            <TouchableOpacity
              onPress={() => {
                Alert.alert('', 'Do you really want to delete this item?', [
                  {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                  },
                  {
                    text: 'OK',
                    onPress: () =>
                      deleteDetails('passwords', item?.item, authToken),
                  },
                ]);
              }}
              style={{
                height: 90,
                width: 90,
                backgroundColor: 'red',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <View style={{height: 50, width: 50}}>
                <Delete />
              </View>
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

 

  return (
    <View style={styles.mainView}>
      <SafeAreaView />
      <ToNavigationBar
        onPressBack={() => {
          navigation.goBack();
        }}
        title={'Passwords'}
        onPressSearch={() => {
          navigation.navigate('SearchPassWordsScreen', {
            defaultDetails: arrsDetails,
          });
        }}
      />
      <View style={styles.flatListMainView}>
        {arrsDetails?.length > 0 ? (
          <SwipeListView
            data={arrsDetails}
            renderItem={({item, index}) => (
              <FriendsNFamilyListItem
                item={item}
                index={index}
                navigation={navigation}
              />
            )}
            renderHiddenItem={(data, rowMap) => (
              <FriendsNFamilyListHiddenItem
                item={data}
                navigation={navigation}
              />
            )}
            rightOpenValue={-75}
            keyExtractor={(item) => item.id}
          />
        ) : (
          <>
            {!isLoading ? (
              <EmptyView
                title={'Empty Notes!'}
                titleImage={appImages.emptyNotes}
                titleStyle={{marginTop: scaleSize(-55)}}
              />
            ) : (
              <ActivityIndicator size="large" color={'black'} />
            )}
          </>
        )}
      </View>
      <AddButton
        onPress={() => {
          navigation.navigate('AddPassword');
        }}
      />
      <SafeAreaView />
    </View>
  );
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: WHITE,
  },
  flatListMainView: {
    marginTop: 10,
    flex: 1,
  },
  friendsNFamilyListItemMainView: {
    width: '92%',
    alignSelf: 'center',
    flexDirection: 'row',
    backgroundColor: WHITE,
    shadowColor: '#000',
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.25,
    shadowRadius: 2,
    elevation: 5,
    borderRadius: 10,
    marginTop: 15,
    marginBottom: 10,
  },
  titleView: {
    width: '100%',
    aspectRatio: 8,
    backgroundColor: WHITE,
    marginVertical: 10,
    justifyContent: 'flex-end',
    paddingLeft: 20,
  },
  headerTitle: {
    flex: 1,
    color: BLACK,
    fontFamily: 'Poppins-Medium',
    fontSize: responsiveScale(10),
    textAlign: 'center',
    marginRight: 50,
  },
});

export default PassWords;
