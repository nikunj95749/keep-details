import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Image,
  Text,
  Alert,
  ActivityIndicator,
} from 'react-native';
import {appImages} from '../../../config/images';
import {useSelector} from 'react-redux';
import {BLACK, responsiveScale, scaleFont, scaleSize, WHITE} from '../../../styles';
import AddButton from '../../../components/AddButton';
import moment from 'moment';
import {SwipeListView} from 'react-native-swipe-list-view';
import {deleteDetails} from '../../../firebaseServices/Details/details';
import User from '../../../assets/appimages/User.svg';
import Delete from '../../../assets/appimages/Delete.svg';
import Search from '../../../assets/appimages/Search.svg';
import {firebase} from '@react-native-firebase/storage';
import EmptyView from '../../../components/EmptyView';
import { ToNavigationBar } from '../../../components/ToNavigationBar';

function Identifications({navigation}) {
  const [arrsDetails, setArrDetails] = useState([]);
  const authToken = useSelector((state) => state.auth?.authToken ?? '');
  const [isLoading, setIsLoading] = useState(true);


  useEffect(() => {
    const getIdentificationsData = () => {
      firebase
        .firestore()
        .collection('Users')
        .doc(authToken)
        .collection('identifications')
        .onSnapshot((snapshot) => {
          const data = snapshot.docs.map((doc) => ({
            id: doc.id,
            ...doc.data(),
          }));
          setArrDetails(data);
          setIsLoading(false);
        });
    };
    getIdentificationsData();
  }, []);

  const _handleIdentityItemPress = (item) => {
    navigation.navigate('AddIdentifications', {
      selectedItem: item,
    });
  };

  const IdentityListItem = ({item, index, navigation}) => {
    var timestemp = new Date(item?.createdAt?.toDate());
    return (
      <View>
        
        <TouchableOpacity
          activeOpacity={0.95}
          style={styles.identityListItemMainView}
          onPress={() => _handleIdentityItemPress(item)}>
          <View
            style={{
              marginLeft: 10,
              height: 65,
              aspectRatio: 1,
              borderRadius: 10,
              overflow: 'hidden',
              marginTop: 10,
            }}>
            {!item?.frontImageUrl ? (
              <User />
            ) : (
              <Image
                style={{height: '100%', width: '100%', resizeMode: 'cover'}}
                source={{
                  uri: item?.frontImageUrl,
                }}
              />
            )}
          </View>
          <View
            style={{
              marginLeft: 10,
              marginVertical: 10,
              flex: 1,
              marginRight: 10,
            }}>
            <Text
              style={{fontFamily: 'Poppins-Medium', fontSize: scaleFont(16)}}>
              {item?.doc_name}
            </Text>
            <Text
              style={{fontFamily: 'Poppins-Medium', fontSize: scaleFont(16)}}>
              Number: {item?.issued_number}
            </Text>
            <Text
              style={{
                fontFamily: 'Poppins-Medium',
              }}>
              {`${moment(timestemp)?.format('ddd, MMMM Do YYYY hh:mm:ss a')}`}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  const IdentityListHiddenItem = ({item, index, navigation}) => {
    return (
      <View>
        <TouchableOpacity
          activeOpacity={0.85}
          style={styles.identityListItemMainView}
          onPress={() => _handleIdentityItemPress(item)}>
          <View
            style={{
              width: '100%',
              flex: 1,
              borderRadius: 10,
              overflow: 'hidden',
              alignItems: 'flex-end',
            }}>
            <TouchableOpacity
              onPress={() => {
                Alert.alert('', 'Do you really want to delete this item?', [
                  {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                  },
                  {
                    text: 'OK',
                    onPress: () =>
                      deleteDetails('identifications', item?.item, authToken),
                  },
                ]);
              }}
              style={{
                height: 80,
                width: 90,
                backgroundColor: 'red',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <View style={{height: 50, width: 50}}>
                <Delete />
              </View>
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={styles.mainView}>
      <SafeAreaView />
      <ToNavigationBar
        onPressBack={() => {
          navigation.goBack();
        }}
        title={'Identity & Document'}
        onPressSearch={() => {
          navigation.navigate('SearchIdentificationsScreen', {
            defaultDetails: arrsDetails,
          });
        }}
      />
      <View style={styles.flatListMainView}>
        {arrsDetails?.length > 0 ? (
          <SwipeListView
            data={arrsDetails}
            renderItem={({item, index}) => (
              <IdentityListItem
                item={item}
                index={index}
                navigation={navigation}
              />
            )}
            renderHiddenItem={(data, rowMap) => (
              <IdentityListHiddenItem item={data} navigation={navigation} />
            )}
            rightOpenValue={-75}
            keyExtractor={(item) => item.id}
          />
        ) : (
          <>
            {!isLoading ? (
              <EmptyView
                title={'Empty Notes!'}
                titleImage={appImages.emptyNotes}
                titleStyle={{marginTop: scaleSize(-55)}}
              />
            ) : (
              <ActivityIndicator size="large" color={'black'} />
            )}
          </>
        )}
      </View>
      <AddButton
        onPress={() => {
          navigation.navigate('AddIdentifications');
        }}
      />
      <SafeAreaView />
    </View>
  );
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: WHITE,
  },
  flatListMainView: {
    marginTop: 10,
    flex: 1,
  },
  identityListItemMainView: {
    width: '92%',
    alignSelf: 'center',
    flexDirection: 'row',
    backgroundColor: WHITE,
    shadowColor: '#000',
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.25,
    shadowRadius: 2,
    elevation: 5,
    borderRadius: 10,
    marginTop: 15,
    marginBottom: 10,
  },
  titleView: {
    width: '100%',
    aspectRatio: 8,
    backgroundColor: WHITE,
    marginVertical: 10,
    justifyContent: 'flex-end',
    paddingLeft: 20,
  },
  headerTitle: {
    color: BLACK,
    fontFamily: 'Poppins-Medium',
    fontSize: responsiveScale(10),
    marginRight: 50,
  },
});
export default Identifications;
