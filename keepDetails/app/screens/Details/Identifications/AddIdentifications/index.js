import React, {useState, useMemo} from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  Text,
  ActivityIndicator,
  Image,
  TouchableOpacity,
} from 'react-native';
import {appImages} from '../../../../config';
import ImagePicker from 'react-native-image-crop-picker';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  addIdentitiesDetails,
  updateIdentitiesDetails,
} from '../../../../firebaseServices/Details/details';
import {isEmpty} from 'lodash';
import {useSelector} from 'react-redux';
import {Root, Popup} from 'popup-ui';
import {
  BLACK,
  responsiveScale,
  scaleFont,
  THEME_COLOR,
  WHITE,
} from '../../../../styles';
import TextInputWithTitle from '../../../../components/TextInputWithTitle';
import {
  CANCEL,
  CHOOSE_FROM_GALLERY,
  TAKE_PHOTO,
} from '../../../../config/constants';
import CustomActionsheet from '../../../../components/CustomActionsheet';

const options = {
  title: 'Choose Photo',
  takePhotoButtonTitle: 'Camera',
  chooseFromLibraryButtonTitle: 'Gallery',
  mediaType: 'photo',
  // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

function AddIdentifications({navigation, route}) {
  const selectedItem = useMemo(() => route?.params?.selectedItem ?? {}, []);

  const authToken = useSelector((state) => state.auth?.authToken ?? '');

  const [imgFront, setImgFront] = useState(selectedItem?.frontImageUrl ?? '');
  const [imgBack, setImgBack] = useState(selectedItem?.backImageUrl ?? '');
  const [txtIdName, setTxtIdName] = useState(selectedItem?.doc_name ?? '');
  const [txtIdNumber, setTxtIdNumber] = useState(
    selectedItem?.issued_number ?? '',
  );
  const [isLoading, setIsLoading] = useState(false);
  const [txtNote, setTxtNote] = useState(selectedItem?.note ?? '');

  const [isFrontImage, setIsFrontImage] = useState(false);

  const actionSheetItemsForPostPrivacy = [
    {
      text: CHOOSE_FROM_GALLERY,
      onPress: () => {
        choosePhotoFromGallery();
      },
    },
    {
      text: TAKE_PHOTO,
      onPress: () => {
        takePhotoFromCamera();
      },
    },
    {
      text: CANCEL,
      onPress: () => setActionsheetVisible(false),
    },
  ];

  const [actionsheetVisible, setActionsheetVisible] = useState(false);

  const choosePhotoFromGallery = () => {
    try {
      console.log('setIsFrontImage(true);==== ', isFrontImage);
      setActionsheetVisible(false);
      setTimeout(() => {
        ImagePicker.openPicker({
          width: 400,
          height: 280,
          cropping: true,
          compressImageQuality: 0.8,
        })
          .then(async (response) => {
            console.log('response==== ', response?.path);
            if (isFrontImage) {
              setImgFront(response?.path);
            } else {
              setImgBack(response?.path);
            }
            // setImgSubject(response?.sourceURL);
          })
          .catch((err) => {});
      }, 500);
    } catch (error) {}
  };

  const takePhotoFromCamera = async () => {
    setActionsheetVisible(false);

    setTimeout(() => {
      ImagePicker.openCamera({
        width: 400,
        height: 300,
        cropping: true,
        includeBase64: true,
        cancelable: true,
        compressImageQuality: 0.8,
      })
        .then(async (response) => {
          if (isFrontImage) {
            setImgFront(response?.path);
          } else {
            setImgBack(response?.path);
          }
        })
        .catch((error) => {
          console.log('error in Camera ===>', error);
        })
        .finally(() => {
          // setIsLoading(false);
        });
    }, 600);
  };

  const handleIdName = (text) => {
    setTxtIdName(text);
  };

  const handleIdNumber = (text) => {
    setTxtIdNumber(text);
  };

  const handleNotes = (text) => {
    setTxtNote(text);
  };

  const _handleSubmitButton = async () => {
    if (!txtIdName) {
      Popup.show({
        type: 'Warning',
        title: 'Empty required field!',
        textBody: 'Please enter Document name.',
        buttonText: 'Ok',
        callback: () => Popup.hide(),
      });
      return;
    }
    // setTimeout(() => {
    //   navigation.goBack();
    // }, 400);
    setIsLoading(true);
    const data = {
      doc_name: txtIdName,
      issued_number: txtIdNumber,
      note: txtNote,
    };

    console.log('data==== ', data);

    if (isEmpty(selectedItem)) {
      data['createdAt'] = new Date();
      await addIdentitiesDetails(
        'identifications',
        data,
        authToken,
        imgFront,
        imgBack,
      );
      setIsLoading(false);
      navigation.goBack();
      return;
    }

    if (
      selectedItem?.frontImageUrl !== imgFront &&
      selectedItem?.backImageUrl !== imgBack
    ) {
      await updateIdentitiesDetails(
        'identifications',
        {...data, id: selectedItem?.id},
        authToken,
        imgFront,
        imgBack,
      );
      setIsLoading(false);
      navigation.goBack();
      return;
    }

    if (
      selectedItem?.backImageUrl !== imgBack &&
      selectedItem?.frontImageUrl == imgFront
    ) {
      await updateIdentitiesDetails(
        'identifications',
        {...data, id: selectedItem?.id},
        authToken,
        '',
        imgBack,
      );
      setIsLoading(false);
      navigation.goBack();
      return;
    }

    if (
      selectedItem?.backImageUrl == imgBack &&
      selectedItem?.frontImageUrl !== imgFront
    ) {
      await updateIdentitiesDetails(
        'identifications',
        {...data, id: selectedItem?.id},
        authToken,
        imgFront,
        '',
      );
      setIsLoading(false);
      navigation.goBack();
      return;
    }

    await updateIdentitiesDetails(
      'identifications',
      {...data, id: selectedItem?.id},
      authToken,
    );
    setIsLoading(false);
    navigation.goBack();
  };

  return (
    <Root>
      <View style={styles.mainView}>
        <SafeAreaView />
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            marginLeft: 15,
            height: 30,
          }}>
          <TouchableOpacity
            style={{
              height: '100%',
              aspectRatio: 1,
              backgroundColor: 'white',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() => {
              navigation.goBack();
            }}>
            <Image
              resizeMode={'contain'}
              style={{height: 22, width: 25}}
              source={appImages.back}
            />
          </TouchableOpacity>
          <Text adjustsFontSizeToFit style={styles.headerTitle}>
            {' '}
            Add Identity & Document Details
          </Text>
        </View>
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps="handled"
          extraScrollHeight={10}>
          <View style={{width: '100%', flex: 1}}>
            <View
              style={{
                width: '85%',
                aspectRatio: 12.7,
                marginTop: 10,
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  marginLeft: 10,
                  fontFamily: 'Poppins-Regular',
                  fontSize: scaleFont(16),
                }}>
                Front Image:{' '}
              </Text>
            </View>
            <TouchableOpacity
              style={{
                marginTop: 10,
                width: '85%',
                aspectRatio: 1.7,
                borderWidth: imgFront === '' ? 1 : 0,
                borderStyle: 'dashed',
                borderColor: THEME_COLOR,
                alignSelf: 'center',
                borderRadius: 25,
                overflow: 'hidden',
              }}
              onPress={() => {
                setIsFrontImage(true);
                setActionsheetVisible(true);
              }}>
              <Image
                style={{
                  height: '100%',
                  width: '100%',
                  borderRadius: 25,
                  overflow: 'hidden',
                  resizeMode: 'contain',
                }}
                source={
                  !imgFront
                    ? appImages.cardFront
                    : {
                        uri: imgFront,
                      }
                }
              />
            </TouchableOpacity>
            <View
              style={{
                width: '85%',
                aspectRatio: 12.7,
                marginTop: 10,
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  marginLeft: 10,
                  fontFamily: 'Poppins-Regular',
                  fontSize: scaleFont(16),
                }}>
                Back Image:{' '}
              </Text>
            </View>
            <TouchableOpacity
              style={{
                marginTop: 10,
                width: '85%',
                aspectRatio: 1.7,
                borderWidth: imgBack === '' ? 1 : 0,
                borderStyle: 'dashed',
                borderColor: THEME_COLOR,
                alignSelf: 'center',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 25,
                overflow: 'hidden',
              }}
              onPress={() => {
                setIsFrontImage(false);
                setActionsheetVisible(true);
              }}>
              <Image
                style={{
                  height: '100%',
                  width: '100%',
                  borderRadius: 25,
                  overflow: 'hidden',
                  resizeMode: 'contain',
                }}
                source={
                  !imgBack
                    ? appImages.cardBack
                    : {
                        uri: imgBack,
                      }
                }
              />
            </TouchableOpacity>
            <TextInputWithTitle
              title={'Identity/Document name:'}
              isRequired={true}
              value={txtIdName}
              onChangeText={handleIdName}
            />

            <TextInputWithTitle
              title={'Issued number:'}
              value={txtIdNumber}
              onChangeText={handleIdNumber}
            />

            <TextInputWithTitle
              title={'Note:'}
              underlineColorAndroid="transparent"
              autoCapitalize="none"
              multiline={true}
              value={txtNote}
              onChangeText={handleNotes}
              aspectRatio={120}
            />
            <TouchableOpacity
            disabled={isLoading}
              style={{
                width: isEmpty(selectedItem) ? 100 : 120,
                height: 50,
                backgroundColor: THEME_COLOR,
                alignSelf: 'center',
                marginVertical: 20,
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 4,
              }}
              onPress={_handleSubmitButton}>
              {isLoading ? (
                <ActivityIndicator color={'white'} />
              ) : (
                <Text
                  style={{
                    color: WHITE,
                    fontFamily: 'Poppins-Regular',
                    fontSize: 16,
                  }}>
                  {isEmpty(selectedItem) ? 'Save' : 'Update'}
                </Text>
              )}
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
      </View>
      <CustomActionsheet
        isVisible={actionsheetVisible}
        actionItems={actionSheetItemsForPostPrivacy}
        OnBackdropPress={() => {
          setActionsheetVisible(false);
        }}
      />
    </Root>
  );
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: 'white',
  },
  headerTitle: {
    flex: 1,
    color: BLACK,
    fontFamily: 'Poppins-Medium',
    fontSize: responsiveScale(10),
    textAlign: 'center',
    marginRight: 50,
  },
});

export default AddIdentifications;
