import {firebase} from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';

export const addDetails = async (
  type = '',
  data = {},
  authToken = '',
  imageUri = '',
) => {
  console.log('authToken  ', authToken);
  const autoId = firebase.firestore().collection('Users').doc().id;

  const imagePath = `Images/useridIsHere/${type}/${autoId}`;

  if (imageUri) {
    await storage().ref(imagePath).putFile(imageUri);
    let imageRef = await storage()
      .ref('/' + imagePath)
      .getDownloadURL();

    await firebase
      .firestore()
      .collection('Users')
      .doc(authToken)
      .collection(type)
      .doc(autoId)
      .set({...data, imageUrl: imageRef, id: autoId});
    return;
  }

  await firebase
    .firestore()
    .collection('Users')
    .doc(authToken)
    .collection(type)
    .doc(autoId)
    .set({...data, id: autoId});
};

export const deleteDetails = async (type = '', data = {}, authToken = '') => {
  console.log('authToken  ', authToken, data);

  await firebase
    .firestore()
    .collection('Users')
    .doc(authToken)
    .collection(type)
    .doc(data?.id)
    .delete();
};

export const updateDetails = async (
  type = '',
  data = {},
  authToken = '',
  imageUri = '',
) => {
  const imagePath = `Images/useridIsHere/${type}/${data?.id}`;

  if (imageUri) {
    await storage().ref(imagePath).putFile(imageUri);
    let imageRef = await storage()
      .ref('/' + imagePath)
      .getDownloadURL();

    await firebase
      .firestore()
      .collection('Users')
      .doc(authToken)
      .collection(type)
      .doc(data?.id)
      .update({...data, imageUrl: imageRef});
    return;
  }

  await firebase
    .firestore()
    .collection('Users')
    .doc(authToken)
    .collection(type)
    .doc(data?.id)
    .update({...data});
};

export const addIdentitiesDetails = async (
  type = '',
  data = {},
  authToken = '',
  frontImageURL = '',
  backImageURL = '',
) => {
  const autoId = firebase.firestore().collection('Users').doc().id;

  if (frontImageURL && !backImageURL) {
    const frontImagePath = `Images/useridIsHere/${type}/${autoId}/front`;

    await storage().ref(frontImagePath).putFile(frontImageURL);
    let frontImageRef = await storage()
      .ref('/' + frontImagePath)
      .getDownloadURL();
    await firebase
      .firestore()
      .collection('Users')
      .doc(authToken)
      .collection(type)
      .doc(autoId)
      .set({
        ...data,
        frontImageUrl: frontImageRef,
        id: autoId,
      });
  } else if (!frontImageURL && backImageURL) {
    const backImagePath = `Images/useridIsHere/${type}/${autoId}/back`;

    await storage().ref(backImagePath).putFile(backImageURL);
    let backImageRef = await storage()
      .ref('/' + backImagePath)
      .getDownloadURL();
    await firebase
      .firestore()
      .collection('Users')
      .doc(authToken)
      .collection(type)
      .doc(autoId)
      .set({
        ...data,
        backImageUrl: backImageRef,
        id: autoId,
      });
  } else if (frontImageURL && backImageURL) {
    const frontImagePath = `Images/useridIsHere/${type}/${autoId}/front`;
    const backImagePath = `Images/useridIsHere/${type}/${autoId}/back`;

    await storage().ref(frontImagePath).putFile(frontImageURL);
    await storage().ref(backImagePath).putFile(backImageURL);
    let frontImageRef = await storage()
      .ref('/' + frontImagePath)
      .getDownloadURL();
    let backImageRef = await storage()
      .ref('/' + backImagePath)
      .getDownloadURL();
    await firebase
      .firestore()
      .collection('Users')
      .doc(authToken)
      .collection(type)
      .doc(autoId)
      .set({
        ...data,
        frontImageUrl: frontImageRef,
        backImageUrl: backImageRef,
        id: autoId,
      });
  } else {
    await firebase
      .firestore()
      .collection('Users')
      .doc(authToken)
      .collection(type)
      .doc(autoId)
      .set({
        ...data,
        id: autoId,
      });
  }
};

export const updateIdentitiesDetails = async (
  type = '',
  data = {},
  authToken = '',
  frontImageURL = '',
  backImageURL = '',
) => {
  if (frontImageURL && !backImageURL) {
    const frontImagePath = `Images/useridIsHere/${type}/${data?.id}/front`;

    await storage().ref(frontImagePath).putFile(frontImageURL);
    let frontImageRef = await storage()
      .ref('/' + frontImagePath)
      .getDownloadURL();

    await firebase
      .firestore()
      .collection('Users')
      .doc(authToken)
      .collection(type)
      .doc(data?.id)
      .update({
        ...data,
        frontImageUrl: frontImageRef,
      });
  } else if (!frontImageURL && backImageURL) {
    const backImagePath = `Images/useridIsHere/${type}/${data?.id}/back`;

    await storage().ref(backImagePath).putFile(backImageURL);
    let backImageRef = await storage()
      .ref('/' + backImagePath)
      .getDownloadURL();

    await firebase
      .firestore()
      .collection('Users')
      .doc(authToken)
      .collection(type)
      .doc(data?.id)
      .update({
        ...data,
        backImageUrl: backImageRef,
      });
  } else if (frontImageURL && backImageURL) {
    const frontImagePath = `Images/useridIsHere/${type}/${data?.id}/front`;
    const backImagePath = `Images/useridIsHere/${type}/${data?.id}/back`;

    await storage().ref(frontImagePath).putFile(frontImageURL);
    await storage().ref(backImagePath).putFile(backImageURL);
    let frontImageRef = await storage()
      .ref('/' + frontImagePath)
      .getDownloadURL();
    let backImageRef = await storage()
      .ref('/' + backImagePath)
      .getDownloadURL();

    await firebase
      .firestore()
      .collection('Users')
      .doc(authToken)
      .collection(type)
      .doc(data?.id)
      .update({
        ...data,
        frontImageUrl: frontImageRef,
        backImageUrl: backImageRef,
      });
  } else {
    await firebase
      .firestore()
      .collection('Users')
      .doc(authToken)
      .collection(type)
      .doc(data?.id)
      .update({
        ...data,
      });
  }
};
