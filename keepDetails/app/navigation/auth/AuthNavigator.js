import React, {useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import SigninScreen from '../../screens/Auth/Login/SigninScreen';
import RegisterScreen from '../../screens/Auth/Login/RegisterScreen';
import ResetPasswordScreen from '../../screens/Auth/Login/ResetPasswordScreen';
import usePushNotificationController from '../../hooks/usePushNotificationController';

const AuthStack = createStackNavigator();
const ModalStack = createStackNavigator();

const ModalStackNavigator = ({navigation}) => {
  return (
    <AuthStack.Navigator initialRouteName="Signin" headerMode="none">
      <AuthStack.Screen
        name="Signin"
        component={SigninScreen}
        options={{headerShown: false}}
      />
      <AuthStack.Screen name="RegisterScreen" component={RegisterScreen} />
      <AuthStack.Screen
        name="ResetPasswordScreen"
        component={ResetPasswordScreen}
      />
    </AuthStack.Navigator>
  );
};

const AuthNavigator = () => {
  usePushNotificationController();
  return (
    <NavigationContainer>
      <ModalStack.Navigator headerMode="none" initialRouteName="Main">
        <ModalStack.Screen name="Main" component={ModalStackNavigator} />
      </ModalStack.Navigator>
    </NavigationContainer>
  );
};

export default AuthNavigator;
