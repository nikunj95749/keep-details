import React from 'react';
import {ActivityIndicator, StyleSheet, View, Image} from 'react-native';

import AppNavigator from './app/AppNavigator';
import useAuthorizedSession from '../hooks/useAuthorizedSession';
import AuthNavigator from './auth/AuthNavigator';
import {appImages} from '../config';
const Navigation = () => {
  const [authToken, isInitializing] = useAuthorizedSession();

  if (isInitializing) {
    return (
      <View style={styles.loaderWrap}>
        <View
          style={{
            width: '100%',
            aspectRatio: 1,
            padding: 50,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            source={appImages.appIcon}
            style={{flex: 1, resizeMode: 'contain'}}></Image>
        </View>

        <ActivityIndicator size="large" color={'black'} />
      </View>
    );
  }

  return <AppNavigator />;
};

export default Navigation;

const styles = StyleSheet.create({
  loaderWrap: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
