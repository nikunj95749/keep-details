import React from 'react';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {StyleSheet, Image, View, Text} from 'react-native';

import Details from '../../screens/Details/DetailsScreen';
import NotesScreen from '../../screens/Notes/NotesScreen';
import Moreinfo from '../../screens/Moreinfo/MoreinfoScreeen';
import {appImages} from '../../config';
import CreditOrDebitCard from '../../screens/Details/CreditOrDebitCard';
import FriendsNFamily from '../../screens/Details/FriendsNFamily';
import Identifications from '../../screens/Details/Identifications';
import PassWords from '../../screens/Details/PassWords';
import VisitingCards from '../../screens/Details/VisitingCards';
import Addresses from '../../screens/Details/Addresses';
import AddFriendNFamilyDetails from '../../screens/Details/FriendsNFamily/AddFriendNFamilyDetails';
import AddPassword from '../../screens/Details/PassWords/AddPassword';
import AddIdentifications from '../../screens/Details/Identifications/AddIdentifications';
import AddCreditOrDebitCard from '../../screens/Details/CreditOrDebitCard/AddCreditOrDebitCard';
import AddNotes from '../../screens/Notes/AddNotes';
import AddReminder from '../../screens/Notes/Reminder/AddReminder';
import PinLockScreen from '../../screens/Moreinfo/PinkLock/PinLockScreen';
import LockScreen from '../../screens/LockScreen/LockScreen';
import useLockScreenShowAndHide from '../../hooks/useLockScreenShowAndHide';
import ForgotPassword from '../../screens/LockScreen/ForgotPassword';
import {THEME_COLOR, WHITE} from '../../styles';
import usePushNotificationController from '../../hooks/usePushNotificationController';
import SearchNotesScreen from '../../screens/Notes/SearchNotesScreen';
import SearchFriendDetailsScreen from '../../screens/Details/FriendsNFamily/SearchFriendDetailsScreen';
import SearchIdentificationsScreen from '../../screens/Details/Identifications/SearchIdentificationsScreen';
import SearchCreditOrDebitCardScreen from '../../screens/Details/CreditOrDebitCard/SearchCreditOrDebitCardScreen';
import SearchPassWordsScreen from '../../screens/Details/PassWords/SearchPassWordsScreen';
import CalculatorListsScreen from '../../screens/Calculator/CalculatorListsScreen';
import CalculatorUnfocus from '../../assets/appimages/CalculatorUnfocus.svg';
import CalculatorFocus from '../../assets/appimages/CalculatorFocus.svg';
import NormalCalculator from '../../screens/Calculator/NormalCalculator';
import EMICalculator from '../../screens/Calculator/EMICalculator';
import SigninScreen from '../../screens/Auth/Login/SigninScreen';
import RegisterScreen from '../../screens/Auth/Login/RegisterScreen';
import ResetPasswordScreen from '../../screens/Auth/Login/ResetPasswordScreen';

const BottomTab = createBottomTabNavigator();

const MainStack = createStackNavigator();
const RootStack = createStackNavigator();

export default function AppNavigator() {
  usePushNotificationController();
  return (
    <NavigationContainer>
      <RootStack.Navigator
        mode="modal"
        headerMode="none"
        initialRouteName="Main">
        <RootStack.Screen name="Main" component={MainStackScreen} />
        <RootStack.Screen name="LockScreen" component={LockScreen} />
        <RootStack.Screen name="ForgotPassword" component={ForgotPassword} />
        <RootStack.Screen
          name="Signin"
          component={SigninScreen}
          options={{headerShown: false}}
        />
        <RootStack.Screen name="RegisterScreen" component={RegisterScreen} />
        <RootStack.Screen
          name="ResetPasswordScreen"
          component={ResetPasswordScreen}
        />
      </RootStack.Navigator>
    </NavigationContainer>
  );
}

function MainStackScreen({navigation}) {
  const TransitionScreenOptions = {
    ...TransitionPresets.SlideFromRightIOS, // This is where the transition happens
  };
  useLockScreenShowAndHide();
  return (
    <MainStack.Navigator
      screenOptions={TransitionScreenOptions}
      initialRouteName="BottomTabNavigator">
      <MainStack.Screen
        name="BottomTabNavigator"
        component={BottomTabNavigator}
        options={{headerShown: false}}
      />
      <MainStack.Screen
        name="CreditOrDebitCard"
        component={CreditOrDebitCard}
        options={{headerShown: false}}
      />
      <MainStack.Screen
        name="SearchNotesScreen"
        component={SearchNotesScreen}
        options={{headerShown: false}}
      />

      <MainStack.Screen
        name="SearchFriendDetailsScreen"
        component={SearchFriendDetailsScreen}
        options={{headerShown: false}}
      />

      <MainStack.Screen
        name="SearchIdentificationsScreen"
        component={SearchIdentificationsScreen}
        options={{headerShown: false}}
      />

      <MainStack.Screen
        name="SearchCreditOrDebitCardScreen"
        component={SearchCreditOrDebitCardScreen}
        options={{headerShown: false}}
      />

      <MainStack.Screen
        name="NormalCalculator"
        component={NormalCalculator}
        options={{headerShown: false}}
      />

      <MainStack.Screen
        name="EMICalculator"
        component={EMICalculator}
        options={{headerShown: false}}
      />

      <MainStack.Screen
        name="SearchPassWordsScreen"
        component={SearchPassWordsScreen}
        options={{headerShown: false}}
      />

      <MainStack.Screen
        name="FriendsNFamily"
        component={FriendsNFamily}
        options={{headerShown: false}}
      />
      <MainStack.Screen
        name="Identifications"
        component={Identifications}
        options={{headerShown: false}}
      />
      <MainStack.Screen
        name="PassWords"
        component={PassWords}
        options={{headerShown: false}}
      />
      <MainStack.Screen
        name="VisitingCards"
        component={VisitingCards}
        options={{headerShown: false}}
      />
      <MainStack.Screen
        name="Addresses"
        component={Addresses}
        options={{headerShown: false}}
      />
      <MainStack.Screen
        name="AddFriendNFamilyDetails"
        component={AddFriendNFamilyDetails}
        options={{headerShown: false}}
      />
      <MainStack.Screen
        name="AddPassword"
        component={AddPassword}
        options={{headerShown: false}}
      />
      <MainStack.Screen
        name="AddIdentifications"
        component={AddIdentifications}
        options={{headerShown: false}}
      />
      <MainStack.Screen
        name="AddCreditOrDebitCard"
        component={AddCreditOrDebitCard}
        options={{headerShown: false, mode: 'card'}}
      />
      <MainStack.Screen
        name="AddNotes"
        component={AddNotes}
        options={{headerShown: false}}
      />

      <MainStack.Screen
        name="AddReminder"
        component={AddReminder}
        options={{headerShown: false}}
      />

      <MainStack.Screen
        name="PinLock"
        component={PinLockScreen}
        options={{headerShown: false}}
      />
    </MainStack.Navigator>
  );
}

function BottomTabNavigator({navigation}) {
  return (
    <BottomTab.Navigator
      initialRouteName="Home"
      lazy={true}
      tabBarOptions={{
        showIcon: true,
        showLabel: true,
        style: {
          borderTopWidth: 0.5,
          backgroundColor: WHITE,
        },
        labelStyle: {
          fontSize: 12,
        },
      }}
      navigationOptions={{
        header: {
          visible: true,
        },
      }}>
      <BottomTab.Screen
        name="Details"
        component={Details}
        options={{
          headerShown: false,
          tabBarIcon: ({focused}) => {
            return (
              <Image
                source={appImages.details}
                style={{
                  top: 2,
                  height: 22,
                  width: 22,
                  tintColor: focused ? THEME_COLOR : '#C4C4C4',
                }}
                resizeMode={'contain'}
              />
            );
          },
          tabBarLabel: ({focused}) => {
            return (
              <Text style={{color: focused ? THEME_COLOR : '#C4C4C4'}}>
                Details
              </Text>
            );
          },
        }}
      />

      <BottomTab.Screen
        name="Notes"
        component={NotesScreen}
        options={{
          headerShown: false,
          tabBarIcon: ({focused}) => {
            return (
              <Image
                source={appImages.notes}
                style={{
                  top: 2,
                  height: 22,
                  width: 22,
                  tintColor: focused ? THEME_COLOR : '#C4C4C4',
                }}
                resizeMode={'contain'}
              />
            );
          },
          tabBarLabel: ({focused}) => {
            return (
              <Text style={{color: focused ? THEME_COLOR : '#C4C4C4'}}>
                Notes
              </Text>
            );
          },
        }}
      />

      <BottomTab.Screen
        name="CalculatorListsScreen"
        component={CalculatorListsScreen}
        options={{
          headerShown: false,
          tabBarIcon: ({focused}) => {
            return (
              <View style={{top: 2}}>
                {focused ? (
                  <CalculatorFocus height={22} width={22} />
                ) : (
                  <CalculatorUnfocus height={22} width={22} />
                )}
              </View>
            );
          },
          tabBarLabel: ({focused}) => {
            return (
              <Text style={{color: focused ? THEME_COLOR : '#C4C4C4'}}>
                Calculator
              </Text>
            );
          },
        }}
      />

      <BottomTab.Screen
        name="Moreinfo"
        component={Moreinfo}
        options={{
          headerShown: false,
          tabBarIcon: ({focused}) => {
            return (
              <Image
                source={appImages.settings}
                style={{
                  top: 2,
                  height: 22,
                  width: 22,
                  tintColor: focused ? THEME_COLOR : '#C4C4C4',
                }}
                resizeMode={'contain'}
              />
            );
          },
          tabBarLabel: ({focused}) => {
            return (
              <Text style={{color: focused ? THEME_COLOR : '#C4C4C4'}}>
                Settigs
              </Text>
            );
          },
        }}
      />
    </BottomTab.Navigator>
  );
}

const styles = StyleSheet.create({});
