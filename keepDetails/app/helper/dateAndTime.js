import {format} from 'date-fns';

export const formatDate = (date = new Date(), formats = 'YYYY-MM-Do') =>
  format(date, formats);

export const computeDateDifference = (
  date = new Date(),
  unitOfTime = 'year',
) => {
  const today = new Date();
  let age_now = today.getFullYear() - date.getFullYear();
  const m = today.getMonth() - date.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < date.getDate())) {
    age_now--;
  }
  return age_now;
};
