import AsyncStorage from '@react-native-community/async-storage';
import {logError} from './logging';

const TOKEN_KEY = '@auth_token';
const USER_DETAILS = '@user_details';

export const setAuthToken = async (value = '') => {
  try {
    await AsyncStorage.setItem(TOKEN_KEY, value);
  } catch (err) {
    logError(err, '[setAuthToken] AsyncStorage Error');
  }
};

export const setValueInLocal = async (key = '', value = '') => {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (err) {
    logError(err, '[setAuthToken] AsyncStorage Error');
  }
};

export const getValueFromLocal = async (key = '') => {
  try {
    return await AsyncStorage.getItem(key);
  } catch (err) {
    logError(err, '[getAuthToken] AsyncStorage Error');
    return null;
  }
};

export const getAuthToken = async () => {
  try {
    return await AsyncStorage.getItem(TOKEN_KEY);
  } catch (err) {
    logError(err, '[getAuthToken] AsyncStorage Error');
    return null;
  }
};

export const setUserdetails = async (value = '') => {
  try {
    await AsyncStorage.setItem(USER_DETAILS, value);
  } catch (err) {
    logError(err, '[setUserdetails] AsyncStorage Error');
  }
};

export const getUserdetails = async () => {
  try {
    return await AsyncStorage.getItem(USER_DETAILS);
  } catch (err) {
    logError(err, '[getAuthToken] AsyncStorage Error');
    return null;
  }
};

export const removeAuthToken = async () => {
  try {
    await AsyncStorage.removeItem(TOKEN_KEY);
  } catch (err) {
    logError(err, '[removeAuthToken] AsyncStorage Error');
  }
};

export const clearAsyncStorage = async () => {
  try {
    AsyncStorage.clear();
  } catch (err) {
    logError(err, '[clearStorage] AsyncStorage Error');
  }
};
