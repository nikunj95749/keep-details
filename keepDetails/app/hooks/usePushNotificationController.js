import {useEffect, useState} from 'react';

import {useDispatch} from 'react-redux';
import OneSignal from 'react-native-onesignal';

const usePushNotificationController = () => {
  const [notificationObject, setNotificationObject] = useState({});

  const dispatch = useDispatch();

  const _clearNotificationObject = () => {
    setNotificationObject({});
  };

  useEffect(() => {
    // dev:
    // prod: 844717ef-b72d-45b3-beef-7742543aa339

    OneSignal.setAppId('844717ef-b72d-45b3-beef-7742543aa339');
    OneSignal.setLogLevel(6, 0);
    // OneSignal.promptForPushNotificationsWithUserResponse((response) => {
    //   console.log('ressssss ', response);
    // });

    OneSignal.setNotificationWillShowInForegroundHandler(
      (notifReceivedEvent) => {
        console.log(
          'OneSignal: notification will show in foreground:',
          notifReceivedEvent,
        );
      },
    );
    OneSignal.setNotificationOpenedHandler((notification) => {
      console.log(
        'OneSignal: notification opened:',
        notification?.notification?.additionalData,
      );
      setNotificationObject(notification?.notification?.additionalData);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return {
    notificationObject,
    clearNotificationObject: _clearNotificationObject,
  };
};
export default usePushNotificationController;
