import {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {setAuthTokenAction} from '../store/auth';

import {getAuthToken, getUserdetails} from '../helper/auth';
import {setUserDetailsAction} from '../store/userDetails';
import {firebase} from '@react-native-firebase/firestore';
const useAuthorizedSession = () => {
  const authToken = useSelector((state) => state.auth?.authToken ?? '');
  const dispatch = useDispatch();

  const [isInitializing, setIsInitializing] = useState(true);

  useEffect(() => {
    const checkStoredTokenAvailability = async () => {
      const storedToken = await getAuthToken();

      if (storedToken) {
        try {
          const docs = await firebase
            .firestore()
            .collection('Users')
            .doc(storedToken)
            .get();
          dispatch(setUserDetailsAction(docs.data()));
        } catch (error) {}
        dispatch(setAuthTokenAction(storedToken));
      } else {
        throw new Error('No token found');
      }
    };

    const validateSessionAndFetch = async () => {
      try {
        await checkStoredTokenAvailability();
      } catch {
      } finally {
        setIsInitializing(false);
      }
    };

    validateSessionAndFetch();
  }, []);

  return [authToken, isInitializing];
};

export default useAuthorizedSession;
