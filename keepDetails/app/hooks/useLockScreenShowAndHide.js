import {useEffect, useState, useRef} from 'react';
import {AppState} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {useNavigation} from '@react-navigation/core';

const useLockScreenShowAndHide = () => {
  const userDetails = useSelector((state) => state.userDetails ?? {});
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const appState = useRef(AppState.currentState);
  const [appStateVisible, setAppStateVisible] = useState(appState.current);

  useEffect(() => {
    if (userDetails?.password) {
      navigation.navigate('LockScreen');
    }
    // AppState.addEventListener('change', _handleAppStateChange);

    // return () => {
    //   AppState.removeEventListener('change', _handleAppStateChange);
    // };
  }, []);

  const _handleAppStateChange = (nextAppState) => {
    if (
      appState.current.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      navigation.navigate('LockScreen');
    }

    appState.current = nextAppState;
    setAppStateVisible(appState.current);
    console.log('AppState', appState.current);
  };

  return [];
};

export default useLockScreenShowAndHide;
