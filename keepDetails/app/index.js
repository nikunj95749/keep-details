import React, {useEffect, useState} from 'react';
import Navigator from './navigation';
import SplashScreen from 'react-native-splash-screen';
import {View, Image, StatusBar} from 'react-native';

import configureStore from './store/configureStore';
import {Provider} from 'react-redux';
import {colors} from 'react-native-elements';
import admob, {MaxAdContentRating} from '@react-native-firebase/admob';

const store = configureStore();

export default function App() {
  useEffect(() => {
    SplashScreen.hide();

    admob()
      .setRequestConfiguration({
        // Update all future requests suitable for parental guidance
        maxAdContentRating: MaxAdContentRating.PG,

        // Indicates that you want your content treated as child-directed for purposes of COPPA.
        tagForChildDirectedTreatment: true,

        // Indicates that you want the ad request to be handled in a
        // manner suitable for users under the age of consent.
        tagForUnderAgeOfConsent: true,
      })
      .then(() => {
        // Request config successfully set!
      });
  }, []);

  return (
    <Provider store={store}>
      <StatusBar backgroundColor={colors.white} barStyle="dark-content" />
      <View style={{flex: 1}}>
        <Navigator />
      </View>
    </Provider>
  );
}
