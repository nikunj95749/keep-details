const SET_IS_LOADING = 'SET_IS_LOADING';

const initialState = {
  isLoading: false,
};

// reducer
export default (state = initialState, action) => {
  if (action.type === SET_IS_LOADING) {
    return { ...state, isLoading: action.payload };
  }

  return state;
};

export const setIsLoading = (value = true) => ({
  type: SET_IS_LOADING,
  payload: value,
});
