import produce from 'immer';

const SET_USER_ID = 'SET_USER_ID';
const SET_USER_FULL_NAME = 'SET_USER_FULL_NAME';
const SET_FONT_SIZE = 'SET_FONT_SIZE';
const SET_THEME_COLOR = 'SET_THEME_COLOR';
const SET_TITLE_DETAILS_FONT_COLOR = 'SET_TITLE_DETAILS_FONT_COLOR';
const SET_TITLE_FONT_COLOR = 'SET_TITLE_FONT_COLOR';
const SET_USER_DETAILS = 'SET_USER_DETAILS';
const SET_LOCK = 'SET_LOCK';
const SET_RECOVER_EMAIL_ID = 'SET_RECOVER_EMAIL_ID';
const SET_PASSWORD = 'SET_PASSWORD';

const initialState = {
  userId: '',
  userFullName: '',
  fontsize: 16,
  themeColor: '',
  titleDetailsFontColor: '',
  titleFontColor: '',
  isSetLock: false,
  recoverEmailId: '',
  password: '',
};

// reducer
export default (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SET_USER_ID:
        draft.userId = action.payload;
        break;
      case SET_FONT_SIZE:
        draft.fontsize = action.payload;
        break;
      case SET_THEME_COLOR:
        draft.themeColor = action.payload;
        break;
      case SET_TITLE_DETAILS_FONT_COLOR:
        draft.titleDetailsFontColor = action.payload;
        break;
      case SET_TITLE_FONT_COLOR:
        draft.titleFontColor = action.payload;
        break;
      case SET_USER_FULL_NAME:
        draft.userFullName = action.payload;
        break;
      case SET_LOCK:
        draft.isSetLock = action.payload;
        break;

      case SET_RECOVER_EMAIL_ID:
        draft.recoverEmailId = action.payload;
        break;
      case SET_PASSWORD:
        draft.password = action.payload;
        break;
      case SET_USER_DETAILS:
        mergeUserDetails(draft, action.payload);
        break;
    }
  });

export const setUserIdAction = (value = '') => ({
  type: SET_USER_ID,
  payload: value,
});

export const setFontSizeAction = (value = '') => ({
  type: SET_FONT_SIZE,
  payload: value,
});

export const setRecoverEmailId = (value = '') => ({
  type: SET_RECOVER_EMAIL_ID,
  payload: value,
});

export const setPassword = (value = '') => ({
  type: SET_PASSWORD,
  payload: value,
});

export const setThemeColorAction = (value = '') => ({
  type: SET_THEME_COLOR,
  payload: value,
});

export const setTitleDetailsFontColorAction = (value = '') => ({
  type: SET_TITLE_DETAILS_FONT_COLOR,
  payload: value,
});

export const setTitleFontColorAction = (value = '') => ({
  type: SET_TITLE_FONT_COLOR,
  payload: value,
});

export const setUserFullNameAction = (value = '') => ({
  type: SET_USER_FULL_NAME,
  payload: value,
});

export const setUserLockAction = (value = false) => ({
  type: SET_LOCK,
  payload: value,
});

export const setUserDetailsAction = (value = {}) => ({
  type: SET_USER_DETAILS,
  payload: value,
});

const mergeUserDetails = (draft = {}, fetchedData = {}) => {
  draft.userId = fetchedData.userId;
  draft.userFullName = fetchedData.userFullName;
  draft.fontsize = fetchedData.fontsize;
  draft.themeColor = fetchedData.themeColor;
  draft.titleDetailsFontColor = fetchedData.titleDetailsFontColor;
  draft.titleFontColor = fetchedData.titleFontColor;
  draft.isSetLock = fetchedData.isSetLock;
  draft.recoverEmailId = fetchedData.recoverEmailId;
  draft.password = fetchedData.password;
};
