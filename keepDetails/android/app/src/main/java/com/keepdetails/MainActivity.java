package com.keepdetails;

import android.os.Bundle;

import androidx.multidex.MultiDex;

import com.facebook.react.ReactActivity;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import org.devio.rn.splashscreen.SplashScreen;

public class MainActivity extends ReactActivity {

  /**
   * Returns the name of the main component registered from JavaScript. This is used to schedule
   * rendering of the component.
   */
  @Override
  protected String getMainComponentName() {
    return "keepDetails";
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    MultiDex.install(this);
    super.onCreate(savedInstanceState);
    SplashScreen.show(this);
    MobileAds.initialize(this, new OnInitializationCompleteListener() {
      @Override
      public void onInitializationComplete(InitializationStatus initializationStatus) {
      }
    });
  }


}
